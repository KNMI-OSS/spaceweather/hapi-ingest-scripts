#!/usr/bin/env python3
import json
import sys
import logging
import schedule
import time
import numpy as np
import pandas as pd
from swxtools.access import goes_xrs, goes_primary_secondary
from swxtools.dataframe_tools import mark_gaps_in_dataframe
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
    resample_lower_cadences,
)
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json = '''{
    "id": "xray_flux_science_goes_8",
    "description": "GOES 8 X-ray flux science data",
    "timeStampLocation": "begin",
    "resourceURL": "https://www.ncei.noaa.gov/data/goes-space-environment-monitor/access/science/xrs",
    "resourceID": "NOAA NCEI",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "xray_flux_short",
            "type": "double",
            "units": "W/m²",
            "fill": "-1e30",
            "description": "Solar X-ray flux in the 0.05-0.4 nm passband",
            "label": "",
            "key": false
        },
        {
            "name": "xray_flux_long",
            "type": "double",
            "units": "W/m²",
            "fill": "-1e30",
            "description": "Solar X-ray flux in the 0.1-0.8 nm passband",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1M",
    "x_relations": [
        {
            "id": "xray_flux_science_goes_8_PT5M",
            "description": "GOES 8 X-ray flux science data downsampled to 5 minutes",
            "cadence": "PT5M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "xray_flux_science_goes_8_PT30M",
            "description": "GOES 8 X-ray flux science data downsampled to 30 minutes",
            "cadence": "PT30M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "xray_flux_science_goes_8_PT3H",
            "description": "GOES 8 X-ray flux science data downsampled to 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "xray_flux_science_goes_8_PT12H",
            "description": "GOES 8 X-ray flux science data downsampled to 12 hours",
            "cadence": "PT12H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "xray_flux_science_goes_8_P3D",
            "description": "GOES 8 X-ray flux science data downsampled to 3 days",
            "cadence": "P3D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "xray_flux_science_goes_8_P10D",
            "description": "GOES 8 X-ray flux science data downsampled to 10 days",
            "cadence": "P10D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''

def get_metadata(metadata_json, goesnum):
    # Update the metadata
    # 1. Set the satellite
    metadata_json_sat = metadata_json.replace(
        'goes_8',
        f'goes_{goesnum}'
    )
    metadata_json_sat = metadata_json_sat.replace(
        'GOES 8',
        f'GOES {goesnum}'
    )

    metadata = json.loads(metadata_json_sat)
    return metadata


def store_xrays_df(metadata, df, resample=True):
    # Get the id, parameters and cadences from the definition
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']
    cadence = metadata_values['cadence']
    ensure_dataset_info(hapi_server, hapi_server_key, metadata)
    # df = mark_gaps_in_dataframe(
    #     df,
    #     nominal_timedelta=pd.to_timedelta(cadence),
    #     nominal_start_time=None,
    #     nominal_end_time=df.index[-1]+pd.to_timedelta(cadence)
    # )
    # df = df.rename({'0.05-0.4nm': 'xray_flux_short',
    #                 '0.1-0.8nm': 'xray_flux_long'}, axis=1)
    # df = df.where(df['xray_flux_long'] > 1e-10, other=np.nan)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    data = df[parameters].replace(replace_nan_fill)

    add_data(hapi_server,
             hapi_server_key,
             db_id,
             data)

    if resample:
        resample_lower_cadences(hapi_server,
                                hapi_server_key,
                                db_id,
                                dataframe=df,
                                t_first_data=df.index[0],
                                t_last_data=df.index[-1])


def store_xrays_file(metadata, filename, resample=True):
    logging.info(f"Storing {filename} in data store.")
    df = swpc_rt.json_to_dataframe(filename)
    store_xrays_df(metadata, df, resample)


def store_xrays_single_sat(goesnum):
    logging.info(f"Processing data for GOES {goesnum}")
    metadata = get_metadata(metadata_json, goesnum)
    filenames = goes_xrs.download(goesnum)
    for filename in filenames:
        logging.info(f"Reading {filename}")
        df = goes_xrs.nc_to_dataframe(filename, apply_flags=True, convert_column_names=True)
        logging.info(f"Storing {filename}")
        store_xrays_df(metadata, df)


def store_xrays_combined():
    metadata = get_metadata(metadata_json, 'combined')
    df_goeslist = goes_primary_secondary.goes_primary_secondary_df()
    goeslist = df_goeslist.to_dict(orient='records')
    for entry in goeslist:
        goesnum = entry['primary']
        t0 = entry['startDate']
        t1 = entry['stopDate']
        files = goes_xrs.download(goesnum=goesnum)
        if goesnum < 8:
            continue
        logging.info(f"Processing data for GOES {goesnum}")
        for file in files:
            logging.info(f"Processing {file}")
            df = goes_xrs.nc_to_dataframe(
                file,
                apply_flags=True,
                convert_column_names=True
            )
            df = df[t0:t1]
            if len(df) > 0:
                store_xrays_df(metadata, df)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    if len(arguments) == 1:
        if arguments[0] == 'combined':
            store_xrays_combined()
        elif int(arguments[0]) >= 8 and int(arguments[0]) <= 18:
            store_xrays_single_sat(goesnum=int(arguments[0]))
        else:
            logging.error("Invalid arguments")

#     if len(arguments) == 0:
#         store_xrays(metadata=metadata, duration='7-day',
#                     primary_secondary='both')
#
#         schedule.every(5).minutes.do(store_xrays,
#                                      duration='1-day',
#                                      metadata=metadata,
#                                      primary_secondary='both')
#
#         while True:
#             try:
#                 schedule.run_pending()
#             except (BaseException) as err:
#                 logging.error(f"Exception: {err}")
#             time.sleep(1)
#
#     else:
#         for filename in arguments:
#             store_xrays_file(
#                 metadata=metadata,
#                 filename=filename,
#                 resample=False
#             )
