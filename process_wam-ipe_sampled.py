#!/usr/bin/env python3
import argparse
import pandas as pd
from swxtools.access import swarm_diss

cmd_line_parser = argparse.ArgumentParser(
    description=('Interpolates WAM-IPE model output along' +
                 ' a provided satellite trajectory'),
    epilog='Contact: eelco.doornbos@knmi.nl'
)


def parse_date_arg(arg):
    (t0, t1) = arg.split(',')
    return (pd.to_datetime(t0), pd.to_datetime(t1))


sats = ['A', 'B', 'C']
models = ['WAM', 'IPE']

cmd_line_parser.add_argument('-s', '--sat', choices=sats, default='A')
cmd_line_parser.add_argument('-m', '--model', choices=models, default='WAM')
cmd_line_parser.add_argument('-t', '--time', type=parse_date_arg)

args = cmd_line_parser.parse_args()
print(args)

swarm_diss_obj = swarm_diss.SwarmFiles(
    data_type='MODx_SC',
    sat=f'Swarm {args.sat}'
)

days = pd.date_range(args.time[0].floor('D'), args.time[1].ceil('D'), freq='D')
for day0, day1 in zip(days, days[1:]):
    swarm_diss_obj.set_time_interval(day0, day1)
    orbit = swarm_diss_obj.to_dataframe()
    print(day0, day1)
