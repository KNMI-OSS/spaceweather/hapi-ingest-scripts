#!/usr/bin/env python3
import json
import sys
import schedule
import time
import logging
import numpy as np
import pandas as pd
from swxtools.access import swpc_rt
from swxtools.dataframe_tools import mark_gaps_in_dataframe
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
    resample_lower_cadences,
)
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json = '''{
    "id": "proton_flux_500_rt",
    "description": "Real-time GOES proton flux",
    "timeStampLocation": "begin",
    "resourceURL": "https://services.swpc.noaa.gov/json/goes/primary/",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "proton_flux_10MeV",
            "type": "double",
            "units": "",
            "fill": "-1e30",
            "description": "Flux of >10 MeV protons",
            "label": "",
            "key": false
        },
        {
            "name": "proton_flux_50MeV",
            "type": "double",
            "units": "",
            "fill": "-1e30",
            "description": "Flux of >50 MeV protons",
            "label": "",
            "key": false
        },
        {
            "name": "proton_flux_100MeV",
            "type": "double",
            "units": "",
            "fill": "-1e30",
            "description": "Flux of >100 MeV protons",
            "label": "",
            "key": false
        },
        {
            "name": "proton_flux_500MeV",
            "type": "double",
            "units": "",
            "fill": "-1e30",
            "description": "Flux of >500 MeV protons",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT5M",
    "x_relations": [
        {
            "id": "proton_flux_500_rt_PT30M",
            "description": "Real-time GOES proton flux downsampled to 30 minutes",
            "cadence": "PT30M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "proton_flux_500_rt_PT3H",
            "description": "Real-time GOES proton flux downsampled to 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "proton_flux_500_rt_PT12H",
            "description": "Real-time GOES proton flux downsampled to 12 hours",
            "cadence": "PT12H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "proton_flux_500_rt_P3D",
            "description": "Real-time GOES proton flux downsampled to 3 days",
            "cadence": "P3D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "proton_flux_500_rt_P10D",
            "description": "Real-time GOES proton flux downsampled to 10 days",
            "cadence": "P10D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''


def store_protons_file(metadata, filename, resample=True):
    # Get the id, parameters and cadences from the definition
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']
    cadence = metadata_values['cadence']

    logging.info(f"Storing {filename} in data store.")
    df = swpc_rt.json_to_dataframe(filename)
    df_flux = {}
    fluxes = [10, 50, 100, 500]
    for flux in fluxes:
        df_flux[flux] = df.query(f"(energy == '>={flux} MeV')")['flux']
    df2 = pd.concat(df_flux, axis=1)
    for flux in fluxes:
        df2 = df2.rename({flux: f'proton_flux_{flux}MeV'}, axis=1)
    df2 = mark_gaps_in_dataframe(
        df2,
        nominal_timedelta=pd.to_timedelta(cadence),
        nominal_start_time=None,
        nominal_end_time=df.index[-1]+pd.to_timedelta(cadence)
    )
    df2['time'] = df2.index.strftime(ISO_TIMEFMT)
    data = df2[parameters].replace(replace_nan_fill)
    add_data(hapi_server,
             hapi_server_key,
             db_id,
             data)

    if resample:
        resample_lower_cadences(hapi_server,
                                hapi_server_key,
                                db_id,
                                t_first_data=df.index[0],
                                t_last_data=df.index[-1])


def store_protons(metadata, duration='1-day'):
    filenames = swpc_rt.download_data(datatype='integral-protons',
                                      duration=duration)
    for filename in filenames:
        print(filename)
        store_protons_file(metadata, filename)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    metadata = json.loads(metadata_json)
    info = ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    arguments = sys.argv[1:]
    if len(arguments) == 0:
        store_protons(metadata=metadata, duration='7-day')
        schedule.every(5).minutes.do(store_protons,
                                     duration='1-day',
                                     metadata=metadata)
        while True:
            try:
                schedule.run_pending()
            except (BaseException) as err:
                logging.error(f"Exception: {err}")
            time.sleep(1)
    elif arguments[0] == 'resample':
        resample_lower_cadences(hapi_server,
                                hapi_server_key,
                                'proton_flux_rt',
                                t_first_data=pd.to_datetime(arguments[1]),
                                t_last_data=pd.to_datetime(arguments[2]))
    else:
        for filename in arguments:
            store_protons_file(metadata=metadata, filename=filename, resample=True)
