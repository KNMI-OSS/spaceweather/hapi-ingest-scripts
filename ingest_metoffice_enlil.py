#!/usr/bin/env python3
import logging
import os
import json
import pandas as pd
import schedule
import time
from swxtools.config import config
from swxtools.access import esa_portal_products
from swxtools.download_tools import ensure_data_dir
from swxtools.hapi_client import (
    ensure_dataset_info,
    add_data
)

metadata_json = '''{
    "id": "solar_wind_plasma_enlil_metoffice",
    "description": "Solar wind plasma parameters from Met Office ENLIL prediction",
    "timeStampLocation": "begin",
    "resourceURL": "https://swe.ssa.esa.int/metoffice-enlil-e-federated",
    "resourceID": "",
    "contact": "helpdesk.swe@esa.int",
    "contactID": "",
    "cadence": "PT1H",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "timetag_issue",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time of forecast issue",
            "label": "",
            "key": true
        },
        {
            "name": "density",
            "type": "double",
            "units": "1/cm3",
            "fill": null,
            "description": "Density",
            "label": "",
            "key": false
        },
        {
            "name": "speed",
            "type": "double",
            "units": "km/s",
            "fill": null,
            "description": "Speed",
            "label": "",
            "key": false
        },
        {
            "name": "temperature",
            "type": "double",
            "units": "K",
            "fill": null,
            "description": "Temperature",
            "label": "",
            "key": false
        },
        {
            "name": "pressure",
            "type": "double",
            "units": "nPa",
            "fill": null,
            "description": "Pressure",
            "label": "",
            "key": false
        },
        {
            "name": "bt",
            "type": "double",
            "units": "nT",
            "fill": null,
            "description": "Interplanetary Magnetic Field",
            "label": "",
            "key": false
        }
    ]
}'''

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']
esa_username = config['swe_ssa_esa_int_username']
esa_password = config['swe_ssa_esa_int_password']

# Local path to store the NetCDF files
LOCAL_PATH = f"{config['local_source_data_path']}/metoffice/enlil/"
ensure_data_dir(LOCAL_PATH)

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

def ingest_enlil_metoffice(start_date=None, end_date=None):
    # Set metadata and derived info
    metadata = json.loads(metadata_json)
    db_id = metadata['id']
    parameters = [param['name'] for param in metadata['parameters']]
    _ = ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    # Read the list of already processed/ingested data files
    processed_filelist_file = f'processed_files/{db_id}_processed_files.txt'
    if os.path.isfile(processed_filelist_file):
        with open(processed_filelist_file, 'r') as fh:
            processed_filenames = fh.read().splitlines()
    else:
        processed_filenames = []

    # Set times for download, by default, download for previous day
    if end_date is not None:
        t1 = pd.to_datetime(end_date, utc=True)
    else:
        t1 = pd.Timestamp.utcnow().ceil('2H')
    if start_date is not None:
        t0 = pd.to_datetime(start_date, utc=True)
    else:
        t0 = t1 - pd.to_timedelta('24H').floor('2H')

    print(t0, t1)

    # Generate list of possible filenames for the period
    date_range = pd.date_range(t0, t1, freq='2H')
    filenames = [f"{date.strftime('%Y-%m-%dT%HZ')}.evo.Earth.nc"
                 for date in date_range]

    for filename in filenames:
        logging.info(f"Considering {filename}...")
        if filename in processed_filenames:
            logging.info(f"Already processed {filename}")
            continue

        use_timetags = ['T00Z', 'T06Z', 'T12Z', 'T18Z']
        if not any(timetag in filename for timetag in use_timetags):
            continue

        # Download if not yet available
        local_filename = f"{LOCAL_PATH}{filename}"
        if not os.path.isfile(local_filename):
            esa_portal_products.download_enlil_file(
                esa_username,
                esa_password,
                filename,
                local_filename
            )
        if not os.path.isfile(local_filename):
            logging.error(f"File {local_filename} does not exist after download attempt")
            continue

        # Set reference time from filename
        reftime = pd.to_datetime(
            filename.split('/')[-1].split('.')[0],
            utc=True
        )

        logging.info(f"Ingesting {filename} in HAPI database")
        df = esa_portal_products.enlilfile_to_dataframe(
            local_filename,
            reftime
        )

        # Use only last 5 days from file, throw away model spin-up
        df_cropped = df[reftime-pd.to_timedelta(5, 'D'):]

        # Put in database
        add_data(hapi_server,
                 hapi_server_key,
                 db_id,
                 df_cropped[parameters])

        # Append the filename to the list of processed files
        processed_filenames.append(filename)
        with open(processed_filelist_file, 'a+') as fh:
            fh.write(filename + '\n')


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    ingest_enlil_metoffice(start_date=(pd.Timestamp.utcnow()-pd.to_timedelta('2D')).floor('2H'))

    schedule.every().hour.do(
        ingest_enlil_metoffice
    )

    while True:
        try:
            schedule.run_pending()
        except (BaseException) as err:
            logging.error(f"Exception: {err}")
        time.sleep(1)
