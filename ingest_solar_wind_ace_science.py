#!/usr/bin/env python3
import pandas as pd
import requests
from swxtools import download_tools, hapi_client
from swxtools.config import config
from swxtools.access import ace
import os
import schedule
import time
import logging
import numpy as np
import json

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

metadata_json_swepam = '''{
    "id": "solar_wind_plasma_ace_science",
    "description": "ACE SWEPAM: 64-sec averaged Real-time Bulk Parameters of the Solar Wind Plasma",
    "timeStampLocation": "begin",
    "resourceURL": "https://izw1.caltech.edu/ACE/ASC/level2/lvl2DATA_SWEPAM.html",
    "resourceID": "ACE Science Center",
    "contact": "asc@srl.caltech.edu",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "proton_speed",
            "type": "double",
            "units": "km/s",
            "fill": "-1e30",
            "description": "proton_speed",
            "label": "",
            "key": false
        },
        {
            "name": "proton_density",
            "type": "double",
            "units": "1/cm3",
            "fill": "-1e30",
            "description": "proton_density",
            "label": "",
            "key": false
        },
        {
            "name": "proton_temp",
            "type": "double",
            "units": "K",
            "fill": "-1e30",
            "description": "proton_temp",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1M4S",
    "x_relations": [
        {
            "id": "solar_wind_plasma_ace_science_PT5M",
            "description": "ACE SWEPAM: 64-sec averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 5 minutes",
            "cadence": "PT5M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_ace_science_PT30M",
            "description": "ACE SWEPAM: 64-sec averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 30 minutes",
            "cadence": "PT30M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_ace_science_PT3H",
            "description": "ACE SWEPAM: 64-sec averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_ace_science_PT12H",
            "description": "ACE SWEPAM: 64-sec averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 12 hours",
            "cadence": "PT12H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_ace_science_P3D",
            "description": "ACE SWEPAM: 64-sec averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 3 days",
            "cadence": "P3D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_ace_science_P10D",
            "description": "ACE SWEPAM: 64-sec averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 10 days",
            "cadence": "P10D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''

metadata_json_mag = '''{
    "id": "solar_wind_mag_ace_science",
    "description": "ACE MAG: 16-sec averaged interplanetary magnetic field",
    "timeStampLocation": "begin",
    "resourceURL": "https://izw1.caltech.edu/ACE/ASC/level2/lvl2DATA_MAG.html",
    "resourceID": "ACE Science Center",
    "contact": "asc@srl.caltech.edu",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "bt",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "bt",
            "label": "",
            "key": false
        },
        {
            "name": "bx_gsm",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "bx_gsm",
            "label": "",
            "key": false
        },
        {
            "name": "by_gsm",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "by_gsm",
            "label": "",
            "key": false
        },
        {
            "name": "bz_gsm",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "bz_gsm",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT16S",
    "x_relations": [
        {
            "id": "solar_wind_mag_ace_science_PT5M",
            "description": "ACE MAG: 16-sec averaged interplanetary magnetic field, downsampled to 5 minutes",
            "cadence": "PT5M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_mag_ace_science_PT30M",
            "description": "ACE MAG: 16-sec averaged interplanetary magnetic field, downsampled to 30 minutes",
            "cadence": "PT30M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_mag_ace_science_PT3H",
            "description": "ACE MAG: 16-sec averaged interplanetary magnetic field, downsampled to 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_mag_ace_science_PT12H",
            "description": "ACE MAG: 16-sec averaged interplanetary magnetic field, downsampled to 12 hours",
            "cadence": "PT12H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_mag_ace_science_P3D",
            "description": "ACE MAG: 16-sec averaged interplanetary magnetic field, downsampled to 3 days",
            "cadence": "P3D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_mag_ace_science_P10D",
            "description": "ACE MAG: 16-sec averaged interplanetary magnetic field, downsampled to 10 days",
            "cadence": "P10D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''

ace_science_rename_fields = {
    'mag': {
        'Bmag': 'bt',
        'Bgsm_x': 'bx_gsm',
        'Bgsm_y': 'by_gsm',
        'Bgsm_z': 'bz_gsm'
    },
    'swepam': {}
}

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                    level=logging.INFO,
                    datefmt="%Y-%m-%d %H:%M:%S")

metadata_swepam = json.loads(metadata_json_swepam)
info_swepam = hapi_client.ensure_dataset_info(hapi_server, hapi_server_key, metadata_swepam)

metadata_mag = json.loads(metadata_json_mag)
info_mag = hapi_client.ensure_dataset_info(hapi_server, hapi_server_key, metadata_mag)

def store_ace_timespan(t0, t1, metadata):
    # Get the necessary info from metadata
    metadata_values = hapi_client.get_info_values(metadata)
    db_id = metadata_values['id']
    if db_id == 'solar_wind_plasma_ace_science':
        datatype = 'swepam'
    elif db_id == 'solar_wind_mag_ace_science':
        datatype = 'mag'

    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    # Info
    info_request = hapi_client.get_info(hapi_server, db_id)

    # Upload to the data store
    files = ace.download_data(t0, t1, datatype=datatype)
    for file in files:
        logging.info(f"Reading {file}")
        df = ace.hdf_to_dataframe(file)
        df = df.rename(ace_science_rename_fields[datatype], axis=1)
        df_upload = df
        df_upload['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        df_upload = df_upload[parameters].replace(replace_nan_fill)
        hapi_client.add_data(hapi_server,
                             hapi_server_key,
                             db_id,
                             df_upload)

        hapi_client.resample_lower_cadences(hapi_server,
                                            hapi_server_key,
                                            db_id,
                                            dataframe=df,
                                            t_first_data=df.index[0],
                                            t_last_data=df.index[-1])


if __name__ == "__main__":
    t0 = pd.to_datetime('1997-01-01T', utc=True)
    t1 = pd.Timestamp.utcnow()
    t1 = pd.to_datetime('1997-12-31T23:59:59', utc=True)

    store_ace_timespan(t0, t1, metadata_swepam)
#     schedule.every(15).minutes.do(store_ace_latest)
#
#     while True:
#         try:
#             schedule.run_pending()
#         except (BaseException) as err:
#             logging.error(f"Exception: {err}")
#         time.sleep(1)
