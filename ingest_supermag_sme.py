#!/usr/bin/env python3
import json
import pandas as pd
import logging
from swxtools.config import config
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
)


hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json = '''{
    "id": "supermag_sme_index",
    "description": "SuperMAG electroject index",
    "timeStampLocation": "begin",
    "resourceURL": "https://supermag.jhuapl.edu/indices",
    "resourceID": "SuperMAG",
    "contact": "supermag@listserv.jhuapl.edu",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "smu",
            "type": "double",
            "units": "nT",
            "fill": "999999.0",
            "description": "SMU index",
            "label": "",
            "key": false
        },
        {
            "name": "sml",
            "type": "double",
            "units": "nT",
            "fill": "999999.0",
            "description": "SML index",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1M",
    "x_relations": [
        {
            "id": "supermag_sme_index_PT5M",
            "description": "SuperMAG electroject index downsampled to 5 minutes",
            "cadence": "PT5M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "supermag_sme_index_PT30M",
            "description": "SuperMAG electroject index downsampled to 30 minutes",
            "cadence": "PT30M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "supermag_sme_index_PT3H",
            "description": "SuperMAG electroject index downsampled to 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "supermag_sme_index_PT12H",
            "description": "SuperMAG electroject index downsampled to 12 hours",
            "cadence": "PT12H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "supermag_sme_index_P3D",
            "description": "SuperMAG electroject index downsampled to 3 days",
            "cadence": "P3D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "supermag_sme_index_P10D",
            "description": "SuperMAG electroject index downsampled to 10 days",
            "cadence": "P10D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''


def read_sme_file(filename):
    datetime_column_names = [
      'year',
      'month',
      'day',
      'hour',
      'minute',
      'second'
    ]

    column_names = [
        *datetime_column_names,
        'SML',
        'SMU'
    ]
    logging.info(f"Reading SuperMAG SME file: {filename}")
    df = pd.read_table(filename,
                       skiprows=105,
                       sep='\s+',
                       names=column_names,
                       na_values=[999999])
    df.index = pd.to_datetime(df[datetime_column_names])
    df.drop(datetime_column_names, axis=1, inplace=True)
    df.rename({'SML': 'sml',
               'SMU': 'smu'},
              axis=1,
              inplace=True)
    df['time'] = df.index.strftime(ISO_TIMEFMT)
    logging.info(f"Reading of {filename} is complete.")
    return df


def store_sme_file(hapi_server, hapi_server_key, metadata, filename):
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    df = read_sme_file(filename)
    df = df[:-1].replace(replace_nan_fill)
    data = df["2022-12-31T":][parameters]
    add_data(hapi_server,
             hapi_server_key,
             db_id,
             data)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    metadata = json.loads(metadata_json)

    info = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
    metadata_values = get_info_values(metadata)
    cadences = metadata_values['cadences']

    # Highest cadence (1-min)
    supermag_sme_txtfile = (
        config['local_source_data_path'] + '/supermag/' +
        '20240613-09-58-supermag.txt'
    )
    store_sme_file(
        hapi_server,
        hapi_server_key,
        metadata,
        supermag_sme_txtfile
    )
