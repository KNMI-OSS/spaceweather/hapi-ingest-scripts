#!/usr/bin/env python3
import sys
import json
import time
import requests.exceptions
import logging
import schedule
import pandas as pd
from swxtools.access import dscovr
from swxtools import hapi_client
from swxtools.config import config
from swxtools.dataframe_tools import mark_gaps_in_dataframe

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']


metadata_json_mag = '''{
    "id": "solar_wind_mag_dscovr",
    "description": "Interplanetary magnetic field observations collected from magnetometer on DSCOVR satellite - 1-minute average of Level 1 data",
    "timeStampLocation": "begin",
    "resourceURL": "http://www.ngdc.noaa.gov/dscovr/",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "william.rowland@noaa.gov",
    "contactID": "DOC/NOAA/NESDIS/NGDC",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "bt",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "bt",
            "label": "",
            "key": false
        },
        {
            "name": "bx_gsm",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "bx_gsm",
            "label": "",
            "key": false
        },
        {
            "name": "by_gsm",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "by_gsm",
            "label": "",
            "key": false
        },
        {
            "name": "bz_gsm",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "bz_gsm",
            "label": "",
            "key": false
        },
        {
            "name": "bx_gse",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "bx_gse",
            "label": "",
            "key": false
        },
        {
            "name": "by_gse",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "by_gse",
            "label": "",
            "key": false
        },
        {
            "name": "bz_gse",
            "type": "double",
            "units": "nT",
            "fill": "-1e30",
            "description": "bz_gse",
            "label": "",
            "key": false
        },
        {
            "name": "phi_gse",
            "type": "double",
            "units": "degrees",
            "fill": "-1e30",
            "description": "Interplanetary Magnetic Field polar angle in Geocentric Solar Ecliptic coordinates",
            "label": "",
            "key": false
        },
        {
            "name": "phi_gsm",
            "type": "double",
            "units": "degrees",
            "fill": "-1e30",
            "description": "Interplanetary Magnetic Field polar angle in Geocentric Solar Magnetospheric coordinates",
            "label": "",
            "key": false
        },
        {
            "name": "theta_gse",
            "type": "double",
            "units": "degrees",
            "fill": "-1e30",
            "description": "Interplanetary Magnetic Field clock angle in Geocentric Solar Ecliptic coordinates",
            "label": "",
            "key": false
        },
        {
            "name": "theta_gsm",
            "type": "double",
            "units": "degrees",
            "fill": "-1e30",
            "description": "Interplanetary Magnetic Field clock angle in Geocentric Solar Magnetospheric coordinates",
            "label": "",
            "key": false
        },
        {
            "name": "overall_quality",
            "type": "integer",
            "units": "",
            "fill": "-999",
            "description": "Overall sample quality (0 = normal, 1 = suspect, 2 = error)",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1M",
    "x_relations": [
        {
            "id": "solar_wind_mag_dscovr_PT5M",
            "description": "Interplanetary Magnetic Field measurements from DSCOVR downsampled to 5 minutes",
            "cadence": "PT5M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_mag_dscovr_PT30M",
            "description": "Interplanetary Magnetic Field measurements from DSCOVR downsampled to 30 minutes",
            "cadence": "PT30M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_mag_dscovr_PT3H",
            "description": "Interplanetary Magnetic Field measurements from DSCOVR downsampled to 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_mag_dscovr_PT12H",
            "description": "Interplanetary Magnetic Field measurements from DSCOVR downsampled to 12 hours",
            "cadence": "PT12H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_mag_dscovr_P3D",
            "description": "Interplanetary Magnetic Field measurements from DSCOVR downsampled to 3 days",
            "cadence": "P3D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_mag_dscovr_P10D",
            "description": "Interplanetary Magnetic Field measurements from DSCOVR downsampled to 10 days",
            "cadence": "P10D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''

metadata_json_plasma = '''{
    "id": "solar_wind_plasma_dscovr",
    "description": "DSCOVR Faraday Cup Level 2 One Minute Averages",
    "timeStampLocation": "begin",
    "resourceURL": "http://www.ngdc.noaa.gov/dscovr/",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "william.rowland@noaa.gov",
    "contactID": "DOC/NOAA/NESDIS/NGDC",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "proton_vx_gse",
            "type": "double",
            "units": "km/s",
            "fill": "-1e30",
            "description": "proton_vx_gse",
            "label": "",
            "key": false
        },
        {
            "name": "proton_vy_gse",
            "type": "double",
            "units": "km/s",
            "fill": "-1e30",
            "description": "proton_vy_gse",
            "label": "",
            "key": false
        },
        {
            "name": "proton_vz_gse",
            "type": "double",
            "units": "km/s",
            "fill": "-1e30",
            "description": "proton_vz_gse",
            "label": "",
            "key": false
        },
        {
            "name": "proton_vx_gsm",
            "type": "double",
            "units": "km/s",
            "fill": "-1e30",
            "description": "proton_vx_gsm",
            "label": "",
            "key": false
        },
        {
            "name": "proton_vy_gsm",
            "type": "double",
            "units": "km/s",
            "fill": "-1e30",
            "description": "proton_vy_gsm",
            "label": "",
            "key": false
        },
        {
            "name": "proton_vz_gsm",
            "type": "double",
            "units": "km/s",
            "fill": "-1e30",
            "description": "proton_vz_gsm",
            "label": "",
            "key": false
        },
        {
            "name": "proton_speed",
            "type": "double",
            "units": "km/s",
            "fill": "-1e30",
            "description": "proton_speed",
            "label": "",
            "key": false
        },
        {
            "name": "proton_density",
            "type": "double",
            "units": "1/cm3",
            "fill": "-1e30",
            "description": "proton_density",
            "label": "",
            "key": false
        },
        {
            "name": "proton_temperature",
            "type": "double",
            "units": "K",
            "fill": "-1e30",
            "description": "proton_temperature",
            "label": "",
            "key": false
        },
        {
            "name": "overall_quality",
            "type": "integer",
            "units": "",
            "fill": "-999",
            "description": "Overall sample quality (0 = normal, 1 = suspect, 2 = error)",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1M",
    "x_relations": [
        {
            "id": "solar_wind_plasma_dscovr_PT5M",
            "description": "DSCOVR Faraday Cup Level 2 One Minute Averages, downsampled to 5 minutes",
            "cadence": "PT5M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_dscovr_PT30M",
            "description": "DSCOVR Faraday Cup Level 2 One Minute Averages, downsampled to 30 minutes",
            "cadence": "PT30M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_dscovr_PT3H",
            "description": "DSCOVR Faraday Cup Level 2 One Minute Averages, downsampled to 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_dscovr_PT12H",
            "description": "DSCOVR Faraday Cup Level 2 One Minute Averages, downsampled to 12 hours",
            "cadence": "PT12H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_dscovr_P3D",
            "description": "DSCOVR Faraday Cup Level 2 One Minute Averages, downsampled to 3 days",
            "cadence": "P3D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_dscovr_P10D",
            "description": "DSCOVR Faraday Cup Level 2 One Minute Averages, downsampled to 10 days",
            "cadence": "P10D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''


def dataframe_closure(datatype='f1m'):
    def to_dataframe(t0, t1):
        return dscovr.to_dataframe(t0, t1, datatype)
    return to_dataframe



def store_dscovr_timespan(t0, t1, metadata):
    # Get the necessary info from metadata
    metadata_values = hapi_client.get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    # Info
    info_request = hapi_client.get_info(hapi_server, db_id)

    # Create if the dataset does not exist
    if info_request['status']['code'] == 1406:
        logging.info(
            f"Creating new dataset {db_id} on server {hapi_server}."
        )
        hapi_client.create_dataset(hapi_server, hapi_server_key, metadata)

    if 'plasma' in db_id:
        datatype = 'f1m'
    elif 'mag' in db_id:
        datatype = 'm1m'
    else:
        logging.error(f"Unknown data type in id: {db_id}")
        return False

    filenames = dscovr.download_data(t0, t1, datatype)
    for filename in filenames:
        # Upload to the data store
        logging.info(f"Processing {filename}")
        df = dscovr.nc_to_dataframe(filename)
        df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        df = df[parameters].replace(replace_nan_fill)
        hapi_client.add_data(hapi_server,
                             hapi_server_key,
                             db_id,
                             df)

        hapi_client.resample_lower_cadences(hapi_server,
                                            hapi_server_key,
                                            db_id,
                                            dataframe=dataframe_closure(datatype),
                                            t_first_data=df.index[0],
                                            t_last_data=df.index[-1])

def store_dscovr_latest():
    t1 = pd.Timestamp.utcnow().ceil('D')
    t0 = t1 - pd.to_timedelta(5, 'D')
    store_dscovr_timespan(t0, t1, metadata_plasma)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    metadata_mag = json.loads(metadata_json_mag)
    info_mag = hapi_client.ensure_dataset_info(hapi_server, hapi_server_key, metadata_mag)

    metadata_plasma = json.loads(metadata_json_plasma)
    info_plasma = hapi_client.ensure_dataset_info(hapi_server, hapi_server_key, metadata_plasma)

    arguments = sys.argv[1:]
    if len(arguments) == 0:
        # Default, no command-line options
        store_dscovr_latest()

        schedule.every(6).hours.do(store_dscovr_latest)

        while True:
            try:
                schedule.run_pending()
            except (BaseException) as err:
                logging.error(f"Exception: {err}")
            time.sleep(1)
