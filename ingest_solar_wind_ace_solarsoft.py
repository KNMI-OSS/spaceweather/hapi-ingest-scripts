#!/usr/bin/env python3
import pandas as pd
import requests
from swxtools import download_tools, hapi_client
from swxtools.config import config
from swxtools.access import solarsoft_ace
import os
import schedule
import time
import logging
import numpy as np
import json

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

metadata_json_swepam = '''{
    "id": "solar_wind_plasma_ace_rt",
    "description": "ACE SWEPAM: 1-minute averaged Real-time Bulk Parameters of the Solar Wind Plasma",
    "timeStampLocation": "begin",
    "resourceURL": "https://sohoftp.nascom.nasa.gov/sdb/goes/ace/daily/",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "SWPC.Webmaster@noaa.gov",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "bulk_speed",
            "type": "double",
            "units": "km/s",
            "fill": "-1e30",
            "description": "bulk_speed",
            "label": "",
            "key": false
        },
        {
            "name": "proton_density",
            "type": "double",
            "units": "1/cm3",
            "fill": "-1e30",
            "description": "proton_density",
            "label": "",
            "key": false
        },
        {
            "name": "ion_temperature",
            "type": "double",
            "units": "K",
            "fill": "-1e30",
            "description": "ion_temperature",
            "label": "",
            "key": false
        },
        {
            "name": "status",
            "type": "integer",
            "units": "",
            "fill": "-999",
            "description": "Status: 0 = nominal data, 1 to 8 = bad data record, 9 = no data",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1M",
    "x_relations": [
        {
            "id": "solar_wind_plasma_ace_rt_PT5M",
            "description": "ACE SWEPAM: 1-minute averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 5 minutes",
            "cadence": "PT5M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_ace_rt_PT30M",
            "description": "ACE SWEPAM: 1-minute averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 30 minutes",
            "cadence": "PT30M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_ace_rt_PT3H",
            "description": "ACE SWEPAM: 1-minute averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_ace_rt_PT12H",
            "description": "ACE SWEPAM: 1-minute averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 12 hours",
            "cadence": "PT12H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_ace_rt_P3D",
            "description": "ACE SWEPAM: 1-minute averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 3 days",
            "cadence": "P3D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_ace_rt_P10D",
            "description": "ACE SWEPAM: 1-minute averaged Real-time Bulk Parameters of the Solar Wind Plasma, downsampled to 10 days",
            "cadence": "P10D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                    level=logging.INFO,
                    datefmt="%Y-%m-%d %H:%M:%S")

metadata_swepam = json.loads(metadata_json_swepam)
info_swepam = hapi_client.ensure_dataset_info(hapi_server, hapi_server_key, metadata_swepam)

def store_ace_timespan(t0, t1, metadata):
    # Get the necessary info from metadata
    metadata_values = hapi_client.get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    # Info
    info_request = hapi_client.get_info(hapi_server, db_id)

    # Create if the dataset does not exist
    if info_request['status']['code'] == 1406:
        logging.info(
            f"Creating new dataset {db_id} on server {hapi_server}."
        )
        hapi_client.create_dataset(hapi_server, hapi_server_key, metadata)

    # Upload to the data store
    df = solarsoft_ace.to_dataframe(t0, t1, data_type='ace_swepam_1m')
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    df = df[parameters].replace(replace_nan_fill)
    hapi_client.add_data(hapi_server,
                         hapi_server_key,
                         db_id,
                         df)

    hapi_client.resample_lower_cadences(hapi_server,
                                        hapi_server_key,
                                        db_id,
                                        dataframe=solarsoft_ace.to_dataframe,
                                        t_first_data=df.index[0],
                                        t_last_data=df.index[-1])


def store_ace_latest():
    t1 = pd.Timestamp.utcnow()
    t0 = t1 - pd.to_timedelta(1, 'D').floor('D')
    dates = pd.date_range(t0, t1, freq='D')
    _ = solarsoft_ace.download(t0, t1, data_type='ace_swepam_1m')
    store_ace_timespan(t0, t1, metadata_swepam)


if __name__ == "__main__":
    store_ace_latest()
    schedule.every(15).minutes.do(store_ace_latest)

    while True:
        try:
            schedule.run_pending()
        except (BaseException) as err:
            logging.error(f"Exception: {err}")
        time.sleep(1)
