#!/usr/bin/env python3
import json
import sys
import logging
import schedule
import time
import pandas as pd
from swxtools.access import swpc_rt
from swxtools.dataframe_tools import mark_gaps_in_dataframe
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
    resample_lower_cadences,
)
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json = '''{
    "id": "solar_wind_plasma_rt",
    "description": "Real time solar wind plasma from active satellite",
    "timeStampLocation": "begin",
    "resourceURL": "https://services.swpc.noaa.gov/products/solar-wind/",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "density",
            "type": "double",
            "units": "1/cm3",
            "fill": "-1e30",
            "description": "Density",
            "label": "",
            "key": false
        },
        {
            "name": "speed",
            "type": "double",
            "units": "km/s",
            "fill": "-1e30",
            "description": "Speed",
            "label": "",
            "key": false
        },
        {
            "name": "temperature",
            "type": "double",
            "units": "K",
            "fill": "-1e30",
            "description": "Temperature",
            "label": "",
            "key": false
        },
        {
            "name": "pressure",
            "type": "double",
            "units": "nPa",
            "fill": "-1e30",
            "description": "Pressure",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1M",
    "x_relations": [
        {
            "id": "solar_wind_plasma_rt_PT5M",
            "description": "Real time solar wind plasma from active satellite downsampled to 5 minutes",
            "cadence": "PT5M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_rt_PT30M",
            "description": "Real time solar wind plasma from active satellite downsampled to 30 minutes",
            "cadence": "PT30M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_rt_PT3H",
            "description": "Real time solar wind plasma from active satellite downsampled to 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_rt_PT12H",
            "description": "Real time solar wind plasma from active satellite downsampled to 12 hours",
            "cadence": "PT12H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_rt_P3D",
            "description": "Real time solar wind plasma from active satellite downsampled to 3 days",
            "cadence": "P3D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "solar_wind_plasma_rt_P10D",
            "description": "Real time solar wind plasma from active satellite downsampled to 10 days",
            "cadence": "P10D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''


def store_plasma_file(metadata, filename, resample=True):
    # Get the id, parameters and cadences from the definition
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']
    cadence = metadata_values['cadence']

    logging.info(f"Storing {filename} in data store.")
    df = swpc_rt.json_to_dataframe(filename)
    df = mark_gaps_in_dataframe(
        df,
        nominal_timedelta=pd.to_timedelta(cadence),
        nominal_start_time=None,
        nominal_end_time=df.index[-1]+pd.to_timedelta(cadence)
    )
    df['time'] = df.index.strftime(ISO_TIMEFMT)
    df['pressure'] = 1.6726e-6 * df['density'] * df['speed'] * df['speed']

    data = df[parameters].replace(replace_nan_fill)

    add_data(hapi_server,
             hapi_server_key,
             db_id,
             data)

    if resample:
        resample_lower_cadences(hapi_server,
                                hapi_server_key,
                                db_id,
                                t_first_data=df.index[0],
                                t_last_data=df.index[-1])


def store_plasma(metadata, duration='5-minute'):
    filenames = swpc_rt.download_data(datatype='plasma', duration=duration)
    for filename in filenames:
        store_plasma_file(metadata=metadata, filename=filename)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    metadata = json.loads(metadata_json)
    info = ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    arguments = sys.argv[1:]
    if len(arguments) == 0:
        # Default, no command-line options
        store_plasma(metadata=metadata, duration='7-day')
        # schedule.every(3).minutes.do(store_plasma,
        #                              duration='5-minute',
        #                              metadata=metadata)
        schedule.every(3).minutes.do(store_plasma,
                                      duration='2-hour',
                                      metadata=metadata)
        schedule.every(1).days.do(store_plasma,
                                  duration='7-day',
                                  metadata=metadata)

        while True:
            try:
                schedule.run_pending()
            except (BaseException) as err:
                logging.error(f"Exception: {err}")
            time.sleep(1)

    else:
        for filename in arguments:
            store_plasma_file(metadata=metadata, filename=filename, resample=False)
