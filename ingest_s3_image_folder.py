#!/usr/bin/env python3
import boto3
import os
import json
import pandas as pd
import logging
import argparse
import re
from swxtools.config import config
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
)


def get_command_line_arguments():
    # Instantiate the parser
    parser = argparse.ArgumentParser(description='Scan S3 filenames and upload URL/timetag combinations to the HAPI server')

    # Choices for parameters
    types = [
        'viirs_dnb_nh',
        'viirs_dnb_sh',
        'viirs_dnb_suominpp_swath',
        'viirs_dnb_noaa20_swath',
        'viirs_dnb_noaa21_swath',
        'polar_vis',
        'image_fuv_wic',
        'gold_oi_1356_emission',
        'gold_oi_1356_emission_10000',
        'gold_ibp_model',
        'dmsp_f17_edr_lbhs',
        'dmsp_f18_edr_lbhs',
        'aurora_sightings_grandin_arctics_survey_may2024_nh',
        'aurora_sightings_grandin_arctics_survey_may2024_sh',
    ]

    # Arguments
    parser.add_argument('--t0', '--start',
                        help='Start date/time')
    parser.add_argument('--t1', '--stop',
                        help='Stop date/time')
    parser.add_argument('--type', choices=types, help='Image type')

    args = parser.parse_args()
    return args

args = get_command_line_arguments()

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

boto3.setup_default_session(profile_name='knmi-pub-dta-saml')
bucket_name = 'knmi-spaceweather-viewer-acc'
server = 'https://acc.spaceweather.knmi.nl'
s3_client = boto3.client('s3')

def get_metadata(type):
    metadata_json_template = '''{
        "id": "{id}",
        "description": "{description}",
        "timeStampLocation": "begin",
        "resourceURL": "{resource_url}",
        "resourceID": "{resource_id}",
        "contact": "eelco.doornbos@knmi.nl",
        "contactID": "Eelco Doornbos",
        "parameters": [
            {
                "name": "time",
                "type": "isotime",
                "units": "UTC",
                "fill": "-9999.0",
                "description": "Timestamp of the image",
                "label": "",
                "key": true
            },
            {
                "name": "url",
                "type": "string",
                "units": "",
                "fill": "None",
                "description": "Image URL",
                "label": "",
                "key": false
            }
        ],
        "cadence": "{cadence}"
    }'''

    metadata_json_template_interval = '''{
        "id": "{id}",
        "description": "{description}",
        "timeStampLocation": "begin",
        "resourceURL": "{resource_url}",
        "resourceID": "{resource_id}",
        "contact": "eelco.doornbos@knmi.nl",
        "contactID": "Eelco Doornbos",
        "parameters": [
            {
                "name": "time",
                "type": "isotime",
                "units": "UTC",
                "fill": "-9999.0",
                "description": "Timestamp of start of the first horizontal pixel of the image",
                "label": "",
                "key": true
            },
            {
                "name": "time_stop",
                "type": "isotime",
                "units": "UTC",
                "fill": "-9999.0",
                "description": "Timestamp of end of the last horizontal pixel of the image",
                "label": "",
                "key": false
            },
            {
                "name": "url",
                "type": "string",
                "units": "",
                "fill": "None",
                "description": "Image URL",
                "label": "",
                "key": false
            }
        ],
        "cadence": "{cadence}"
    }'''

    data_type_info = {
        'viirs_dnb_nh': {
            'replacements': {
                "description": "VIIRS DNB images of aurora in the Northern Hemisphere in MLAT/MLT coordinates",
                "id": "viirs_dnb_nh",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of VIIRS DNB data",
                "cadence": "PT50M"
            },
            'prefix': 'data/viirs_dnb_nh/',
            'interval': False
        },
        'viirs_dnb_sh': {
            'replacements': {
                "description": "VIIRS DNB images of aurora in the Southern Hemisphere in MLAT/MLT coordinates",
                "id": "viirs_dnb_sh",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of VIIRS DNB data",
                "cadence": "PT50M"
            },
            'prefix': 'data/viirs_dnb_sh/',
            'interval': False
        },
        'viirs_dnb_suominpp_swath': {
            'replacements': {
                "description": "VIIRS DNB swaths from the SUOMI-NPP satellite",
                "id": "viirs_dnb_suominpp_swath",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of VIIRS DNB data",
                "cadence": "PT6M"
            },
            'prefix': 'data/viirs_dnb_suominpp_swath/',
            'interval': True
        },
        'viirs_dnb_noaa20_swath': {
            'replacements': {
                "description": "VIIRS DNB swaths from the NOAA-20 satellite",
                "id": "viirs_dnb_noaa20_swath",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of VIIRS DNB data",
                "cadence": "PT6M"
            },
            'prefix': 'data/viirs_dnb_noaa20_swath/',
            'interval': True
        },
        'viirs_dnb_noaa21_swath': {
            'replacements': {
                "description": "VIIRS DNB swaths from the NOAA-21 satellite",
                "id": "viirs_dnb_noaa21_swath",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of VIIRS DNB data",
                "cadence": "PT6M"
            },
            'prefix': 'data/viirs_dnb_noaa21_swath/',
            'interval': True
        },
        'polar_vis': {
            'replacements': {
                "description": "POLAR Vis Earth camera images",
                "id": "polar_vis_earth_camera_calibrated_images",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of POLAR data",
                "cadence": "PT50S"
            },
            'prefix': 'data/polar/vis_earth-camera-calibrated/',
            'interval': False
        },
        'image_fuv_wic': {
            'replacements': {
                "description": "IMAGE FUV WIC images",
                "id": "image_fuv_wic_images",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of IMAGE data",
                "cadence": "PT2M"
            },
            'prefix': 'data/image/image_k0_wic/',
            'interval': False
        },
        'gold_oi_1356_emission': {
            'replacements': {
                "description": "GOLD atomic oxygen emission at 135.6 nm quicklook images",
                "id": "gold_oi_1356_emission",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of GOLD data",
                "cadence": "PT20M"
            },
            'prefix': 'data/gold/OI_1356_emission/',
            'interval': False
        },
        'gold_oi_1356_emission_10000': {
            'replacements': {
                "description": "GOLD atomic oxygen emission at 135.6 nm quicklook images",
                "id": "gold_oi_1356_emission_10000",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of GOLD data",
                "cadence": "PT20M"
            },
            'prefix': 'data/gold/OI_1356_emission_10000/',
            'interval': False
        },
        'gold_ibp_model': {
            'replacements': {
                "description": "Stolle et al IBP model from GOLD vantage point",
                "id": "gold_ibpmodel_contours",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of GOLD data and IBP model",
                "cadence": "PT20M"
            },
            'prefix': 'data/gold/ibp_model',
            'interval': False
        },
        'dmsp_f17_edr_lbhs': {
            'replacements': {
                "description": "DMSP F17 SSUSI EDR-Aurora LBHS images in 6-minute intervals",
                "id": "dmspf17_edr_aurora_lbhs_north",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of DMSP SSUSI data",
                "cadence": "PT6M"
            },
            'prefix': 'data/dmspf17_edr_aurora_lbhs_north/',
            'interval': False
        },
        'dmsp_f18_edr_lbhs': {
            'replacements': {
                "description": "DMSP F18 SSUSI EDR-Aurora LBHS images in 6-minute intervals",
                "id": "dmspf18_edr_aurora_lbhs_north",
                "resource_url": "https://spaceweather.knmi.nl/",
                "resource_id": "KNMI-processing of DMSP SSUSI data",
                "cadence": "PT6M"
            },
            'prefix': 'data/dmspf18_edr_aurora_lbhs_north/',
            'interval': False
        },
        'aurora_sightings_grandin_arctics_survey_may2024_nh': {
            'replacements': {
                "description": "Images of aurora survey locations",
                "id": "aurora_sightings_grandin_arctics_survey_may2024_nh",
                "resource_url": "",
                "resource_id": "KNMI-processing of ARCTICS survey data",
                "cadence": "PT5M"
            },
            'prefix': 'data/aurora_sightings_grandin_arctics_survey_may2024_nh/',
            'interval': False
        },
        'aurora_sightings_grandin_arctics_survey_may2024_sh': {
            'replacements': {
                "description": "Images of aurora survey locations",
                "id": "aurora_sightings_grandin_arctics_survey_may2024_sh",
                "resource_url": "",
                "resource_id": "KNMI-processing of ARCTICS survey data",
                "cadence": "PT5M"
            },
            'prefix': 'data/aurora_sightings_grandin_arctics_survey_may2024_sh/',
            'interval': False
        }
    }

    # Replace placeholders in metadata
    replacements = data_type_info[type]['replacements']
    interval = data_type_info[type]['interval']
    if interval:
        metadata_json = metadata_json_template_interval
    else:
        metadata_json = metadata_json_template
    for key in replacements:
        metadata_json = metadata_json.replace(f"{{{key}}}", replacements[key])
    metadata = json.loads(metadata_json)
    return data_type_info[type]['prefix'], metadata, interval


def s3_full_object_list(bucket_name, prefix):
    truncated = True
    continuation_token = ""
    files_in_bucket = []
    while truncated:
        if len(continuation_token) > 0:
            s3_obj_list = s3_client.list_objects_v2(
                Bucket=bucket_name,
                Prefix=prefix,
                ContinuationToken=continuation_token
            )
        else:
            s3_obj_list = s3_client.list_objects_v2(
                Bucket=bucket_name,
                Prefix=prefix
            )
        if 'Contents' not in s3_obj_list:
            return files_in_bucket
        files_in_bucket.extend([obj['Key'] for obj in s3_obj_list['Contents']])
        if s3_obj_list['IsTruncated']:
            continuation_token = s3_obj_list["NextContinuationToken"]
        else:
            truncated = False
    return files_in_bucket


prefix, metadata, interval = get_metadata(type=args.type)

# Set up the list of dates
t0 = args.t0
t1 = args.t1
date_list = pd.date_range(t0, t1, freq='1D', tz='utc')

files_in_bucket = s3_full_object_list(bucket_name, prefix)

urls = []
times = []
times_stop = []
for file in files_in_bucket:
    basename = os.path.basename(file)
    if interval:
        datetime_match = re.search(r'(\d*T\d*)_(\d*T\d*)', basename)
        timestrs = datetime_match.groups()
        time = pd.to_datetime(timestrs[0], utc=True)
        time_stop = pd.to_datetime(timestrs[1], utc=True)
    else:
        datetime_match = re.search(r'\d*T\d*', basename)
        timestr = datetime_match.group()
        time = pd.to_datetime(timestr, utc=True)
    if time >= date_list[0] and time <= date_list[-1]:
        urls.append(f"{server}/{file}")
        times.append(time.strftime(ISO_TIMEFMT))
        if interval:
            times_stop.append(time_stop.strftime(ISO_TIMEFMT))

if interval:
    df = pd.DataFrame(data={'time': times,
                            'time_stop': times_stop,
                            'url': urls})
    print(df)
else:
    df = pd.DataFrame(data={'time': times,
                            'url': urls})

# Upload the list of files and time tags
metadata_values = get_info_values(metadata)
db_id = metadata_values['id']

ensure_dataset_info(hapi_server, hapi_server_key, metadata)

add_data(hapi_server,
         hapi_server_key,
         db_id,
         df)
