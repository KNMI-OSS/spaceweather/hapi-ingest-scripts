#!/usr/bin/env python3
from spacepy import pycdf
from swxtools import bitmap_tools
from swxtools import download_tools
import os
import numpy as np
import pandas as pd
import logging


logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                    level=logging.INFO,
                    datefmt="%Y-%m-%d %H:%M:%S")

# t0 = pd.to_datetime("2000-04-25", utc=True)
# t1 = pd.to_datetime("2005-12-19", utc=True)
t0 = pd.to_datetime("2003-09-02", utc=True)
t1 = pd.to_datetime("2004-01-01", utc=True)
dates = pd.date_range(t0, t1, freq='1D')

base_url = 'https://cdaweb.gsfc.nasa.gov/pub/data/image/fuv/wic_k0/'
local_data_dir = '/Volumes/datasets/image/'
output_dir = '{local_data_dir}pngs/image/fuv/wic/'
download_tools.ensure_data_dir(local_data_dir)
for date in dates:
    cdf_filename = f'im_k0_wic_{date.strftime("%Y%m%d")}_v01.cdf'
    cdf_filepath = f'{local_data_dir}/{date.strftime("%Y")}'
    cdf_fullpath = f'{cdf_filepath}/{cdf_filename}'
    full_url = f'{base_url}/{date.strftime("%Y")}/{cdf_filename}'

    # Download the file if it does not exist
    if not os.path.isfile(cdf_fullpath):
        # Create download directory if it does not exist
        download_tools.ensure_data_dir(cdf_filepath)
        # Download file
        print(f"Downloading {cdf_filename}")
        download_tools.download_file_http(full_url, cdf_fullpath)

    # Now process the file if it exists
    if os.path.isfile(cdf_fullpath):
        print(f"Processing {cdf_filename}")
        data = pycdf.CDF(cdf_fullpath)
        n_images = data['WIC_PIXELS'].shape[0]
        for i in range(n_images):
            # Set the filename of the PNG
            epoch = data['EPOCH'][i]
            png_filename = f'im_k0_wic_{epoch.strftime("%Y%m%dT%H%M%S")}.png'
            png_dir = f'{local_data_dir}/pngs/{epoch.strftime("%Y")}/{epoch.strftime("%Y%m%d")}/'
            download_tools.ensure_data_dir(png_dir)

            # Create the PNG
            plt_data = data['WIC_PIXELS'][i,:,:].astype(float)
            unique = np.unique(plt_data)
            unique_range = unique.max() - unique.min()

            if len(unique) > 500 and unique_range > 500:
                min_bright = 0
                max_bright = np.percentile(data['WIC_PIXELS'][i,:,:],98)
                png_fullpath = f'{png_dir}{png_filename}'
            else:
                min_bright = unique.min()
                max_bright = unique.max()
                png_fullpath = f'{png_dir}../../bad_data/{png_filename}'

            if os.path.isfile(png_fullpath):
                continue
            plt_data[plt_data < 0] = max_bright
            print(png_fullpath)
            tmpfile = './image.png'
            bitmap_tools.array_to_colormap_png(plt_data, tmpfile, 'grey', min_bright, max_bright)
            os.system(f"pngquant {tmpfile} --ext -quant.png && mv ./image-quant.png {png_fullpath}")

