#!/usr/bin/env python3
import sys
import json
import time
import requests.exceptions
import logging
import schedule
import pandas as pd
from swxtools.access import swpc_geomagnetic_forecast
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
    resample_lower_cadences,
    APIError
)
from swxtools.config import config
from swxtools.dataframe_tools import mark_gaps_in_dataframe

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json = '''{
    "id": "kp_index_noaa_geomag_forecast",
    "description": "Daily geomagnetic Kp index forecast from NOAA for next three days",
    "timeStampLocation": "begin",
    "resourceURL": "https://services.swpc.noaa.gov/products/",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time of forecasted epoch",
            "label": "",
            "key": true
        },
        {
            "name": "timetag_issue",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time of forecast issue",
            "label": "",
            "key": true
        },
        {
            "name": "kp_index",
            "type": "double",
            "units": "",
            "fill": "-1e30",
            "description": "Geomagnetic Kp index",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT3H"
}'''


def store_swpc_geomagnetic_forecast_file(metadata, filename):
    # Get the id, parameters and cadences from the definition
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    # replace_nan_fill = metadata_values['replace_nan_fill']
    # cadence = metadata_values['cadence']

    df = swpc_geomagnetic_forecast.to_dataframe(filename)
    df['timetag_issue'] = df['timetag_issue'].dt.strftime(ISO_TIMEFMT)
    df['time'] = df['timetag_forecast'].dt.strftime(ISO_TIMEFMT)
    data = df[parameters]
    add_data(hapi_server,
             hapi_server_key,
             db_id,
             data)


def store_swpc_geomagnetic_forecast_current(metadata):
    filenames = swpc_geomagnetic_forecast.download_current()
    for filename in filenames:
        store_swpc_geomagnetic_forecast_file(metadata, filename)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    metadata = json.loads(metadata_json)
    info = ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    arguments = sys.argv[1:]
    if len(arguments) == 0:
        # Default, no command-line options
        store_swpc_geomagnetic_forecast_current(metadata)

        schedule.every().day.at("22:10", "UTC").do(
            store_swpc_geomagnetic_forecast_current,
            metadata=metadata
        )
        schedule.every().day.at("00:03", "UTC").do(
            store_swpc_geomagnetic_forecast_current,
            metadata=metadata
        )

        while True:
            try:
                schedule.run_pending()
            except (BaseException) as err:
                logging.error(f"Exception: {err}")
            time.sleep(1)

    else:
        for filename in arguments:
            store_swpc_geomagnetic_forecast_file(metadata=metadata, filename=filename)
