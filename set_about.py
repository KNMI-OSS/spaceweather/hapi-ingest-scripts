#!/usr/bin/env python3
import requests
import json
from swxtools.config import config


hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

about = '''{
    "id": "KNMI",
    "title": "KNMI public HAPI server for use with the space weather timeline viewer",
    "contact": "Eelco Doornbos",
    "description": "Data on this server has been obtained from a variety of sources, and reproduced based on open licenses and/or with express permission of the data owners. It is intended for quick-look visualization with the space weather timeline viewer only. For further analysis, users are advised to retrieve copies of the data from the data owner's repositories.",
    "contactID": "eelco.doornbos@knmi.nl"
}'''

json_about = json.loads(about)
# print(json_about)

url = f'{hapi_server}/api/about?key={hapi_server_key}'
# print(url)
r = requests.post(url, json=json_about)
print(r.content)
