#!/usr/bin/env python3
import xarray as xr
import netCDF4
import os
import numpy as np
import pandas as pd
import pygmt
import apexpy
import glob
from swxtools import bitmap_tools
from swxtools.download_tools import ensure_data_dir
from swxtools import download_tools
import logging

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                    level=logging.INFO,
                    datefmt="%Y-%m-%d %H:%M:%S")

satnum = 18

download = False
if download:
    # Download data
    base_url = f"https://spdf.gsfc.nasa.gov/pub/data/dmsp/dmspf{satnum}/ssusi/data/sdr-disk/"
    for date in pd.date_range("2023-11-01", "2024-01-01", freq='1D'):
        sub_url = f'{date.strftime("%Y")}/{date.strftime("%j")}/'
        print(sub_url)
        download_tools.mirror(
            base_url=base_url,
            sub_url=sub_url,
            local_dir=f'/Volumes/datasets/dmsp/dmspf{satnum}/ssusi/data/sdr-disk/',
            include_strings=None,
            exclude_strings=None,
            esa_eo_timespan=None,
            username='anonymous',
            password='',
        )

# landmaskfile = 'landmask.grd'
# if not os.path.isfile(landmaskfile):
#     pygmt.grdlandmask(outgrid=landmaskfile, region='g', spacing=0.1, area_thresh='100000/0/1')
#     landmask = xr.load_dataarray(landmaskfile)
# landmask = xr.load_dataarray('landmask.grd')

channel_shortnames = ['proton', 'O1304', 'O1356', 'LBHS', 'LBHL']
channel_titles = ['Proton 121.6', 'O 130.4', 'O 135.6', 'N2 LBHS (140-150nm)', 'N2 LBHL (165-180nm)']

outputdir = f'/Volumes/datasets/dmsp/dmspf{satnum}/ssusi/timeline_viewer_images/dmspf{satnum}_sdr_swath_lbhs/'
filelist = glob.glob(f"/Volumes/datasets/dmsp/dmspf{satnum}/ssusi/data/sdr-disk/202*/*/*.nc")
a = apexpy.Apex(refh=110)
pixelfilename = 'pixel.nc'
projfilename = 'proj.nc'

for filename in sorted(filelist):
    for icolor in [3]:  # range(3, 5):
        xrdata = xr.open_dataset(filename)

        times = pd.to_datetime((xrdata['YEAR_DAY']*1e3 + xrdata['DOY_DAY']).data, format='%Y%j', utc=True) + pd.to_timedelta(xrdata['TIME_DAY'], 's')
        time0 = times[0]
        time1 = times[-1]
        year = times[0].year
        doy = times[0].day_of_year
        yymmdd = times[0].strftime("%Y%m%d")
        path = f'{outputdir}/{year}/{yymmdd}'
        ensure_data_dir(path)

        pngfile = f'{path}/dmspf{satnum}_ssusi_{channel_shortnames[icolor].lower()}_swath_{time0.strftime("%Y%m%dT%H%M%S")}_{time1.strftime("%Y%m%dT%H%M%S")}.png'
        if os.path.isfile(pngfile):
            print(f"{pngfile} already exists. Skipping...")
            xrdata.close()
            continue
        else:
            print()
            print(f"Creating {pngfile}")
            print()
        mlat, mlt = a.convert(xrdata['PIERCEPOINT_DAY_LATITUDE_AURORAL'].data, xrdata['PIERCEPOINT_DAY_LONGITUDE_AURORAL'][:].data, 'geo', 'mlt', datetime=time0)
        xrdata['mlat'] = xr.DataArray(data=mlat)
        xrdata['mlt'] = xr.DataArray(data=mlt)

        bitmap_tools.array_to_colormap_png(
            data=xrdata['DISK_RECTIFIED_INTENSITY_DAY_AURORAL'][:, :, icolor],
            filename='out.png',
            cmapname='turbo',
            zmin=0,
            zmax=10000
        )

        os.system(f"pngquant out.png --ext -quant.png && mv ./out-quant.png {pngfile}")

        #land_map = landmask.interp(lon=xrdata['PIERCEPOINT_DAY_LONGITUDE_AURORAL'], lat=xrdata['PIERCEPOINT_DAY_LATITUDE_AURORAL'])

        # fig = pygmt.Figure()
        # pygmt.makecpt(cmap='turbo', series=[0,20000,1])
        # fig.grdimage(xrdata['DISK_INTENSITY_DAY_AURORAL'][:,:,3], projection='X24c/4c', frame=False)
        # # fig.grdimage(xrdata['mlt'][:,:], projection='X24c/5c', frame=True)
        # fig.grdcontour(xrdata['PIERCEPOINT_DAY_SZA_AURORAL'][:,:], pen=['a1p,black@0', 'c0.25p,black@50'], levels=10, annotation='90+u°', label_placement='n1')
        # # fig.grdcontour(xrdata['PIERCEPOINT_DAY_LONGITUDE'])
        # # fig.grdcontour(xrdata['PIERCEPOINT_DAY_LATITUDE'])
        # fig.grdcontour(xrdata['mlat'], pen=['a1p,white@0', 'c0.25p,white@50'], levels=10, annotation='30+u°', label_placement='n1')
        # # fig.grdcontour(xrdata['mlt'], pen=['a1p,cyan@0', 'c0.25p,cyan@50'], levels=1, annotation='3+uh', label_placement='n1')
        # # fig.grdcontour(land_map, interval=0.9, annotation='-', pen='0.25p,white')
        # try:
        #     fig.savefig(pngfile)
        # except pygmt.exceptions.GMTCLibError as e:
        #     print(f"Error saving figure: {e}")
        # xrdata.close()

