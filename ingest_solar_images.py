#!/usr/bin/env python3
import logging
import pandas as pd
import json
import schedule
import time
import argparse
from swxtools import download_tools
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
)
from swxtools.config import config

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                    level=logging.INFO,
                    datefmt="%Y-%m-%d %H:%M:%S")

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"


def get_command_line_arguments():
    # Instantiate the parser
    parser = argparse.ArgumentParser(
        description='Scan SDO filenames and upload URL/timetag ' +
        'combinations to the HAPI server'
    )

    # Choices for parameters
    types = [
        'sdo_hmiic_quicklook',
    ]

    # Arguments
    parser.add_argument('--t0', '--start',
                        help='Start date/time')
    parser.add_argument('--t1', '--stop',
                        help='Stop date/time')
    parser.add_argument('--type', choices=types, help='Image type')

    args = parser.parse_args()
    return args


def get_metadata(data_type):
    metadata_json_template = '''{
        "id": "{id}",
        "description": "{description}",
        "timeStampLocation": "begin",
        "resourceURL": "{resource_url}",
        "resourceID": "{resource_id}",
        "contact": "eelco.doornbos@knmi.nl",
        "contactID": "Eelco Doornbos",
        "parameters": [
            {
                "name": "time",
                "type": "isotime",
                "units": "UTC",
                "fill": "-9999.0",
                "description": "Timestamp of the image",
                "label": "",
                "key": true
            },
            {
                "name": "url",
                "type": "string",
                "units": "",
                "fill": "None",
                "description": "Image URL",
                "label": "",
                "key": false
            }
        ],
        "cadence": "{cadence}"
    }'''

    relation_json_template = """ "object": {
        "id": "{id}_{cadence}",
        "description": "{description}",
        "cadence": "{cadence}",
        "type": "resample",
        "method": "max",
        "add": "automatic"
        }
    """

    data_type_info = {
        "soho_lasco_c3_quicklook": {
            "replacements": {
                "id": "soho_lasco_c3_quicklook",
                "description": "Quick-look coronagraph images from SOHO LASCO C3",
                "cadence": "PT12M",
                "resource_url": "https://soho.nascom.nasa.gov/data/REPROCESSING/Completed",
                "resource_id": "NASA"
            },
            "downsample_factors": [2, 4, 8, 16, 32, 64],
            "url_type": "c3"
        },
        "sdo_aia_0094_quicklook": {
            "replacements": {
                "id": "sdo_aia_0094_quicklook",
                "description": "Quick-look solar images from SDO/AIA 0094",
                "cadence": "PT10M",
                "resource_url": "https://sdo.gsfc.nasa.gov",
                "resource_id": "NASA"
            },
            "downsample_factors": [2, 4, 8, 16, 32, 64],
            "url_type": "0094"
        },
        "sdo_aia_0304_quicklook": {
            "replacements": {
                "id": "sdo_aia_0304_quicklook",
                "description": "Quick-look solar images from SDO/AIA 0304",
                "cadence": "PT10M",
                "resource_url": "https://sdo.gsfc.nasa.gov",
                "resource_id": "NASA"
            },
            "downsample_factors": [2, 4, 8, 16, 32, 64],
            "url_type": "0304"
        },
        "sdo_hmiic_quicklook": {
            "replacements": {
                "id": "sdo_hmiic_quicklook",
                "description": "Quick-look solar images from SDO/HMI",
                "cadence": "PT15M",
                "resource_url": "https://sdo.gsfc.nasa.gov",
                "resource_id": "NASA"
            },
            "downsample_factors": [2, 4, 8, 16, 32, 64],
            "url_type": "HMIIC"
        }

    }
    # Replace placeholders in metadata
    metadata_json = metadata_json_template
    replacements = data_type_info[data_type]['replacements']
    for key in replacements:
        metadata_json = metadata_json.replace(f"{{{key}}}", replacements[key])
    metadata = json.loads(metadata_json)
    if len(data_type_info[data_type]["downsample_factors"]) > 0:
        metadata['x_relations'] = []
        for factor in data_type_info[data_type]["downsample_factors"]:
            new_cadence = factor*pd.to_timedelta(replacements['cadence'])
            new_cadence_str = new_cadence.isoformat()
            metadata['x_relations'].append({
                "id": f"{replacements['id']}_{new_cadence_str}",
                "description": f"{replacements['description']}",
                "cadence": f"{new_cadence_str}",
                "type": "resample",
                "method": "max",
                "add": "automatic"
            })

    return metadata, data_type_info[data_type]['url_type']


def get_quicklook_urls(t0, t1, data_type='c3', resolution=512):
    days = pd.date_range(t0, t1, freq='1D')
    list_of_urls = []
    for day in days:
        year = day.year
        if data_type == 'c3':
            ymd = day.strftime("%Y%m%d")
            base_url = f"https://soho.nascom.nasa.gov/data/REPROCESSING/Completed/{year}/c3/{ymd}/"
            print(base_url)
            list_of_urls_per_day = download_tools.crawl_http(base_url)
            list_of_urls.extend([{'time': pd.to_datetime(item['url'].split('/')[-1][0:13], format='%Y%m%d_%H%M', utc=True).strftime(ISO_TIMEFMT),
                                 'url': item['url']}
                                 for item in list_of_urls_per_day[1]
                                 if item['url'].endswith(f'{data_type}_{resolution}.jpg')])
        elif data_type == '0094' or data_type == '0304' or data_type == 'HMIIC':
            month = day.strftime("%m")
            day = day.strftime("%d")
            base_url = f"https://sdo.gsfc.nasa.gov/assets/img/browse/{year}/{month}/{day}/"
            print(base_url)
            list_of_urls_per_day = download_tools.crawl_http(base_url)
            list_of_urls.extend([{'time': pd.to_datetime(item['url'].split('/')[-1][0:15], format='%Y%m%d_%H%M%S', utc=True).strftime(ISO_TIMEFMT),
                                 'url': item['url']}
                                 for item in list_of_urls_per_day[1]
                                 if item['url'].endswith(f'{resolution}_{data_type}.jpg')])

    df = pd.DataFrame(list_of_urls)
    df.index = pd.to_datetime(df['time'])
    return df


def ingest(image_type, t0, t1):
    db_id = image_type
    metadata, url_type = get_metadata(db_id)
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    df = get_quicklook_urls(t0, t1, data_type=url_type, resolution=512)

    add_data(hapi_server,
             hapi_server_key,
             db_id,
             df)

    for dataset in metadata['x_relations']:
        db_id_resampled = dataset['id']
        cadence = pd.to_timedelta(dataset['cadence'])
        df_new = df.resample(cadence, origin='epoch').nearest()[:-1]
        add_data(
            hapi_server,
            hapi_server_key,
            db_id_resampled,
            df_new
        )


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    ingest(
        image_type='sdo_hmiic_quicklook',
        t0=pd.Timestamp.utcnow()-pd.to_timedelta(15,'D'),
        t1=pd.Timestamp.utcnow()
    )
    schedule.every(30).minutes.do(
        ingest_latest,
        image_type='sdo_hmiic_quicklook',
        t0=pd.Timestamp.utcnow()-pd.to_timedelta(3,'D'),
        t1=pd.Timestamp.utcnow()
    )

    while True:
        try:
            schedule.run_pending()
        except (BaseException) as err:
            logging.error(f"Exception: {err}")
        time.sleep(1)
