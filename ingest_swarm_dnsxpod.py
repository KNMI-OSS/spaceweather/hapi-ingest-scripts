#!/usr/bin/env python3
import pandas as pd
import numpy as np
import os
import sys
import json
import glob
import logging
import schedule
import time
from swxtools.access import swarm_diss
from swxtools.orbit.transforms import interpolate_orbit_to_datetimeindex, itrf_to_geodetic, geodetic_to_qd
from swxtools import hapi_client
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json = '''{
    "id": "sw_oper_dnsapod",
    "description": "Neutral density from the GNSS accelerations of the Swarm A satellite",
    "timeStampLocation": "begin",
    "resourceURL": "https://swarm-diss.eo.esa.int/",
    "resourceID": "ESA",
    "contact": "ESA EO Help",
    "contactID": "ESA EO Help",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-9999.0",
            "description": "Timestamp of the density measurement",
            "label": "",
            "key": true
        },
        {
            "name": "density",
            "type": "double",
            "units": "kg/m3",
            "fill": "-9999.0",
            "description": "Neutral mass density",
            "label": "",
            "key": false
        },
        {
            "name": "density_orbitmean",
            "type": "double",
            "units": "1/cm3",
            "fill": "-9999.0",
            "description": "Mean neutral mass density over one orbital period",
            "label": "",
            "key": false
        },
        {
            "name": "validity_flag",
            "type": "integer",
            "units": "",
            "fill": "-999",
            "description": "Validity flag",
            "label": "",
            "key": false
        },
        {
            "name": "lon",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Geographic longitude",
            "label": "",
            "key": false
        },
        {
            "name": "lat",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Geodetic latitude",
            "label": "",
            "key": false
        },
        {
            "name": "height",
            "type": "double",
            "units": "m",
            "fill": "-9999.0",
            "description": "Geodetic altitude",
            "label": "",
            "key": false
        },
        {
            "name": "lon_qd",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Quasi-dipole longitude",
            "label": "",
            "key": false
        },
        {
            "name": "lat_qd",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Quasi-dipole latitude",
            "label": "",
            "key": false
        },
        {
            "name": "mlt",
            "type": "double",
            "units": "hours",
            "fill": "-9999.0",
            "description": "Quasi-dipole magnetic local time",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT30S",
    "x_relations": [
        {
            "id": "sw_oper_dnsapod_PT2M",
            "description": "Neutral density from the GNSS accelerations of the Swarm A satellite, downsampled to 2 minute cadence",
            "cadence": "PT2M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_dnsapod_PT6M",
            "description": "Neutral density from the GNSS accelerations of the Swarm A satellite, downsampled to 6 minute cadence",
            "cadence": "PT6M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_dnsapod_PT15M",
            "description": "Neutral density from the GNSS accelerations of the Swarm A satellite, downsampled to 15 minute cadence",
            "cadence": "PT15M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        }
    ]
}'''

metadata_json_aggregate = '''{
    "id": "sw_oper_dnsapod_aggregate",
    "description": "Lower cadence aggregate statistics of the thermosphere mass density data from the GNSS accelerations of the Swarm A satellite",
    "timeStampLocation": "begin",
    "resourceURL": "https://swarm-diss.eo.esa.int/",
    "resourceID": "ESA",
    "contact": "ESA EO Help",
    "contactID": "ESA EO Help",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-9999.0",
            "description": "Timestamp",
            "label": "",
            "key": true
        },
        {
            "name": "density_min",
            "type": "double",
            "units": "kg/m3",
            "fill": "-9999.0",
            "description": "Minimum neutral mass density",
            "label": "",
            "key": false
        },
        {
            "name": "density_mean",
            "type": "double",
            "units": "kg/m3",
            "fill": "-9999.0",
            "description": "Mean neutral mass density",
            "label": "",
            "key": false
        },
        {
            "name": "density_max",
            "type": "double",
            "units": "kg/m3",
            "fill": "-9999.0",
            "description": "Maximum neutral mass density",
            "label": "",
            "key": false
        },
        {
            "name": "coverage",
            "type": "double",
            "units": "",
            "fill": "-9999.0",
            "description": "Coverage",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1H30M",
    "x_relations": [
        {
            "id": "sw_oper_dnsapod_aggregate_PT6H",
            "description": "Lower cadence aggregate statistics of the thermosphere mass density data from the GNSS accelerations of the Swarm A satellite",
            "cadence": "PT6H",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_dnsapod_aggregate_P1D",
            "description": "Lower cadence aggregate statistics of the thermosphere mass density data from the GNSS accelerations of the Swarm A satellite",
            "cadence": "P1D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_dnsapod_aggregate_P4D",
            "description": "Lower cadence aggregate statistics of the thermosphere mass density data from the GNSS accelerations of the Swarm A satellite",
            "cadence": "P4D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_dnsapod_aggregate_P16D",
            "description": "Lower cadence aggregate statistics of the thermosphere mass density data from the GNSS accelerations of the Swarm A satellite",
            "cadence": "P16D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_dnsapod_aggregate_P32D",
            "description": "Lower cadence aggregate statistics of the thermosphere mass density data from the GNSS accelerations of the Swarm A satellite",
            "cadence": "P32D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        }
    ]
}'''


def get_metadata(metadata_json, satletter):
    # Update the metadata
    # 1. Set the satellite
    metadata_json_sat = metadata_json.replace(
        'dnsapod',
        f'dns{satletter.lower()}pod'
    )
    metadata_json_sat = metadata_json_sat.replace(
        'Swarm A',
        f'Swarm {satletter.upper()}'
    )

    metadata = json.loads(metadata_json_sat)
    return metadata


def ingest_swarm_dnsxpod(
        satletter,
        metadata,
        t_start=pd.to_datetime("1900-01-01T00:00:00Z", utc=True),
        t_stop=pd.to_datetime("2100-01-01T00:00:00Z", utc=True)):

    # Get the necessary info from metadata
    metadata_values = hapi_client.get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']
    replace_inf_fill = metadata_values['replace_nan_fill']

    # Info
    info_request = hapi_client.get_info(hapi_server, db_id)

    # Create if the dataset does not exist
    if info_request['status']['code'] == 1406:
        logging.info(
            f"Creating new dataset {db_id} on server {hapi_server}."
        )
        hapi_client.create_dataset(hapi_server, hapi_server_key, metadata)

    # Download latest data files
    ftp_conn = swarm_diss.setup_swarm_ftp_conn()
    for data_type in ['MODx_SC', 'DNSxPOD']:
        swarm_diss.download(sat=satletter.upper(),
                            data_type=data_type,
                            fast=False,
                            ftp_conn=ftp_conn)
    ftp_conn.close()

    # Read the list of already processed/ingested data files
    processed_filelist_file = f'processed_files/{db_id}_processed_files.txt'
    if os.path.isfile(processed_filelist_file):
        with open(processed_filelist_file, 'r') as fh:
            processed_filenames = fh.read().splitlines()
    else:
        processed_filenames = []

    # Initial set up of the boundaries for processing lower cadence data
    t_first_data = pd.to_datetime("2100-01-01T00:00:00", utc=True)
    t_last_data = pd.to_datetime("1900-01-01T00:00:00", utc=True)

    # Get the Langmuir Probe data filenames
    swarm_obj_lp = swarm_diss.SwarmFiles(
        data_type='DNSxPOD',
        sat=satletter.upper(),
        fast=False
    )
    swarm_obj_lp.set_time_interval(t_start, t_stop)
    filelist = swarm_obj_lp.filelist.to_dict(orient='records')

    for i_file, file_info in enumerate(filelist):

        # Skip if file has already been processed
        if file_info['filename'] in processed_filenames:
            continue

        pickle_file = f"{file_info['filename']}.pickle"

        if os.path.isfile(pickle_file):
            swarm_data_complete = pd.read_pickle(pickle_file)
            logging.info(f"Read data from {pickle_file}")
        else:
            # Load the DNSxPOD data
            logging.info(f"Processing: {i_file} {file_info['filename']}")
            swarm_data = swarm_obj_lp.to_dataframe_for_file_index(i_file)

            if len(swarm_data) == 0:
                logging.info("No data found, continuing...")
                continue

            # Get and clean up the orbit data
            swarm_obj_orbit = swarm_diss.SwarmFiles(
                data_type='MODx_SC',
                sat=satletter.upper(),
                fast=False
            )
            swarm_obj_orbit.set_time_interval(
                swarm_data.iloc[0].name - pd.to_timedelta(2, 'min'),
                swarm_data.iloc[-1].name + pd.to_timedelta(2, 'min')
            )
            swarm_orbit = swarm_obj_orbit.to_dataframe()
            if len(swarm_orbit) == 0:
                continue
            swarm_orbit.drop('time_gps', axis=1, inplace=True)
            swarm_orbit = swarm_orbit[~swarm_orbit.index.duplicated()]

            # Interpolate the orbit
            swarm_orbit_interpolated = interpolate_orbit_to_datetimeindex(
                swarm_orbit,
                swarm_data.index
            )

            # Convert to geodetic and quasi-dipole coordinates
            swarm_orbit_geo = itrf_to_geodetic(
                swarm_orbit_interpolated
            )
            swarm_orbit_qd = geodetic_to_qd(
                swarm_orbit_geo
            )

            # Append the orbit data to the Langmuir Probe data
            swarm_data_complete = pd.concat(
                [swarm_data,
                 swarm_orbit_qd[[
                     'lon',
                     'lat',
                     'height',
                     'lon_qd',
                     'lat_qd',
                     'mlt'
                 ]]],
                axis=1
            )

            # Add time-tag as string
            swarm_data_complete['time'] = (
                swarm_data_complete.index.strftime(ISO_TIMEFMT)
            )

            # Store in pickle
            swarm_data_complete.to_pickle(pickle_file)

        # swarm_data_complete = swarm_data_complete.where(swarm_data['validity_flag'] == 0, np.nan)
        swarm_data = swarm_data_complete[parameters].replace(replace_nan_fill)

        # Upload to the data store
        hapi_client.add_data(hapi_server,
                             hapi_server_key,
                             db_id,
                             swarm_data)

        # Append the filename to the list of processed files
        processed_filenames.append(file_info['filename'])
        with open(processed_filelist_file, 'a+') as fh:
            fh.write(file_info['filename'] + '\n')

        # Update the boundaries for processing lower cadence data
        if swarm_data_complete.index[0] < t_first_data:
            t_first_data = swarm_data_complete.index[0]
        if swarm_data_complete.index[-1] > t_last_data:
            t_last_data = swarm_data_complete.index[-1]

        hapi_client.resample_lower_cadences(
            hapi_server,
            hapi_server_key,
            db_id,
            dataframe=dataframe_by_time_closure(satletter, dir='./'),
            t_first_data=swarm_data_complete.index[0],
            t_last_data=swarm_data_complete.index[-1]
        )

    return (t_first_data, t_last_data)


def dataframe_by_time_closure(satletter='A', dir='./'):
    def dataframe_by_time(t0, t1):
        datatype = f'SW_OPER_DNS{satletter.upper()}POD'
        filelist_full = sorted(glob.glob(f'{dir}/{datatype}*.pickle'))
        if len(filelist_full) == 0:
            return pd.DataFrame()
        dfs = []
        for file in filelist_full:
            file_t0 = pd.to_datetime(os.path.basename(file).split('_')[5], utc=True)
            file_t1 = pd.to_datetime(os.path.basename(file).split('_')[6], utc=True)
            if file_t0 <= t1 and file_t1 >= t0:
                dfs.append(pd.read_pickle(file))
                logging.info(f"Reading dataframe from file: {file}")
        df_out = pd.concat(dfs, axis=0).sort_index().drop_duplicates()
        df_out = df_out.where(df_out['validity_flag'] == 0, np.nan)
        return df_out
    return dataframe_by_time


def ingest_swarm_all():
    t_start = pd.to_datetime("2024-10-01T00:00:00", utc=True)
    t_stop = pd.Timestamp.utcnow()
    for satletter in ['A', 'B', 'C']:
        print(f"Processing for satellite {satletter}")
        metadata = get_metadata(metadata_json, satletter)
        _ = ingest_swarm_dnsxpod(satletter, metadata, t_start, t_stop)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    if len(arguments) == 0:
        ingest_swarm_all()
        schedule.every(24).hours.do(ingest_swarm_all)
        while True:
            try:
                schedule.run_pending()
            except (BaseException) as err:
                logging.error(f"Exception: {err}")
            time.sleep(1)

    elif arguments[0] == 'resample':
        satletter = arguments[1]
        t_start = pd.to_datetime(arguments[2], utc=True)
        t_stop = pd.to_datetime(arguments[3], utc=True)
        print(f"Processing lower cadences for satellite {satletter}")
        metadata = get_metadata(metadata_json, satletter, fast=False)
        metadata_values = hapi_client.get_info_values(metadata)
        db_id = metadata_values['id']
        hapi_client.resample_lower_cadences(
            hapi_server,
            hapi_server_key,
            db_id,
            dataframe=dataframe_by_time_closure(satletter, dir='./'),
            t_first_data=t_start,
            t_last_data=t_stop
        )

    elif arguments[0] == 'aggregate':
        satletter = arguments[1]
        t_start = pd.to_datetime(arguments[2], utc=True)
        t_stop = pd.to_datetime(arguments[3], utc=True)
        print(f"Processing lower cadence aggregates for satellite {satletter}")
        metadata = get_metadata(metadata_json_aggregate, satletter)

        info_request = hapi_client.get_info(hapi_server, metadata['id'])
        if info_request['status']['code'] == 1406:
            logging.info(
                f"Creating new dataset {metadata['id']} on server {hapi_server}."
            )
            hapi_client.create_dataset(hapi_server, hapi_server_key, metadata)

        hapi_client.ingest_lower_cadence_aggregates(
            hapi_server,
            hapi_server_key,
            metadata,
            dataframe_by_time_closure(satletter=satletter, dir='./'),
            t0=t_start,
            t1=t_stop,
            high_cadence='PT30S',
            min_completeness=0.9
        )
