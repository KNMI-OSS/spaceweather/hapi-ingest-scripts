def get_metadata(metadata_json, satletter, fast=False):
# Update the metadata
# 1. Set the satellite
metadata_json_sat = metadata_json.replace(
    'efia_lp',
    f'efi{satletter.lower()}_lp'
)
metadata_json_sat = metadata_json_sat.replace(
    'Swarm A',
    f'Swarm {satletter.upper()}'
)

# 2. Change oper to fast data, if requested
if fast:
    metadata_json_sat = metadata_json_sat.replace('oper', 'fast')

metadata = json.loads(metadata_json_sat)
return metadata
