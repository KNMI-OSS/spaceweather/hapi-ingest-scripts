import logging
import os
import requests
from requests.adapters import HTTPAdapter, Retry

# Change the max_retries but also enable a backoff strategy
# which makes requests to all https:// addresses sleep for a period of time
# before retrying (to a total of 5 times)

s = requests.Session()

retries = Retry(total=5, # how many retries
                backoff_factor=0.1, # sleep between retries: [0.05s, 0.1s, 0.2s, 0.4s, ...]
                status_forcelist=[ 500, 502, 503, 504 ]) # force retry if one of these is returned
s.mount('https://', HTTPAdapter(max_retries=retries))
# Use this s later one to download files

def list_wam_ipe_files(root_url, root_dir):
    file_list = [];
    r = requests.get(root_url)
    if r.ok:
        soup = BeautifulSoup(r.text, "html.parser")
        dirs = []
        # Find the URLs in the table
        for pre in soup.find_all('pre'):
            for link in pre.find_all('a'):
                address = link.get('href')
                if address[0:1] != '/' and address[-1] == '/' and address[0:2] != 'wrs': # Directory
                    print(f"{address} is a directory")
                    ensure_data_dir(f"{root_dir}{address}")
                    file_list = file_list + list_wam_ipe_files(f"{root_url}{address}", f"{root_dir}{address}") # Call recursively
                if (address[0:1] != '?' and address[0:1] != '/' and address[-1] != '/' and address[0:2] != 'wrs'): # Get rid of the sorting options and previous directory links
                    url = f"{root_url}{address}"
                    download = False
                    if not os.path.isfile(f"{root_dir}{os.path.basename(url)}"): # Download if file does not exist
                         download = True
                    if download:
                        file_list.append((url, root_dir))

    return file_list



def download_file(url, fname):
    # Check if file already exists
    if not os.path.isfile(fname):
        try:
            # Use the s (Session) mounted at the beginning of this file
            # NOTE the stream=True parameter below
            with s.get(url, stream=True) as r:
                r.raise_for_status()
                # Create directory for new file, if already exists > no problem
                logging.info(f"Downloading {url}")
                os.makedirs(os.path.dirname(fname), exist_ok=True)
                with open(fname, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=8192):
                        f.write(chunk)
        except requests.exceptions.HTTPError as e:
            logging.warning(f"The following HTTP error occurred: {e}")
            pass
    else:
        logging.warning(f"File {fname} already exists")

def log_ip_address():
    ip = s.get('https://api.ipify.org').content.decode('utf8')
    logging.info(f"My public ip address is: {ip}")
