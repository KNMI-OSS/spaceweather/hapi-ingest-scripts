#!/usr/bin/env python3
import pandas as pd
import json
import logging
import astropy.time
from swxtools.orbit import transforms
from swxtools import hapi_client
from swxtools.config import config
from swxtools.access import cosmic2

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                    level=logging.INFO,
                    datefmt="%Y-%m-%d %H:%M:%S")

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json_orbit = '''{
    "id": "cosmic2_leoorb_sat1",
    "description": "Orbit data from the Cosmic-2 satellite 1",
    "timeStampLocation": "begin",
    "resourceURL": "https://data.cosmic.ucar.edu/gnss-ro/cosmic2/nrt/level1b/",
    "resourceID": "UCAR",
    "contact": "",
    "contactID": "",
    "cadence": "PT1M",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-9999.0",
            "description": "Timestamp of the LP measurement",
            "label": "",
            "key": true
        },
        {
            "name": "x_itrf",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "ECEF Cartesian position, X-composition",
            "label": "",
            "key": false
        },
        {
            "name": "y_itrf",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "ECEF Cartesian position, Y-composition",
            "label": "",
            "key": false
        },
        {
            "name": "z_itrf",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "ECEF Cartesian position, Z-composition",
            "label": "",
            "key": false
        },
        {
            "name": "x_gcrs",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "GCRS Cartesian position, X-composition",
            "label": "",
            "key": false
        },
        {
            "name": "y_gcrs",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "GCRS Cartesian position, Y-composition",
            "label": "",
            "key": false
        },
        {
            "name": "z_gcrs",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "GCRS Cartesian position, Z-composition",
            "label": "",
            "key": false
        },
        {
            "name": "lon",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Geographic longitude",
            "label": "",
            "key": false
        },
        {
            "name": "lat",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Geodetic latitude",
            "label": "",
            "key": false
        },
        {
            "name": "height",
            "type": "double",
            "units": "m",
            "fill": "-9999.0",
            "description": "Geodetic altitude",
            "label": "",
            "key": false
        },
        {
            "name": "lon_qd",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Quasi-dipole longitude",
            "label": "",
            "key": false
        },
        {
            "name": "lat_qd",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Quasi-dipole latitude",
            "label": "",
            "key": false
        },
        {
            "name": "mlt",
            "type": "double",
            "units": "hours",
            "fill": "-9999.0",
            "description": "Quasi-dipole magnetic local time",
            "label": "",
            "key": false
        }
    ],
    "x_relations": [
        {
            "id": "cosmic2_leoOrb_sat1_PT6M",
            "description": "Orbit data from the Cosmic-2 satellite 1, downsampled to 6 minute cadence",
            "cadence": "PT6M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "cosmic2_leoOrb_sat1_PT15M",
            "description": "Orbit data from the Cosmic-2 satellite 1, downsampled to 15 minute cadence",
            "cadence": "PT15M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        }
    ]
}'''


metadata_json_vfms = '''{
    "id": "cosmic2_ivm_sat1",
    "description": "Plasma data from the ivmL2 product of Cosmic-2, satellite 1",
    "timeStampLocation": "begin",
    "resourceURL": "https://data.cosmic.ucar.edu/gnss-ro/cosmic2/postProc/Level2",
    "resourceID": "UCAR",
    "contact": "",
    "contactID": "",
    "cadence": "PT1M",
    "parameters": [
  {
        "name": "time",
        "type": "isotime",
        "long_name": "Time",
        "units": "UTC",
        "fill": "-9999.0",
        "label": "",
        "key": true
      },
      {
        "name": "ion_dens",
        "type": "double",
        "long_name": "Ion density",
        "units": "1/cm3",
        "fill": "-9999.0",
        "label": "",
        "key": false
      },
      {
        "name": "orig_ion_density",
        "type": "double",
        "units": "1/cm3",
        "long_name": "orig_ion_density",
        "fill": "-9999.0",
        "label": "",
        "key": false
      },
      {
        "name": "sc_flag",
        "type": "integer",
        "units": "",
        "long_name": "S/C events flag",
        "fill": "-9999",
        "label": "",
        "key": false
      },
      {
        "name": "rpa_flag",
        "type": "integer",
        "long_name": "Flag for RPA quality",
        "units": "",
        "fill": "-9999",
        "label": "",
        "key": false
      },
      {
        "name": "dm_flag",
        "type": "integer",
        "long_name": "Flag for DM quality",
        "units": "",
        "fill": "-9999",
        "label": "",
        "key": false
      },
      {
        "name": "alt",
        "type": "double",
        "long_name": "Geographic Altitude (WGS-84)",
        "units": "km",
        "fill": "-9999.0",
        "label": "",
        "key": false
      },
      {
        "name": "lat",
        "type": "double",
        "long_name": "Geographic latitude (WGS-84)",
        "units": "deg",
        "fill": "-9999.0",
        "label": "",
        "key": false
      },
      {
        "name": "mlt",
        "type": "double",
        "long_name": "Magnetic local time",
        "units": "h",
        "fill": "-9999.0",
        "label": "",
        "key": false
      },
      {
        "name": "lon",
        "type": "double",
        "long_name": "Geographic longitude (WGS-84)",
        "units": "deg",
        "fill": "-9999.0",
        "label": "",
        "key": false
      },
      {
        "name": "mlat",
        "type": "double",
        "long_name": "Magnetic latitude",
        "units": "deg",
        "fill": "-9999.0",
        "label": "",
        "key": false
      },
      {
        "name": "apex_lon",
        "type": "double",
        "long_name": "Apex longitude",
        "units": "deg",
        "fill": "-9999.0",
        "label": "",
        "key": false
      },
      {
        "name": "apex_height",
        "type": "double",
        "long_name": "Apex height",
        "units": "km",
        "fill": "-9999.0",
        "label": "",
        "key": false
      }
    ],
    "cadence": "PT1S",
    "x_relations": [
        {
            "id": "cosmic2_ivm_sat1_PT3S",
            "description": "Plasma data from the ivmL2 product of Cosmic-2, downsampled to 3 second cadence",
            "cadence": "PT2S",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "cosmic2_ivm_sat1_PT8S",
            "description": "Plasma data from the ivmL2 product of Cosmic-2, downsampled to 8 second cadence",
            "cadence": "PT8S",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "cosmic2_ivm_sat1_PT30S",
            "description": "Plasma data from the ivmL2 product of Cosmic-2, downsampled to 30 second cadence",
            "cadence": "PT30S",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "cosmic2_ivm_sat1_PT2M",
            "description": "Plasma data from the ivmL2 product of Cosmic-2, downsampled to 2 minute cadence",
            "cadence": "PT2M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "cosmic2_ivm_sat1_PT6M",
            "description": "Plasma data from the ivmL2 product of Cosmic-2, downsampled to 6 minute cadence",
            "cadence": "PT6M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "cosmic2_ivm_sat1_PT15M",
            "description": "Plasma data from the ivmL2 product of Cosmic-2, downsampled to 15 minute cadence",
            "cadence": "PT15M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        }
    ]
}'''


def get_metadata(metadata_json, satnumber):
    # Update the metadata
    # 1. Set the satellite
    metadata_json_sat = metadata_json.replace(
        'sat1',
        f'sat{satnumber}'
    )
    metadata_json_sat = metadata_json_sat.replace(
        'satellite 1',
        f'satellite {satnumber}'
    )
    metadata = json.loads(metadata_json_sat)
    return metadata


def gps_to_utc(date_time_index):
    gps_epoch_tai = astropy.time.Time("1980-01-06T00:00:00", scale='tai')
    gps_epoch_utc = astropy.time.Time("1980-01-06T00:00:00", scale='utc')

    return pd.to_datetime(
            (gps_epoch_utc + (astropy.time.Time(date_time_index, scale='tai') -
             gps_epoch_tai)).iso, utc=True
        )


t0 = pd.to_datetime("20210101", utc=True)
t1 = pd.to_datetime("20220101", utc=True)

data_type = "leoOrb"
#cosmic2.download(data_type=data_type, t0=t0, t1=t1)

for sat in list(range(3, 7)):
    print(f"Satellite {sat}")
    if data_type == 'leoOrb':
        metadata = get_metadata(metadata_json_orbit, satnumber=sat)
    else:
        metadata = get_metadata(metadata_json_vfms, satnumber=sat)
    hapi_client.ensure_dataset_info(hapi_server, hapi_server_key, metadata)
    metadata_values = hapi_client.get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    for date0 in pd.date_range(t0, t1, freq='1D'):
        print(date0)
        date1 = date0 + pd.to_timedelta(1, 'D')
        df = cosmic2.to_dataframe(
            sat=sat,
            data_type=data_type,
            t0=date0,
            t1=date1
        )
        if len(df) == 0:
            continue
        if data_type == 'leoOrb':
            orbit_igrs = transforms.itrs_to_igrs(df)
            orbit_geo = transforms.itrf_to_geodetic(orbit_igrs)
            orbit_qd = transforms.geodetic_to_qd(orbit_geo)
            orbit_qd = orbit_qd.drop([
                'time_gps',
                'vx_itrf',
                'vy_itrf',
                'vz_itrf',
                'vx_gcrs',
                'vy_gcrs',
                'vz_gcrs'], axis=1)
            orbit_qd['time'] = (
                orbit_qd.index.strftime(ISO_TIMEFMT)
            )
            df_out = orbit_qd
        else:
            df.index = gps_to_utc(df.index)
            df['time'] = df.index.strftime(ISO_TIMEFMT)
            df['mlt'] = df['mlt']/pd.to_timedelta(1, 'h')
            df_out = df[parameters].replace(replace_nan_fill)

        hapi_client.add_data(
            hapi_server,
            hapi_server_key,
            db_id,
            df_out
        )

        hapi_client.resample_lower_cadences(
            hapi_server,
            hapi_server_key,
            db_id,
            dataframe=df_out,
            t_first_data=df_out.index[0],
            t_last_data=df_out.index[-1],
            set_cols_fill=['sca_flag', 'dm_flag', 'rpa_flag']
        )
