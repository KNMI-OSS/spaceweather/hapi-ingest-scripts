#
# This file is part of hapi-ingest-scripts
#
# https://gitlab.com/KNMI-OSS/spaceweather/hapi-ingest-scripts
#
# Copyright (c) 2024 KNMI
# All Rights Reserved
"""Process ICAO events using swxtools and upload them to the hapi server."""

import requests
import plotly.express as px
import json
import numpy as np
import pandas as pd
import argparse
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
    resample_lower_cadences,
)
from swxtools.config import config
import swxtools.access.icao_events as swxtools_icao
# from geojson import Polygon
from os import walk

# - global parameters ------------------------------
hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

# Time format for timeline viewer
ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

# Json metadata for all event types
metadata_json_icao_gnss = '''{
    "id": "gnss_alerts_icao",
    "description": "GNSS alerts from ICAO",
    "timeStampLocation": "begin",
    "resourceURL": "https://gitlab.com/KNMI/rdsw/spaceweather/swx_icao_advisory_statistics/-/blob/main/s3/icao_advisories.csv",
    "resourceID": "ICAO",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "message_code",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message Code",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        },
        {
            "name": "severity",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Severity",
            "label": "",
            "key": false
        },
        {
            "name": "coordinate_set",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Coordinate set",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

metadata_json_icao_hf = '''{
    "id": "hf_alerts_icao",
    "description": "HF COM alerts from ICAO",
    "timeStampLocation": "begin",
    "resourceURL": "https://gitlab.com/KNMI/rdsw/spaceweather/swx_icao_advisory_statistics/-/blob/main/s3/icao_advisories.csv",
    "resourceID": "ICAO",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "message_code",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message Code",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        },
        {
            "name": "severity",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Severity",
            "label": "",
            "key": false
        },
        {
            "name": "coordinate_set",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Coordinate set",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

metadata_json_icao_rad = '''{
    "id": "rad_alerts_icao",
    "description": "Radiation alerts from ICAO",
    "timeStampLocation": "begin",
    "resourceURL": "https://gitlab.com/KNMI/rdsw/spaceweather/swx_icao_advisory_statistics/-/blob/main/s3/icao_advisories.csv",
    "resourceID": "ICAO",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "message_code",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message Code",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        },
        {
            "name": "severity",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Severity",
            "label": "",
            "key": false
        },
        {
            "name": "coordinate_set",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Coordinate set",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''


def argument_parser() -> dict:
    """Process the command line arguments to settings for the program

    Parameters
    ----------
    None

    Returns
    -------
    settings_dict : dict
        A dictionary with values for the different program settings.
    """

    # Set up parser
    parser = argparse.ArgumentParser()

    ISO_TIMEFMT_PARSER = "%Y-%m-%d"

    # Choices for parameters

    notif_sets = ['ALL', 'RE', 'AR', 'TEST']
    event_types = ['ALL', 'GNSS', 'HF', 'RAD']

    # Add potential parser arguments
    parser.add_argument('--t0', '--start',
                        help='Start date/time, Y-m-d format')
    parser.add_argument('--t1', '--stop',
                        help='Stop date/time, Y-m-d format')

    parser.add_argument('--sets', choices=notif_sets, nargs='+', help='Choose which data sets to use')
    parser.add_argument('--types', choices=event_types, nargs='+', help='Choose which event types to process')

    parser.add_argument("--map", help="produce a coordinate map of the processed ICAO notifications")
    parser.add_argument("--interp", help="interpolate points between coordinates of the map")
    parser.add_argument("--upload", help="upload the processed NOAA notifications")

    # Retrieve arguments from command line
    args = parser.parse_args()
    settings_dict = {}

    # Save arguments to a usable dict
    if 'RE' in args.sets or 'ALL' in args.sets:
        settings_dict['recent'] = True
    else:
        settings_dict['recent'] = False
    if 'AR' in args.sets or 'ALL' in args.sets:
        settings_dict['archive'] = True
    else:
        settings_dict['archive'] = False
    if 'TEST' in args.sets or 'ALL' in args.sets:
        settings_dict['test'] = True
    else:
        settings_dict['test'] = False

    if 'GNSS' in args.types or 'ALL' in args.types:
        settings_dict['gnss'] = True
    else:
        settings_dict['gnss'] = False
    if 'HF' in args.types or 'ALL' in args.types:
        settings_dict['hf'] = True
    else:
        settings_dict['hf'] = False
    if 'RAD' in args.types or 'ALL' in args.types:
        settings_dict['rad'] = True
    else:
        settings_dict['rad'] = False

    if args.map == 'y':
        settings_dict['map'] = True
    else:
        settings_dict['map'] = False

    if args.upload == 'y':
        settings_dict['upload'] = True
    else:
        settings_dict['upload'] = False

    if args.interp == 'y':
        settings_dict['interpolate'] = True
    else:
        settings_dict['interpolate'] = False

    if args.t0 and args.t1:
        settings_dict['start_date'] = args.t0
        settings_dict['end_date'] = args.t1

    return settings_dict


def process_icao(parser_data: dict) -> dict:
    """Processes the ICAO events to upload-ready notifications

    Parameters
    ----------
    parser_data : dict
        A dictionary from the argument parser
        with values for the different program settings.

    Returns
    -------
    notifications : dict
        A dictionary with notification dataframes for each of the event types.
    """

    gnss_dataframes = []
    hf_dataframes = []
    rad_dataframes = []

    # Archive pipeline
    if parser_data['archive']:
        # TODO, no archive available as of yet, uncomment code when this is the case

        print("Start downloading (archive)")
        # swxtools_icao.download_icao_html()
        print("Finished downloading (archive)")

        # Make list of html files
        # read_list = []
        # path = config['local_source_data_path'] + '/icao_events'
        #
        # for root, dirc, files in walk(path):
        #     for FileName in files:
        #         if '.zip' not in FileName:
        #             read_list.append(path + '/' + FileName)

        print("Start parsing (archive)")
        # TODO not yet implemented
        # archive_dataframes = swxtools_icao.parse_icao_html_to_dataframe(read_list, parser_data)
        print("Finished parsing (archive)")

        # Add parser output to input dataframes for uploading
        # if parser_data['gnss']:
        #     gnss_dataframes.append(archive_dataframes['gnss'])
        # if parser_data['hf']:
        #     hf_dataframes.append(archive_dataframes['hf'])
        # if parser_data['rad']:
        #     rad_dataframes.append(archive_dataframes['rad'])

    # Recent data pipeline
    if parser_data['recent']:
        # TODO, no recent data available as of yet, uncomment code when this is the case

        print("Start downloading (recent)")
        # recent_notifications = swxtools_icao.download_icao_recent_json()
        print("Finished downloading (recent)")

        print("Start parsing (recent)")
        # recent_dataframes = swxtools_icao.parse_icao_recent_json_to_dataframe(recent_notifications, parser_data)
        print("Finished parsing (recent)")

        # Add parser output to input dataframes for uploading
        # if parser_data['gnss']:
        #     gnss_dataframes.append(recent_dataframes['gnss'])
        # if parser_data['hf']:
        #     hf_dataframes.append(recent_dataframes['hf'])
        # if parser_data['rad']:
        #     rad_dataframes.append(recent_dataframes['rad'])

    # For now only this test dataset is available
    csv_dict = {}
    if parser_data['test']:
        print("Start parsing test dataset")
        csv_dict = swxtools_icao.parse_icao_csv_to_dict(['icao_advisories.csv'])

    # Convert
    print("Start converting")
    # Fr now only the test csv dict is used as input, expand later
    gnss_notifications_final = swxtools_icao.convert_icao_gnss(csv_dict)
    hf_notifications_final = swxtools_icao.convert_icao_hf(csv_dict)
    rad_notifications_final = swxtools_icao.convert_icao_rad(csv_dict)
    print("Finished converting")

    # Print results
    print("Print results")
    print("GNSS events", gnss_notifications_final)
    print("HF COM events", hf_notifications_final)
    print("RADIATION events", rad_notifications_final)
    print("Finished printing results")

    notifications_dict = {'gnss': gnss_notifications_final,
                          'hf': hf_notifications_final,
                          'rad': rad_notifications_final}

    return notifications_dict


def map_icao(upload_data: dict, parser_data: dict) -> dict:
    """Generate a json list with polygons for coordinates

    Parameters
    ----------
    upload_data : dict
        A dictionary from the process icao function
        with the dataframes of upload-ready ICAO events.
    parser_data : dict
        A dictionary from the argument parser
        with values for the different program settings.

    Returns
    -------
    upload_data : dict
        The same as the input upload data, but with coordinates
        in the correct format for plotting in GeoJSON and with
        possible interpolation coordinates.
    """

    # Empty feature collection for coordinates
    metadata_json_coordinates = {
            'type': "FeatureCollection",
            'features': []
    }

    # Optional counter to cut off code manually after X number of events
    # counter = 0
    for key, value in upload_data.items():
        # Go through each event for each dataset in upload data
        for item in value:
            # Set up structure required for GeoJSON
            coordinate_set = eval(item['coordinate_set'])
            feature_entry = {
                'type': "Feature",
                'properties': {},
                'geometry': {
                    'type': "Polygon",
                    'coordinates': coordinate_set
                }
            }

            # Perform interpolation between coordinates
            if parser_data['interpolate']:
                set_counter = 0
                new_coordinates_total = []
                for set_data in feature_entry['geometry']['coordinates']:
                    new_coordinates = []
                    # Add interpolated coordinates between each coordinate of the set data
                    for i in range(len(set_data) - 1):
                        start_coord = set_data[i]
                        end_coord = set_data[i+1]
                        start_lon, start_lat = start_coord[0], start_coord[1]
                        end_lon, end_lat = end_coord[0], end_coord[1]

                        # Add 8 coordinates in between each data point
                        # TODO perhaps make the number of interpolated coordinates an input variable
                        new_coordinates.append(start_coord)
                        extra_coordinates = 20  # adjust this value to increase/decrease detail in coordinates
                        coordinate_counter = 1
                        while coordinate_counter <= extra_coordinates:
                            new_lon = start_lon + (end_lon - start_lon) * (coordinate_counter / (extra_coordinates + 1))
                            new_lat = start_lat + (end_lat - start_lat) * (coordinate_counter / (extra_coordinates + 1))
                            new_coord = [new_lon, new_lat]
                            new_coordinates.append(new_coord)
                            coordinate_counter += 1
                        if i == len(set_data) - 2:
                            new_coordinates.append(end_coord)
                    # Append coordinates to coordinate list of the set
                    new_coordinates_total.append(new_coordinates)
                    set_counter += 1
                # Add coordinates to final list of the event
                feature_entry['geometry']['coordinates'] = new_coordinates_total

            # Update coordinate set format if it is a multi-polygon
            if len(feature_entry['geometry']['coordinates']) > 1:
                feature_entry['geometry']['type'] = "MultiPolygon"

                coordinate_set_new = []
                for coordinate in feature_entry['geometry']['coordinates']:
                    coordinate_set_new.append([coordinate])
                feature_entry['geometry']['coordinates'] = coordinate_set_new

                # Add to local metadata json coordinates (mostly for test use)
                metadata_json_coordinates['features'].append(feature_entry)

                # Add to actual coordinate set of the event
                item['coordinate_set'] = str(feature_entry)
            else:
                # Add to local metadata json coordinates (mostly for test use)
                metadata_json_coordinates['features'].append(feature_entry)

                # Add to actual coordinate set of the event
                item['coordinate_set'] = str(feature_entry)

            # Cut off coordinate processing at specific ounter to get a short print statement
            # counter += 1
            # if counter > 10:
            #     print('Coordinate data, short print statement:', metadata_json_coordinates)
            #     return upload_data

    return upload_data


def upload_icao(upload_data: dict, parser_data: dict):
    """Upload the ICAO events to the hapi server

    Parameters
    ----------
    upload_data : dict
        A dictionary from the process icao function
        with the dataframes of ICAO events to upload to the hapi server.
    parser_data : dict
        A dictionary from the argument parser
        with values for the different program settings.

    Returns
    -------
    None

    Uploads data to the hapi server
    """

    # Go over processed lists and upload one by one
    for data_type, data_to_upload in upload_data.items():

        # Set metadata for json depending on data type
        if data_type == 'gnss' and parser_data['gnss']:
            metadata = json.loads(metadata_json_icao_gnss)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        elif data_type == 'hf' and parser_data['hf']:
            metadata = json.loads(metadata_json_icao_hf)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        elif data_type == 'rad' and parser_data['rad']:
            metadata = json.loads(metadata_json_icao_rad)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        else:
            print('Data type ', data_type, ' skipped')
            continue

        # Set other settings
        metadata_values = get_info_values(metadata)
        db_id = metadata_values['id']
        parameters = metadata_values['parameters']
        replace_nan_fill = metadata_values['replace_nan_fill']

        # Retrieve alerts and put in dataframe
        notifications = data_to_upload

        # Check whether any notifications are present
        if len(notifications) > 0:
            df = pd.DataFrame(notifications)
            df.index = pd.to_datetime(df['issue_datetime'], format='%Y %b %d %H%M %Z')

            # Convert time to correct format
            df['time'] = df.index.strftime(ISO_TIMEFMT)

            df['begin_time'] = pd.to_datetime(df['begin_time'], format='%Y %b %d %H%M %Z').dt.strftime(
                ISO_TIMEFMT).replace(
                {np.nan: '-999'})
            df['end_time'] = pd.to_datetime(df['end_time'],
                                            format='%Y %b %d %H%M %Z').dt.strftime(ISO_TIMEFMT).replace(
                {np.nan: '-999'})

            data = df[parameters].replace(replace_nan_fill)

            # Potentially print data per row for final check
            # for index, row in data.iterrows():
            #      print(row)

            # Upload to the hapi server
            add_data(hapi_server,
                     hapi_server_key,
                     db_id,
                     data)


if __name__ == "__main__":

    # Argument parser
    parser_output = argument_parser()
    print("Parser output", parser_output)

    # Process ICAO data using swxtools
    process_output = process_icao(parser_output)
    print("Process output", process_output)

    # Produce a map of the processed ICAO data
    if parser_output['map']:
        process_output = map_icao(process_output, parser_output)
        print("Map process output", process_output)

    # Upload processed ICAO data to the server
    if parser_output['upload']:
        upload_icao(process_output, parser_output)
        print("Upload succesful")
