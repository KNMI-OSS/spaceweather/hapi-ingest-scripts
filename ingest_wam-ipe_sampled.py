import os
import pandas as pd
import xarray as xr
from swxtools import hapi_client
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"
metadata_json = {}

metadata_json['swarm_plasma'] = '''{
    "id": "swarm_a_model_noaa_wam-ipe_plasma",
    "description": "Ionospheric plasma parameters from the WAM-IPE model sampled along the Swarm A satellite trajectory",
    "timeStampLocation": "begin",
    "resourceURL": "",
    "resourceID": "ESA",
    "contact": "eelco.doornbos@knmi.nl",
    "contactID": "eelco.doornbos@knmi.nl",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "latitude",
            "type": "double",
            "units": "degrees",
            "fill": "",
            "description": "Latitude",
            "label": "",
            "key": false
        },
        {
            "name": "longitude",
            "type": "double",
            "units": "degrees",
            "fill": "",
            "description": "Longitude",
            "label": "",
            "key": false
        },
        {
            "name": "altitude",
            "type": "double",
            "units": "km",
            "fill": "",
            "description": "Altitude",
            "label": "",
            "key": false
        },
        {
            "name": "electron_density",
            "type": "double",
            "units": "1/cm3",
            "fill": null,
            "description": "Electron density",
            "label": "",
            "key": false
        },
        {
            "name": "electron_temperature",
            "type": "double",
            "units": "K",
            "fill": null,
            "description": "Electron temperature",
            "label": "",
            "key": false
        },
        {
            "name": "VTEC_above",
            "type": "double",
            "units": "TECu",
            "fill": null,
            "description": "Vertical total electron content above the satellite",
            "label": "",
            "key": false
        },
        {
            "name": "eastward_exb_velocity",
            "type": "double",
            "units": "m/s",
            "fill": null,
            "description": "Northward ExB velocity",
            "label": "",
            "key": false
        },
        {
            "name": "northward_exb_velocity",
            "type": "double",
            "units": "m/s",
            "fill": null,
            "description": "Northward ExB velocity",
            "label": "",
            "key": false
        },
        {
            "name": "upward_exb_velocity",
            "type": "double",
            "units": "m/s",
            "fill": null,
            "description": "Upward ExB velocity",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT10S",
    "x_relations": [
        {
            "id": "swarm_a_model_noaa_wam-ipe_plasma_PT30S",
            "description": "Ionospheric plasma parameters from the WAM-IPE model sampled along the Swarm A satellite trajectory downsampled to 30 seconds",
            "cadence": "PT30S",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "swarm_a_model_noaa_wam-ipe_plasma_PT3M",
            "description": "Ionospheric plasma parameters from the WAM-IPE model sampled along the Swarm A satellite trajectory downsampled to 3 minutes",
            "cadence": "PT3M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "swarm_a_model_noaa_wam-ipe_plasma_PT6M",
            "description": "Ionospheric plasma parameters from the WAM-IPE model sampled along the Swarm A satellite trajectory downsampled to 6 minutes",
            "cadence": "PT6M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "swarm_a_model_noaa_wam-ipe_plasma_PT15M",
            "description": "Ionospheric plasma parameters from the WAM-IPE model sampled along the Swarm A satellite trajectory downsampled to 15 minutes",
            "cadence": "PT15M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}
'''

metadata_json['swarm_neutral'] = '''{
    "id": "swarm_a_model_noaa_wam-ipe_plasma",
    "description": "Thermospheric parameters from the WAM-IPE model sampled along the Swarm A satellite trajectory",
    "timeStampLocation": "begin",
    "resourceURL": "",
    "resourceID": "KNMI",
    "contact": "eelco.doornbos@knmi.nl",
    "contactID": "eelco.doornbos@knmi.nl",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "latitude",
            "type": "double",
            "units": "degrees",
            "fill": "",
            "description": "Latitude",
            "label": "",
            "key": false
        },
        {
            "name": "longitude",
            "type": "double",
            "units": "degrees",
            "fill": "",
            "description": "Longitude",
            "label": "",
            "key": false
        },
        {
            "name": "altitude",
            "type": "double",
            "units": "km",
            "fill": "",
            "description": "Altitude",
            "label": "",
            "key": false
        },
        {
            "name": "mass_density",
            "type": "double",
            "units": "kg/m3",
            "fill": null,
            "description": "Neutral mass density",
            "label": "",
            "key": false
        },
        {
            "name": "temp_neutral",
            "type": "double",
            "units": "K",
            "fill": null,
            "description": "Neutral temperature",
            "label": "",
            "key": false
        },
        {
            "name": "u_neutral",
            "type": "double",
            "units": "m/s",
            "fill": null,
            "description": "Zonal neutral wind",
            "label": "",
            "key": false
        },
        {
            "name": "v_neutral",
            "type": "double",
            "units": "m/s",
            "fill": null,
            "description": "Meridional neutral wind",
            "label": "",
            "key": false
        },
        {
            "name": "w_neutral",
            "type": "double",
            "units": "m/s",
            "fill": null,
            "description": "Vertical neutral wind",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT10S",
    "x_relations": [
        {
            "id": "swarm_a_model_noaa_wam-ipe_plasma_PT30S",
            "description": "Thermospheric parameters from the WAM-IPE model sampled along the Swarm A satellite trajectory downsampled to 30 seconds",
            "cadence": "PT30S",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "swarm_a_model_noaa_wam-ipe_plasma_PT3M",
            "description": "Thermospheric parameters from the WAM-IPE model sampled along the Swarm A satellite trajectory downsampled to 3 minutes",
            "cadence": "PT3M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "swarm_a_model_noaa_wam-ipe_plasma_PT6M",
            "description": "Thermospheric parameters from the WAM-IPE model sampled along the Swarm A satellite trajectory downsampled to 6 minutes",
            "cadence": "PT6M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "swarm_a_model_noaa_wam-ipe_plasma_PT15M",
            "description": "Thermospheric parameters from the WAM-IPE model sampled along the Swarm A satellite trajectory downsampled to 15 minutes",
            "cadence": "PT15M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''


def get_metadata(metadata_json, satletter):
    # Update the metadata
    # 1. Set the satellite
    metadata_json_sat = metadata_json.replace(
        'swarm_a_',
        f'swarm_{satletter.lower()}_'
    )
    metadata_json_sat = metadata_json_sat.replace(
        'Swarm A',
        f'Swarm {satletter.upper()}'
    )

    # 2. Change oper to fast data, if requested
    if fast:
        metadata_json_sat = metadata_json_sat.replace('oper', 'fast')

    metadata = json.loads(metadata_json_sat)
    return metadata


def ingest_wam_ipe_sampled_file(filename, metadata):

    # Get the necessary info from metadata
    metadata_values = hapi_client.get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']
    replace_inf_fill = metadata_values['replace_nan_fill']

    # Info
    info_request = hapi_client.get_info(hapi_server, db_id)

    # Create if the dataset does not exist
    if info_request['status']['code'] == 1406:
        logging.info(
            f"Creating new dataset {db_id} on server {hapi_server}."
        )
        hapi_client.create_dataset(hapi_server, hapi_server_key, metadata)

    # Get the data
    with xr.open_dataset(in_file) as xrdata:
        df = xrdata.to_dataframe()

    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
    df['electron_density'] = df['electron_density'] * 1e-6 # Conversion from 1/m3 to 1/cm3

    # swarm_data_complete = swarm_data_complete.where(swarm_data['validity_flag'] == 0, np.nan)
    df_in = df[parameters].replace(replace_nan_fill)

    # Upload to the data store
    hapi_client.add_data(hapi_server,
                         hapi_server_key,
                         db_id,
                         df_in)



input_dir = '/Volumes/datasets/wam-ipe/sampled'
sat = "Swarm C"
satletter = sat[-1:]

parameters = ['latitude', 'longitude', 'altitude', 'electron_density', 'electron_temperature', 'VTEC_above', 'eastward_exb_velocity', 'northward_exb_velocity', 'upward_exb_velocity']
allparameters = ['time', *parameters]

# Initialize the processing time
t0 = pd.to_datetime("2021-11-01", utc=True)
t1 = pd.to_datetime("2021-11-10", utc=True)
days = pd.date_range(t0, t1, freq='1D')

# Loop over the days
for daystart, dayend in zip(days[:], days[1:]):
    in_file = f"{input_dir}/ipe_swarm{satletter.lower()}_{daystart.strftime('%Y%m%d')}.nc"
    if os.path.isfile(in_file):
        print(in_file)
        with xr.open_dataset(in_file) as xrdata:
            df = xrdata.to_dataframe()
            df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
            df['electron_density'] = df['electron_density'] * 1e-6 # Conversion from 1/m3 to 1/cm3
            data = df[allparameters].values.tolist()
            id = f'wam_ipe_ne_swarm{satletter.lower()}_PT10S'
            print("PT10S")
            ingest_tools.store(
                id,
                allparameters,
                data,
                update=True,
                api='api_url'
            )

            # Now process the lower cadences of high-rate (< 1 orbital period) data
            cadences = ['PT30S', 'PT3M', 'PT6M', 'PT15M']
            for cadence in cadences:
                print(cadence)
                df_resampled = df[parameters].resample(pd.to_timedelta(cadence)).mean()
                df_resampled['time'] = df_resampled.index.strftime("%Y-%m-%dT%H:%M:%SZ")
                data = df_resampled[allparameters].values.tolist()
                id = f'wam_ipe_ne_swarm{satletter.lower()}_' + cadence
                ingest_tools.store(
                    id,
                    allparameters,
                    data,
                    update=True,
                    api='api_url'
                    )
