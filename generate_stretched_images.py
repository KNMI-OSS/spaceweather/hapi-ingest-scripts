#!/usr/bin/env python3
import numpy as np
import os
import pandas as pd
import xarray as xr
from swxtools.access import swarm_diss
from swxtools.orbit import transforms
from swxtools.download_tools import ensure_data_dir
import scipy
import logging
from swxtools import bitmap_tools


def get_data(satname, productname, t0, t1):
    t0str = pd.to_datetime(t0).strftime("%Y-%m-%d")
    t1str = pd.to_datetime(t1).strftime("%Y-%m-%d")

    # Load the data
    offset = pd.to_timedelta(1, 'D')
    swarm_obj = swarm_diss.SwarmFiles(productname, sat=satname, processed=True)
    swarm_obj.set_time_interval(t0, pd.to_datetime(t1)+offset)
    logging.info("Reading data...")
    df = swarm_obj.to_dataframe()

    df = df.replace(9.999900e+04, np.nan)
    return df


def get_orbit(satname, orbit_productname, t0, t1):
    # Take a bit of orbit beyond the edges, to be able to determine
    # the time at the edges of the first and last pixels
    offset = pd.to_timedelta(1, 'D')
    t0orbit = pd.to_datetime(t0) - offset
    t1orbit = pd.to_datetime(t1) + offset
    # Load the orbit data
    swarm_obj_orbit = swarm_diss.SwarmFiles(
        orbit_productname, sat=satname, processed=True
    )
    swarm_obj_orbit.set_time_interval(t0orbit, t1orbit)
    logging.info("Reading orbit...")
    df_orbit = swarm_obj_orbit.to_dataframe()
    return df_orbit


def get_arcs_from_orbit(df_orbit):
    t0str = df_orbit.index[0].strftime('%Y-%m-%d')
    t1str = df_orbit.index[-1].strftime('%Y-%m-%d')
    satfilename = satname.lower().replace(" ", "_")
    logging.info("Determining orbit arcs")
    df_arcs = transforms.itrf_orbit_arcs(df_orbit)
    return df_arcs


def compute_lat_polar_annot(lat):
    latgrad = np.gradient(lat)
    use_normal_lat = np.sign(lat) == np.sign(latgrad)
    nh = np.sign(lat) >= 0
    result = lat.where(use_normal_lat, (90+(90-lat)).where(nh, -90-(90+lat)))
    return result


def create_timeline_viewer_heatmap_png(
    t0,
    t1,
    satname,
    which_arcs,
    statistic,
    orbit_productname,
    productname,
    yvar,
    zvar,
    cmapname,
    zmin,
    zmax,
    zstep
):

    if which_arcs == 'NH':
        latmin = 0
        latmax = 180
        step = 0.5
    elif which_arcs == 'SH':
        latmin = -180
        latmax = 0
        step = 0.5
    elif which_arcs == 'A' or which_arcs == 'D':
        latmin = -90
        latmax = 90
        step = 0.5

    df_orbit = get_orbit(satname, orbit_productname, t0, t1)
    df_arcs = get_arcs_from_orbit(df_orbit)

    # Select the arcs
    df_arcs_sel_type = df_arcs[df_arcs['type'] == which_arcs]
    df_arcs_sel_time = df_arcs_sel_type[
        (df_arcs_sel_type.t0 > t0) & (df_arcs_sel_type.t0 < t1)
    ]

    # The start/stop time should point to the edges of the pixel column
    # so that the column centers align with
    t_img_start = df_arcs.loc[df_arcs_sel_time.iloc[0].name-2].tmid
    if df_arcs_sel_time.iloc[-1].name+2 < df_arcs.index[-1]:
        t_img_stop = df_arcs.loc[df_arcs_sel_time.iloc[-1].name+2].tmid
    else:
        t_img_stop = df_arcs.loc[df_arcs_sel_time.iloc[-2].name+2].tmid
    tstartstr = t_img_start.strftime("%Y%m%dT%H%M%S")
    tstopstr = t_img_stop.strftime("%Y%m%dT%H%M%S")

    # Download the data
    df = get_data(satname, productname, t0, t1)
    if productname == 'MAGx_LR':
        df = df.where(np.abs(df['B_NEC_1'])>1.0, np.nan)

    # Compute polar annotations, e.g. going from 0 to 90 and back t0 0
    if which_arcs == 'NH' or which_arcs == 'SH':
        if yvar == 'lat':
            yvar = 'lat_polar_annot'
            df['lat_polar_annot'] = compute_lat_polar_annot(df['lat'])
        elif yvar == 'lat_qd':
            yvar = 'lat_polar_annot_qd'
            df['lat_polar_annot_qd'] = compute_lat_polar_annot(df['lat_qd'])
        else:
            print("Unknown yvar for polar pass")

    # Set up for binning the data per arc in the Y-coordinate
    bin_edges = np.arange(latmin,latmax+1e-9, step)
    bin_mids = bin_edges[:-1] + 0.5*np.diff(bin_edges)
    t_mids = df_arcs_sel_time['tmid'].values
    values = np.empty((len(bin_mids), len(t_mids)))
    values[:,:] = np.nan  # Fill with NaNs for default in case of missing data

    # Gather the data, arc by arc
    for iarc in range(len(df_arcs_sel_time)):
        arc = df_arcs_sel_time.iloc[iarc]
        df_arc = df[
            arc.t0-pd.to_timedelta(10, 'min'):arc.t1+pd.to_timedelta(10, 'min')
        ]
        # Check if there is missing data
        if (len(df_arc) == 0):
            continue
        # Otherwise continue
        stats = scipy.stats.binned_statistic(
            x=df_arc[yvar].values,
            values=df_arc[zvar].values,
            statistic=statistic,
            bins=bin_edges,
            range=None
        )
        values[:, iarc] = stats.statistic

    # Collect in xarray structure and output the image
    xrdata = xr.DataArray(
        data=values,
        coords={
            'modlat_mids': bin_mids,
            'arc_tmids': np.arange(len(t_mids))
        }
    )
    satfilename = "swarm_" + satname.lower()
    dirname = (
        "/Volumes/datasets/swarm/"
        f"{satfilename.lower()}_" +
        f"{productname.lower()}_{which_arcs.lower()}" +
        f"_{yvar.lower()}_{zvar.lower()}_" +
        f"{statistic.lower()}_{cmapname.lower()}_{zmin}_{zmax}/"
    )
    ensure_data_dir(dirname)
    filename = (
        f"{satfilename.lower()}_{productname.lower()}_{which_arcs.lower()}_" +
        f"{yvar.lower()}_{zvar.lower()}_" +
        f"{statistic.lower()}_{cmapname.lower()}_{zmin}_{zmax}_" +
        f"{tstartstr}_{tstopstr}.png"
    )
    output_filepath = f"{dirname}/{filename}"
    bitmap_tools.array_to_colormap_png(
        data=xrdata.data,
        filename=output_filepath,
        cmapname=cmapname,
        zmin=zmin,
        zmax=zmax
    )
    print(output_filepath)
    return xrdata

satname = "B"
which_arcs = 'D'
statistic = 'mean'
orbit_productname = 'MODx_SC'
productname = 'EFIx_LP'
yvar = 'lat_qd'
zvar = 'Ne'
cmapname = 'turbo'
zmin = 0
zmax = 2e6
zstep = (zmax-zmin)/200


# satname = "B"
# which_arcs = 'NH'
# statistic = 'mean'
# orbit_productname = 'MODx_SC'
# productname = 'MAGx_LR'
# yvar = 'lat_qd'
# zvar = 'Delta_B_NEC_1'
# cmapname = 'RdBu_r'
# zmin = -500
# zmax = 500
# zstep = (zmax-zmin)/200
#
#
# satname = "B"
# which_arcs = 'SH'
# statistic = 'mean'
# orbit_productname = 'MODx_SC'
# productname = 'MAGx_LR'
# yvar = 'lat_qd'
# zvar = 'Delta_B_NEC_1'
# cmapname = 'RdBu_r'
# zmin = -500
# zmax = 500
# zstep = (zmax-zmin)/200

for t0 in pd.date_range(pd.to_datetime('2023-01-01T',utc=True), pd.to_datetime('2024-05-31T', utc=True), freq='MS'):
    t1 = (pd.to_datetime(t0) + pd.offsets.MonthBegin(1)).strftime("%Y-%m-%d")
    print(t0, t1)
    xrdata = create_timeline_viewer_heatmap_png(
        t0,
        t1,
        satname,
        which_arcs,
        statistic,
        orbit_productname,
        productname,
        yvar,
        zvar,
        cmapname,
        zmin,
        zmax,
        zstep)