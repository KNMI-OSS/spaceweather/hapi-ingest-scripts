# Ingest scripts for the KNMI HAPI server implementation
This repository contains scripts in use at KNMI to add datasets (data and metadata) to the KNMI HAPI server. The scripts are to be used in combination with a deployed version of the KNMI HAPI server:
https://gitlab.com/KNMI-OSS/spaceweather/knmi-hapi-server

The scripts make extensive use of the Python package swxtools:
https://gitlab.com/KNMI-OSS/spaceweather/swxtools

## Acknowledgements

The code has been produced with support from ESA through the Swarm Data Innovation and Science Cluster (Swarm DISC). For more information on Swarm DISC, please visit https://earth.esa.int/eogateway/activities/swarm-disc
