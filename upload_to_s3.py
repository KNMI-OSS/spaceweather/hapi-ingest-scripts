#!/usr/bin/env python3
import logging
import boto3
from botocore.exceptions import ClientError
import os
import pandas as pd
import logging

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
level=logging.INFO,
datefmt="%Y-%m-%d %H:%M:%S")


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = os.path.basename(file_name)

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        s3_client.upload_file(
            file_name,
            bucket,
            object_name,
            ExtraArgs={
                'ContentType': 'image/png',
                'CacheControl': 'max-age=31536000'
            }
        )
    except ClientError as e:
        logging.error(e)
        return False
    return True


def upload_files(bucket_name, data_type, t0, t1, freq='1D', force=False):
    for date in pd.date_range(t0, t1, freq=freq):
        print(date)
        year = date.strftime("%Y")
        ymd = date.strftime("%Y%m%d")
        if data_type == 'polar_vis_earth_camera':
            source_dir = f'/Volumes/datasets/polar/pngs/{year}/{ymd}/'
            remote_dir = f'data/polar/vis_earth-camera-calibrated/{year}/{ymd}/'

        elif data_type == 'image_k0_wic':
            source_dir = f'/Volumes/datasets/image/pngs/{year}/{ymd}/'
            remote_dir = f'data/image/image_k0_wic/{year}/{ymd}/'

        elif data_type == 'viirs_dnb_nh':
            source_dir = f'/Volumes/datasets/viirs/output/mlt_mlat/small/{ymd}/nh/'
            remote_dir = f'data/viirs_dnb_nh/{ymd}/'

        elif data_type == 'viirs_dnb_sh':
            source_dir = f'/Volumes/datasets/viirs/output/mlt_mlat/small/{ymd}/sh/'
            remote_dir = f'data/viirs_dnb_sh/{ymd}/'

        elif data_type == 'viirs_dnb_suominpp_swath':
            source_dir = f'/Volumes/datasets/viirs/output/swaths_timeline_viewer/SUOMI-NPP/{year}/{ymd}/'
            remote_dir = f'data/viirs_dnb_suominpp_swath/{year}/{ymd}/'

        elif data_type == 'viirs_dnb_noaa20_swath':
            source_dir = f'/Volumes/datasets/viirs/output/swaths_timeline_viewer/NOAA-20/{year}/{ymd}/'
            remote_dir = f'data/viirs_dnb_noaa20_swath/{year}/{ymd}/'

        elif data_type == 'viirs_dnb_noaa21_swath':
            source_dir = f'/Volumes/datasets/viirs/output/swaths_timeline_viewer/NOAA-21/{year}/{ymd}/'
            remote_dir = f'data/viirs_dnb_noaa21_swath/{year}/{ymd}/'

        elif data_type == 'gold':
            source_dir = f'/Volumes/datasets/gold/pngs_timeline_viewer/{year}/{ymd}/'
            remote_dir = f'data/gold/OI_1356_emission/{year}/{ymd}/'

        elif data_type == 'gold_10000':
            source_dir = f'/Volumes/datasets/gold/pngs_timeline_viewer_10000/{year}/{ymd}/'
            remote_dir = f'data/gold/OI_1356_emission_10000/{year}/{ymd}/'

        elif data_type == 'gold_ibpmodel':
            source_dir = f'/Volumes/datasets/gold/pngs_timeline_viewer_ibpmodel/{year}/{ymd}/'
            remote_dir = f'data/gold_ibpmodel/{year}/{ymd}/'

        elif data_type == 'dmspf17_sdr_swath_lbhs':
            source_dir = f'/Volumes/datasets/dmsp/dmspf17/ssusi/timeline_viewer_images/dmspf17_sdr_swath_lbhs/{year}/{ymd}/'
            remote_dir = f'data/dmspf17_sdr_swath_lbhs/{year}/{ymd}/'

        elif data_type == 'dmspf18_sdr_swath_lbhs':
            source_dir = f'/Volumes/datasets/dmsp/dmspf18/ssusi/timeline_viewer_images/dmspf18_sdr_swath_lbhs/{year}/{ymd}/'
            remote_dir = f'data/dmspf18_sdr_swath_lbhs/{year}/{ymd}/'

        elif data_type == 'dmspf17_edr_aurora_lbhs_north':
            source_dir = f'/Volumes/datasets/dmsp/pngs_timeline_viewer/dmspf17/lbhs/north/{year}/{ymd}/'
            remote_dir = f'data/dmspf17_edr_aurora_lbhs_north/{year}/{ymd}/'

        elif data_type == 'dmspf18_edr_aurora_lbhs_north':
            source_dir = f'/Volumes/datasets/dmsp/pngs_timeline_viewer/dmspf18/lbhs/north/{year}/{ymd}/'
            remote_dir = f'data/dmspf18_edr_aurora_lbhs_north/{year}/{ymd}/'

        elif data_type == 'swarm_b_magx_lr_nh_lat_polar_annot_qd_delta_b_nec_1_mean_rdbu_r_-500_500':
            source_dir = '/Volumes/datasets/swarm/swarm_b_magx_lr_nh_lat_polar_annot_qd_delta_b_nec_1_mean_rdbu_r_-500_500/'
            remote_dir = 'data/swarm_b_magx_lr_nh_lat_polar_annot_qd_delta_b_nec_1_mean_rdbu_r_-500_500/'

        elif data_type == 'aurora_sightings_grandin_arctics_survey_may2024_nh':
            source_dir = f'/Users/doornbos/Desktop/may2024_sightings/small/{ymd}/nh/'
            remote_dir = f'data/aurora_sightings_grandin_arctics_survey_may2024_nh/{ymd}/'

        elif data_type == 'aurora_sightings_grandin_arctics_survey_may2024_sh':
            source_dir = f'/Users/doornbos/Desktop/may2024_sightings/small/{ymd}/sh/'
            remote_dir = f'data/aurora_sightings_grandin_arctics_survey_may2024_sh/{ymd}/'

        else:
            logging.error(f"Unknown data type: {data_type}")

        logging.info(f"Processing {source_dir}")

        # Fetch list of already uploaded files
        s3_obj_list = s3_client.list_objects_v2(
            Bucket=bucket_name,
            Prefix=remote_dir
        )
        if s3_obj_list['KeyCount'] > 0:
            files_in_bucket = [obj['Key'] for obj in s3_obj_list['Contents']]
        else:
            print("No files in bucket yet")
            files_in_bucket = []

#        print("Bucket: ", bucket_name, remote_dir)
#        print("In bucket: ", files_in_bucket)

        # Build list of files
        files = []
        for root, _, filenames in os.walk(source_dir):
            for filename in sorted(filenames):
                files.append(os.path.join(root, filename))

        print(f"{len(files)} found locally")

        # Upload files
        for local_file in files:
            if os.path.basename(local_file).startswith('.'):
                continue
            remote_file = local_file.replace(source_dir, remote_dir)
            if remote_file in files_in_bucket:
                if force:
                    logging.info(f"Forced uploading of {remote_file}")
                    upload_file(local_file, bucket_name, remote_file)
                else:
                    logging.info(f"{remote_file} is in bucket. Skipping...")
            else:
                logging.info(f"{remote_file} is not in bucket - uploading")
                upload_file(local_file, bucket_name, remote_file)


logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                    level=logging.INFO,
                    datefmt="%Y-%m-%d %H:%M:%S")

boto3.setup_default_session(profile_name='knmi-pub-dta-saml')
bucket_name = 'knmi-spaceweather-viewer-acc'
s3_client = boto3.client('s3')

upload_files(bucket_name, 'aurora_sightings_grandin_arctics_survey_may2024_sh', "2024-05-01", "2024-06-01", "1D", force=True)
