#!/usr/bin/env python3
from spacepy import pycdf
from swxtools import bitmap_tools
from swxtools import download_tools
import os
import numpy as np
import pandas as pd
import xarray as xr
import pygmt
import logging

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
level=logging.INFO,
datefmt="%Y-%m-%d %H:%M:%S")


landmaskfile = 'landmask.grd'
if not os.path.isfile(landmaskfile):
    pygmt.grdlandmask(outgrid=landmaskfile, region='g', spacing=0.1, area_thresh='100000/0/1')
    landmask = xr.load_dataarray(landmaskfile)
landmask = xr.load_dataarray('landmask.grd')

projection = 'X4.56i/4i'

#t0 = pd.to_datetime("1996-04-30", utc=True)
t0 = pd.to_datetime("2000-08-21", utc=True)

t1 = pd.to_datetime("2008-04-16", utc=True)
dates = pd.date_range(t0, t1, freq='1H')

# po_vis_earth-camera-calibrated_2008041607_v01.cdf

base_url = 'https://cdaweb.gsfc.nasa.gov/pub/data/polar/vis/vis_earth-camera-calibrated/'
local_data_dir = '/Volumes/SanDiskPhotos/datasets/polar/'
output_dir = '{local_data_dir}pngs/polar/vis/vis_earth-camera-calibrated/'
download_tools.ensure_data_dir(local_data_dir)
for date in dates:
    cdf_filename = f'po_vis_earth-camera-calibrated_{date.strftime("%Y%m%d%H")}_v01.cdf'
    cdf_filepath = f'{local_data_dir}/{date.strftime("%Y")}'
    cdf_fullpath = f'{cdf_filepath}/{cdf_filename}'
    full_url = f'{base_url}/{date.strftime("%Y")}/{cdf_filename}'

    # Download the file if it does not exist
    if not os.path.isfile(cdf_fullpath):
        # Create download directory if it does not exist
        download_tools.ensure_data_dir(cdf_filepath)
        # Download file
        print(f"Downloading {cdf_filename}")
        download_tools.download_file_http(full_url, cdf_fullpath)

    # Now process the file if it exists
    if os.path.isfile(cdf_fullpath):
        print(f"Processing {cdf_filename}")
        data = pycdf.CDF(cdf_fullpath)
        n_images = data['Image_Counts_Clean'].shape[0]
        for i in range(n_images):
            # Set the filename of the PNG
            epoch = data['Epoch'][i]

            lons = xr.DataArray(data['Geo_Lon'][i,::-1,:].T).where(data['Geo_Lon'][i,...]!=-1e+31, np.nan)
            lats = xr.DataArray(data['Geo_Lat'][i,::-1,:].T).where(data['Geo_Lat'][i,...]!=-1e+31, np.nan)
            map = landmask.interp(lon=lons, lat=lats)

            png_filename = f'po_vis_earth-camera-calibrated_{epoch.strftime("%Y%m%dT%H%M%S")}.png'
            png_dir = f'{local_data_dir}/pngs/{epoch.strftime("%Y")}/{epoch.strftime("%Y%m%d")}/'
            download_tools.ensure_data_dir(png_dir)
            png_fullpath = f'{png_dir}{png_filename}'
            if os.path.isfile(png_fullpath):
                continue

            # Create the PNG
            plt_data = data['Image_Counts_Clean'][i,:,:].astype(float)
            #unique = np.unique(plt_data)
            #unique_range = unique.max() - unique.min()

            min_bright = 0
            max_bright = np.percentile(data['Image_Counts_Clean'][i,:,:], 95)

            if max_bright < 11:
                continue

            #plt_data[plt_data < 0] = max_bright
            print(pd.Timestamp.now(), png_fullpath, max_bright)
            tmpfile = './polar.png'

            fig = pygmt.Figure()
            pygmt.makecpt(cmap='gray', series=[min_bright, max_bright])
            fig.grdimage(xr.DataArray(data['Image_Counts_Clean'][i,::-1,:].T), cmap='gray', projection=projection)
            fig.grdcontour(map, frame=False, annotation='-', interval=[0.9], cut=10, pen='0.25p,cyan', projection=projection)
            fig.grdcontour(xr.DataArray(data['On_Earth'][i,::-1,:].T).astype(float), frame=False, annotation='-', interval=[0.9], cut=10, pen='0.25p,darkcyan', projection=projection)
            fig.savefig('polar.png', dpi=64)
            os.system(f"pngquant --force polar.png && mv polar-fs8.png {png_fullpath} && rm -f polar-fs8.png")

            #bitmap_tools.array_to_colormap_png(plt_data, png_fullpath, 'grey', min_bright, max_bright, dtype=np.uint8)
            #print(f"pngquant {tmpfile} --ext -quant.png && mv ./polar-quant.png {png_fullpath}")
            #os.system(f"pngquant {tmpfile} --ext -quant.png && mv ./polar-quant.png {png_fullpath}")
