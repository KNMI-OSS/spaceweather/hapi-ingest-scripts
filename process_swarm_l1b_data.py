#!/usr/bin/env python3
import argparse
import sys
import os
import numpy as np
import pandas as pd
import logging

import schedule
import time

from swxtools.config import config
from swxtools.access import swarm_diss
from swxtools.download_tools import ensure_data_dir
from swxtools.orbit import transforms

from swxtools import hapi_client
from swxtools.config import config


hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']
ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

## TODO: Include metadata, to set up dataset tables
## TODO: Prevent unnecessary upload of downsampled data when no new high-res data is uploaded
## TODO: Create "latest" dataset out of OPER + FAST
## TODO: Schedule continuous updates (based on time since last FAST data?)

def get_command_line_arguments():
    # Instantiate the parser
    parser = argparse.ArgumentParser(description='Process Swarm CDF and SP3 files into parquet files with added records')

    # Choices for parameters
    sats = ['A', 'B', 'C']
    types = ['MODx_SC', 'EFIx_LP', 'MAGx_LR', 'FACxTMS', 'IPDxIRR']
    modes = ['OPER', 'FAST', 'PREL', 'REPR']

    # Arguments
    parser.add_argument('--t0', '--start',
                        help='Start date/time')
    parser.add_argument('--t1', '--stop',
                        help='Stop date/time')
    parser.add_argument('--sat', choices=sats, nargs='+', help='Satellite')
    parser.add_argument('--type', choices=types, nargs='+', help='Swarm L1B data type')
    parser.add_argument('--mode', choices=modes,
                        help='Use FAST instead of OPER data')
    parser.add_argument('--download', '-d', action='store_true',
                        help='Download the original data files')
    parser.add_argument('--convert', '-c', action='store_true',
                        help='Convert original data files and store as parquet')
    parser.add_argument('--upload', '-u', action='store_true',
                        help='Upload converted data files to HAPI server')
    parser.add_argument('--upload_resampled', '-r', action='store_true',
                        help='Upload resampled data only')
    parser.add_argument('--upload_aggregate', '-a', action='store_true',
                        help='Upload aggregate data only')
    parser.add_argument('--force', '-f', action='store_true',
                        help='Force write if file exists or already uploaded')
    parser.add_argument('--repeat', type=int,
                        help='Schedule repeat time in minutes')


    args = parser.parse_args()
    print(args)
    return args


def swarm_add_orbit_parameters_modx_sc(df_orbit):
    # Process the orbit transforms
    orbit_igrs = transforms.itrs_to_igrs(df_orbit)
    orbit_geo = transforms.itrf_to_geodetic(orbit_igrs)
    orbit_qd = transforms.geodetic_to_qd(orbit_geo)
    return orbit_qd


def swarm_add_orbit_parameters(df_in, sat, fast):
    if len(df_in) == 0:
        logging.info("No data found, continuing...")
        return df_in

    # Get and clean up the orbit data
    swarm_obj_orbit = swarm_diss.SwarmFiles(
        data_type='MODx_SC',
        sat=sat,
        fast=fast,
        processed=True
    )

    # If B or q flags are available, remove data where the value is >= 255
#    if 'Flags_B' in df_in:
#        df_in = df_in.where(df_in['Flags_B'] < 255, np.nan)
#    if 'Flags_q' in df_in:
#        df_in = df_in.where(df_in['Flags_q'] < 255, np.nan)

    # Read orbit with 2 min overlaps on either side for interpolation
    t0_orbit = df_in.iloc[0].name - pd.to_timedelta(2, 'min')
    t1_orbit = df_in.iloc[-1].name + pd.to_timedelta(2, 'min')
    swarm_obj_orbit.set_time_interval(t0_orbit, t1_orbit)

    swarm_orbit = swarm_obj_orbit.to_dataframe()
    if len(swarm_orbit) == 0:
        logging.error(f"No orbit data available for interval {t0_orbit} - {t1_orbit}")
        return pd.DataFrame()

    swarm_orbit.drop('time_gps', axis=1, inplace=True)
    swarm_orbit = swarm_orbit[~swarm_orbit.index.duplicated()].drop(
        ["height", "lon_qd", "lat_qd", "mlt", "lon", "lat", "height"], axis=1
    )

    # Interpolate the orbit, but drop the processed fields
    logging.info("Interpolating orbit data")
    swarm_orbit_interpolated = transforms.interpolate_orbit_to_datetimeindex(
        swarm_orbit,
        df_in.index
    )

    # Convert to geodetic and quasi-dipole coordinates
    logging.info("Converting to geodetic and magnetic coordinates")
    swarm_orbit_geo = transforms.itrf_to_geodetic(
        swarm_orbit_interpolated
    )

    swarm_orbit_qd = transforms.geodetic_to_qd(
        swarm_orbit_geo
    )

    swarm_orbit_sun = transforms.itrf_to_sun_angles(
        swarm_orbit_qd
    )

    # Append the orbit data
    swarm_data = pd.concat(
        [df_in,
         swarm_orbit_sun[[
             'lon',
             'lat',
             'height',
             'lon_qd',
             'lat_qd',
             'mlt',
             'solar_azimuth',
             'solar_elevation'
         ]]],
        axis=1
    )

    # If magnetic data is available, add in modeled magnetic data and residuals
    if 'B_NEC_1' in swarm_data:
        logging.info("Computing CHAOS-7 model residuals")
        # Compute the model values
        modeltime = (pd.to_datetime(swarm_data.index.values) -
                     pd.to_datetime("2000-01-01T"))/pd.to_timedelta(1, 'D')

        B_radius, B_theta, B_phi = chaosmodel(
            modeltime,
            swarm_data['Radius'].values / 1000,
            90.0 - swarm_data['Latitude'].values,
            swarm_data['Longitude'].values,
            source_list='internal'
        )

        swarm_data['Delta_B_NEC_1'] = (
            swarm_data['B_NEC_1'].values + B_theta)
        swarm_data['Delta_B_NEC_2'] = (
            swarm_data['B_NEC_2'].values - B_phi)
        swarm_data['Delta_B_NEC_3'] = (
            swarm_data['B_NEC_3'].values + B_radius)
        swarm_data['Delta_F'] = (
            swarm_data['F'].values -
            np.sqrt(B_radius**2 + B_theta**2 + B_phi**2)
        )

    return swarm_data


def download(args):
    t0 = pd.to_datetime(args.t0, utc=True)
    t1 = pd.to_datetime(args.t1, utc=True)
    logging.info("Initiating downloads from swarm_diss FTP")
    ftp_conn = swarm_diss.setup_swarm_ftp_conn()
    for sat in args.sat:
        for data_type in args.type:
            logging.info(f"Downloading {data_type} data for Swarm {sat.upper()}")
            swarm_obj = swarm_diss.SwarmFiles(
                data_type=data_type,
                sat=sat,
                fast=args.mode == 'FAST',
                prel=args.mode == 'PREL',
                processed=False)
            swarm_obj.set_time_interval(t0, t1)
            swarm_obj.download(ftp_conn=ftp_conn)
    ftp_conn.close()


def convert(args, data_type, sat):
    logging.info(f"Converting {data_type} data for Swarm {sat.upper()}")

    swarm_obj = swarm_diss.SwarmFiles(
        data_type=data_type,
        sat=sat,
        fast=args.mode == 'FAST',
        prel=args.mode == 'PREL',
        processed=False
    )
    t0 = pd.to_datetime(args.t0, utc=True)
    t1 = pd.to_datetime(args.t1, utc=True)
    swarm_obj.set_time_interval(t0, t1)

    # Set output path for processed files and ensure it exists
    local_data_dir, file_location = swarm_diss.swarm_data_dir(
        data_type=data_type,
        sat=sat,
        fast=args.mode == 'FAST',
        prel=args.mode == 'PREL',
        processed=True
    )
    ensure_data_dir(file_location)

    # Loop over the Swarm files
    for i, file_info in enumerate(swarm_obj.filelist.to_dict(orient='records')):
        parquet_filename = f"{file_info['filename'].split('.')[0]}.parquet"
        parquet_filepath = (
            file_location + f"{parquet_filename}"
        )
        if os.path.isfile(parquet_filepath):
            logging.debug(f"File exists: {parquet_filepath}")
            if not args.force:
                continue
        logging.info(f"({i+1}/{len(swarm_obj.filelist)}): " +
                     f"Creating {parquet_filepath}")
        df_in = swarm_diss.swarm_files_to_df([file_info['path']])
        if data_type == 'MODx_SC':
            df_out = swarm_add_orbit_parameters_modx_sc(df_in)
        elif (data_type == 'EFIx_LP' or
              data_type == 'MAGx_LR' or
              data_type == 'FACxTMS' or
              data_type == 'IPDxIRR'):
            df_out = swarm_add_orbit_parameters(df_in, sat, fast=args.mode == 'FAST')
        else:
            logging.error("This type has not been implemented")
        if len(df_out) == len(df_in):
            logging.info(f"Writing {parquet_filepath}")
            df_out.to_parquet(parquet_filepath)
        else:
            logging.error(f"Length of converted dataframe does not match for {parquet_filename}")


def upload(args, data_type, sat):
    logging.info(f"Uploading {data_type} data for Swarm {sat.upper()}")

    swarm_obj = swarm_diss.SwarmFiles(
        data_type=data_type,
        sat=sat,
        fast=args.mode == 'FAST',
        prel=args.mode == 'PREL',
        processed=True
    )
    t0 = pd.to_datetime(args.t0, utc=True)
    t1 = pd.to_datetime(args.t1, utc=True)
    swarm_obj.set_time_interval(t0, t1)

    db_id = f"sw_{args.mode.lower()}_{data_type.lower().replace('x',sat.lower())}"

    # Read the info
    print(db_id)
    metadata = hapi_client.get_info(hapi_server, db_id)
    info_values = hapi_client.get_info_values(metadata)
    parameters = info_values['parameters']
    replace_nan_fill = info_values['replace_nan_fill']
    high_cadence = metadata['cadence']

    # Set up min and max time for aggregate processing
    t_min = pd.to_datetime("2050-01-01T", utc=True)
    t_max = pd.to_datetime("1950-01-01T", utc=True)

    # Read the list of already processed/ingested data files
    processed_filelist_file = f'processed_files/{db_id}_processed_files.txt'
    if os.path.isfile(processed_filelist_file):
        with open(processed_filelist_file, 'r') as fh:
            processed_filenames = fh.read().splitlines()
    else:
        processed_filenames = []
    timestamps_processed = (["-".join(name.split("_")[5:7]) for name in processed_filenames])

    for i, file_info in enumerate(swarm_obj.filelist.to_dict(orient='records')):
        # Skip data outside the time period
        if file_info['t1'] < t0 or file_info['t0'] > t1:
            logging.info(f"Skipping - out of time interval: {file_info['filename']}")
            continue

        # Skip already processed files
        current_timestamps = "-".join([
            file_info['t0'].strftime("%Y%m%dT%H%M%S"),
            file_info['t1'].strftime("%Y%m%dT%H%M%S")
        ])
        if current_timestamps in timestamps_processed:
            if not args.force:
                logging.info("Skipping - already processed")
                continue

        # Read the data
        df = swarm_obj.to_dataframe_for_file_index(i)

        # Set min and max time for aggregate processing
        if df.index[0] < t_min:
            t_min = df.index[0]
        if df.index[-1] > t_max:
            t_max = df.index[-1]

        # Set time string for database key
        df['time'] = df.index.strftime(ISO_TIMEFMT)

        # Upload to the data store
        swarm_data = df[parameters].replace(replace_nan_fill)
        hapi_client.add_data(hapi_server,
                             hapi_server_key,
                             db_id,
                             swarm_data)

        # Upload lower cadences
        logging.info("Resampling lower cadences")
        hapi_client.resample_lower_cadences(
            hapi_server,
            hapi_server_key,
            db_id,
            dataframe=swarm_diss.processed_dataframe_by_time_closure(
                sat=sat,
                data_type=data_type,
                fast=args.mode == 'FAST'
            ),
            t_first_data=swarm_data.index[0],
            t_last_data=swarm_data.index[-1]
        )

        # Append the filename to the list of processed files
        processed_filenames.append(file_info['filename'])
        with open(processed_filelist_file, 'a+') as fh:
            fh.write(file_info['filename'] + '\n')

    # Start aggregate processing
    logging.info(f"Uploading {data_type} aggregate data for Swarm {sat.upper()}")
    db_id = f"sw_{args.mode.lower()}_{data_type.lower().replace('x',sat.lower())}_aggregate"
    # Read the info
    metadata = hapi_client.get_info(hapi_server, db_id)
    info_values = hapi_client.get_info_values(metadata)
    parameters = info_values['parameters']
    replace_nan_fill = info_values['replace_nan_fill']


    if t_min > t0:
        t_min = t0
    if t_max < t1:
        t_max = t1

    hapi_client.ingest_lower_cadence_aggregates(
        hapi_server,
        hapi_server_key,
        metadata,
        dataframe_func=swarm_diss.processed_dataframe_by_time_closure(
            sat=sat,
            data_type=data_type,
            fast=args.mode == 'FAST'
        ),
        t0=t_min,
        t1=t_max,
        high_cadence=high_cadence,
        min_completeness=0.9
    )


def upload_resampled(args, data_type, sat):
    logging.info(f"Uploading {data_type} data for Swarm {sat.upper()}")

    swarm_obj = swarm_diss.SwarmFiles(
        data_type=data_type,
        sat=sat,
        fast=args.mode == 'FAST',
        prel=args.mode == 'PREL',
        processed=True
    )
    t0 = pd.to_datetime(args.t0, utc=True)
    t1 = pd.to_datetime(args.t1, utc=True)
    swarm_obj.set_time_interval(t0, t1)

    db_id = f"sw_{args.mode.lower()}_{data_type.lower().replace('x',sat.lower())}"

    # Read the info
    metadata = hapi_client.get_info(hapi_server, db_id)
    info_values = hapi_client.get_info_values(metadata)
    parameters = info_values['parameters']
    replace_nan_fill = info_values['replace_nan_fill']
    high_cadence = metadata['cadence']

    # Upload lower cadences
    logging.info("Resampling lower cadences")
    hapi_client.resample_lower_cadences(
        hapi_server,
        hapi_server_key,
        db_id,
        dataframe=swarm_diss.processed_dataframe_by_time_closure(
            sat=sat,
            data_type=data_type,
            fast=args.mode == 'FAST'
        ),
        t_first_data=t0,
        t_last_data=t1
    )


def upload_aggregate(args, data_type, sat):
    # Upload aggregate
    logging.info(f"Uploading {data_type} aggregate data for Swarm {sat.upper()}")

    swarm_obj = swarm_diss.SwarmFiles(
        data_type=data_type,
        sat=sat,
        fast=args.mode == 'FAST',
        prel=args.mode == 'PREL',
        processed=True
    )
    t0 = pd.to_datetime(args.t0, utc=True)
    t1 = pd.to_datetime(args.t1, utc=True)
    swarm_obj.set_time_interval(t0, t1)

    db_id = f"sw_{args.mode.lower()}_{data_type.lower().replace('x',sat.lower())}_aggregate"

    # Read the info
    metadata = hapi_client.get_info(hapi_server, db_id)
    info_values = hapi_client.get_info_values(metadata)
    parameters = info_values['parameters']
    replace_nan_fill = info_values['replace_nan_fill']
    high_cadence = metadata['cadence']

    # Upload low cadence aggregates
    hapi_client.ingest_lower_cadence_aggregates(
        hapi_server,
        hapi_server_key,
        metadata,
        dataframe_func=swarm_diss.processed_dataframe_by_time_closure(
            sat=sat,
            data_type=data_type,
            fast=args.mode == 'FAST'
        ),
        t0=t0,
        t1=t1,
        high_cadence=high_cadence,
        min_completeness=0.9
    )


def all_tasks(args):
    logging.info("Starting scheduled Swarm download/processing/upload tasks")
    if args.download:
        download(args)

    for data_type in args.type:
        for sat in args.sat:
            if args.convert:
                convert(args, data_type, sat)
            if args.upload:
                upload(args, data_type, sat)
            if args.upload_resampled:
                upload_resampled(args, data_type, sat)
            if args.upload_aggregate:
                upload_aggregate(args, data_type, sat)
    logging.info("End of scheduled Swarm download/processing/upload tasks")


if __name__ == "__main__":
    # Set up logging
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

    args = get_command_line_arguments()

    # If necessary, load the CHAOS model
    if 'MAGx_LR' in args.type and args.convert:
        import chaosmagpy
        chaosmodel = chaosmagpy.load_CHAOS_matfile(
            config['local_source_data_path'] + '/chaosmodel/CHAOS-7.18.mat'
        )

    all_tasks(args)
    if args.repeat is not None:
        if args.repeat < 10:
            logging.error(f"Repeat is set to {args.repeat} minutes. "
                          "Schedules repeating at less than 10 minutes "
                          "should not be necessary for Swarm data")
        else:
            logging.info(
                f"Setting schedule to repeat every {args.repeat} minutes"
            )
            schedule.every(args.repeat).minutes.do(all_tasks, args=args)

            while True:
                try:
                    schedule.run_pending()
                except (BaseException) as err:
                    # If any error occurs, this is usually due to an issue
                    # with the availability of network or disk resources.
                    # So we log the error and wait ten minutes before trying
                    # again, to avoid the script generating too many errors.
                    # TODO: Send an email/slack message.
                    logging.error(f"Exception: {err}")
                    time.sleep(600)

                # Wait 10 seconds before trying the next schedule loop
                time.sleep(10)
