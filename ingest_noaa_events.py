#
# This file is part of hapi-ingest-scripts
#
# https://gitlab.com/KNMI-OSS/spaceweather/hapi-ingest-scripts
#
# Copyright (c) 2024 KNMI
# All Rights Reserved
"""Process NOAA space weather events using swxtools and upload them to the hapi server."""

import requests
import json
import numpy as np
import pandas as pd
import argparse
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
    resample_lower_cadences,
)
from swxtools.config import config
import swxtools.access.noaa_events as swxtools_noaa
from os import walk

# - global parameters ------------------------------
hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

# Time format for timeline viewer
ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

# Json metadata for all event types
metadata_json_electronflux = '''{
    "id": "electronflux_alerts_noaa",
    "description": "Electron flux alerts from NOAA/SWPC",
    "timeStampLocation": "begin",
    "resourceURL": "https://services.swpc.noaa.gov/products/alerts.json",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        },
        {
            "name": "original_message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Original message",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

metadata_json_geomagnetic = '''{
    "id": "geomagnetic_alerts_noaa",
    "description": "Geomagnetic alerts from NOAA/SWPC",
    "timeStampLocation": "begin",
    "resourceURL": "https://services.swpc.noaa.gov/products/alerts.json",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "noaa_scale",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "NOAA scale",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        },
        {
            "name": "original_message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Original message",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

metadata_json_proton = '''{
    "id": "proton_alerts_noaa",
    "description": "Proton alerts from NOAA/SWPC",
    "timeStampLocation": "begin",
    "resourceURL": "https://services.swpc.noaa.gov/products/alerts.json",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "noaa_scale",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "NOAA scale (predicted)",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        },
        {
            "name": "original_message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Original message",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

metadata_json_radio = '''{
    "id": "radio_alerts_noaa",
    "description": "Radio alerts from NOAA/SWPC",
    "timeStampLocation": "begin",
    "resourceURL": "https://services.swpc.noaa.gov/products/alerts.json",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "peak_flux",
            "type": "double",
            "units": "sfu",
            "fill": "-999",
            "description": "Peak flux",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        },
        {
            "name": "original_message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Original message",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

metadata_json_stratospheric = '''{
    "id": "stratospheric_alerts_noaa",
    "description": "Stratospheric alerts from NOAA/SWPC",
    "timeStampLocation": "begin",
    "resourceURL": "https://services.swpc.noaa.gov/products/alerts.json",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        },
        {
            "name": "original_message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Original message",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

metadata_json_xray = '''{
    "id": "xray_alerts_noaa",
    "description": "X-ray alerts from NOAA/SWPC",
    "timeStampLocation": "begin",
    "resourceURL": "https://services.swpc.noaa.gov/products/alerts.json",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "noaa_scale",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "NOAA scale",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        },
        {
            "name": "original_message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Original message",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''


def argument_parser() -> dict:
    """Process the command line arguments to settings for the program

    Parameters
    ----------
    None

    Returns
    -------
    settings_dict : dict
        A dictionary with values for the different program settings.
    """

    # Set up parser
    parser = argparse.ArgumentParser()

    ISO_TIMEFMT_PARSER = "%Y-%m-%d"

    # Choices for parameters
    notif_sets = ['ALL', 'RE', 'AR']
    event_types = ['ALL', 'EL', 'GEO', 'PRO', 'RAD', 'STR', 'XRAY']

    # Add potential parser arguments
    parser.add_argument('--t0', '--start',
                        help='Start date/time, Y-m-d format')
    parser.add_argument('--t1', '--stop',
                        help='Stop date/time, Y-m-d format')

    parser.add_argument('--sets', choices=notif_sets, nargs='+', help='Choose which data sets to use')
    parser.add_argument('--types', choices=event_types, nargs='+', help='Choose which event types to process')

    parser.add_argument("--upload", help="upload the processed NOAA notifications")

    # Retrieve arguments from command line
    args = parser.parse_args()
    settings_dict = {}

    # Save arguments to a usable dict
    if 'RE' in args.sets or 'ALL' in args.sets:
        settings_dict['recent'] = True
    else:
        settings_dict['recent'] = False
    if 'AR' in args.sets or 'ALL' in args.sets:
        settings_dict['archive'] = True
    else:
        settings_dict['archive'] = False

    if 'EL' in args.types or 'ALL' in args.types:
        settings_dict['electronflux'] = True
    else:
        settings_dict['electronflux'] = False
    if 'GEO' in args.types or 'ALL' in args.types:
        settings_dict['geomagnetic'] = True
    else:
        settings_dict['geomagnetic'] = False
    if 'PRO' in args.types or 'ALL' in args.types:
        settings_dict['proton'] = True
    else:
        settings_dict['proton'] = False
    if 'RAD' in args.types or 'ALL' in args.types:
        settings_dict['radio'] = True
    else:
        settings_dict['radio'] = False
    if 'STR' in args.types or 'ALL' in args.types:
        settings_dict['stratospheric'] = True
    else:
        settings_dict['stratospheric'] = False
    if 'XRAY' in args.types or 'ALL' in args.types:
        settings_dict['xray'] = True
    else:
        settings_dict['xray'] = False

    if args.upload == 'y':
        settings_dict['upload'] = True
    else:
        settings_dict['upload'] = False

    if args.t0 and args.t1:
        settings_dict['start_date'] = args.t0
        settings_dict['end_date'] = args.t1

    return settings_dict


def process_noaa(parser_data: dict) -> dict:
    """Process the NOAA events to upload-ready notifications

    Parameters
    ----------
    parser_data : dict
        A dictionary from the argument parser
        with values for the different program settings.

    Returns
    -------
    notifications : dict
        A dictionary with notification dataframes for each of the event types.
    """

    electronflux_dataframes = []
    geomagnetic_dataframes = []
    proton_dataframes = []
    radio_dataframes = []
    stratospheric_dataframes = []
    xray_dataframes = []

    # Archive pipeline
    if parser_data['archive']:
        print("Start downloading (archive)")
        swxtools_noaa.download_noaa_html()
        print("Finished downloading (archive)")

        # Make list of html files
        read_list = []
        path = config['local_source_data_path'] + '/noaa_events'

        for root, dirc, files in walk(path):
            for FileName in files:
                if '.zip' not in FileName:
                    read_list.append(path + '/' + FileName)

        print("Start parsing (archive)")
        archive_dataframes = swxtools_noaa.parse_noaa_html_to_dataframe(read_list, parser_data)
        print("Finished parsing (archive)")

        print("Archive dataframes", archive_dataframes)

        # Add parser output to input dataframes for uploading
        if parser_data['electronflux']:
            electronflux_dataframes.append(archive_dataframes['electronflux'])
        if parser_data['geomagnetic']:
            geomagnetic_dataframes.append(archive_dataframes['geomagnetic'])
        if parser_data['proton']:
            proton_dataframes.append(archive_dataframes['proton'])
        if parser_data['radio']:
            radio_dataframes.append(archive_dataframes['radio'])
        if parser_data['stratospheric']:
            stratospheric_dataframes.append(archive_dataframes['stratospheric'])
        if parser_data['xray']:
            xray_dataframes.append(archive_dataframes['xray'])

    # Recent data pipeline
    if parser_data['recent']:
        print("Start downloading (recent)")
        recent_notifications = swxtools_noaa.download_noaa_recent_json()
        print("Finished downloading (recent)")

        print("Start parsing (recent)")
        recent_dataframes = swxtools_noaa.parse_noaa_recent_json_to_dataframe(recent_notifications, parser_data)
        print("Finished parsing (recent)")

        print("Recent dataframes", recent_dataframes)

        # Add parser output to input dataframes for uploading
        if parser_data['electronflux']:
            electronflux_dataframes.append(recent_dataframes['electronflux'])
        if parser_data['geomagnetic']:
            geomagnetic_dataframes.append(recent_dataframes['geomagnetic'])
        if parser_data['proton']:
            proton_dataframes.append(recent_dataframes['proton'])
        if parser_data['radio']:
            radio_dataframes.append(recent_dataframes['radio'])
        if parser_data['stratospheric']:
            stratospheric_dataframes.append(recent_dataframes['stratospheric'])
        if parser_data['xray']:
            xray_dataframes.append(recent_dataframes['xray'])

    # Convert
    print("Start converting")
    electronflux_notifications_final = swxtools_noaa.convert_noaa_electronflux(electronflux_dataframes)
    geomagnetic_notifications_final = swxtools_noaa.convert_noaa_geomagnetic(geomagnetic_dataframes)
    proton_notifications_final = swxtools_noaa.convert_noaa_proton(proton_dataframes)
    radio_notifications_final = swxtools_noaa.convert_noaa_radio(radio_dataframes)
    stratospheric_notifications_final = swxtools_noaa.convert_noaa_stratospheric(stratospheric_dataframes)
    xray_notifications_final = swxtools_noaa.convert_noaa_xray(xray_dataframes)
    print("Finished converting")

    # Print results
    print("Print results")
    print("Electron-flux events", electronflux_notifications_final)
    print("Geomagnetic events", geomagnetic_notifications_final)
    print("Proton events", proton_notifications_final)
    print("Radio events", radio_notifications_final)
    print("Stratospheric events", stratospheric_notifications_final)
    print("X-ray events", xray_notifications_final)
    print("Finished printing results")

    notifications_dict = {'electronflux': electronflux_notifications_final,
                          'geomagnetic': geomagnetic_notifications_final,
                          'proton': proton_notifications_final,
                          'radio': radio_notifications_final,
                          'stratospheric': stratospheric_notifications_final,
                          'xray': xray_notifications_final}

    return notifications_dict


def upload_noaa(upload_data: dict, parser_data: dict):
    """Uploads the NOAA events to the hapi server

    Parameters
    ----------
    upload_data : dict
        A dictionary from the process noaa function
        with the dataframes of NOAA events to upload to the hapi server.
    parser_data : dict
        A dictionary from the argument parser
        with values for the different program settings.

    Returns
    -------
    None

    Uploads data to the hapi server
    """

    # Go over processed lists and upload one by one
    for data_type, data_to_upload in upload_data.items():

        # Set metadata for json depending on data type
        if data_type == 'electronflux' and parser_data['electronflux']:
            metadata = json.loads(metadata_json_electronflux)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        elif data_type == 'geomagnetic' and parser_data['geomagnetic']:
            metadata = json.loads(metadata_json_geomagnetic)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        elif data_type == 'proton' and parser_data['proton']:
            metadata = json.loads(metadata_json_proton)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        elif data_type == 'radio' and parser_data['radio']:
            metadata = json.loads(metadata_json_radio)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        elif data_type == 'stratospheric' and parser_data['stratospheric']:
            metadata = json.loads(metadata_json_stratospheric)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        elif data_type == 'xray' and parser_data['xray']:
            metadata = json.loads(metadata_json_xray)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        else:
            print('Data type ', data_type, ' skipped')
            continue

        # Set other settings
        metadata_values = get_info_values(metadata)
        db_id = metadata_values['id']
        parameters = metadata_values['parameters']
        replace_nan_fill = metadata_values['replace_nan_fill']

        # Retrieve alerts and put in dataframe
        notifications = data_to_upload

        # Check whether any notifications are present
        if len(notifications) > 0:

            df = pd.DataFrame(notifications)
            df.index = pd.to_datetime(df['issue_datetime'], format='%Y %b %d %H%M %Z')

            # Convert time to correct format
            df['time'] = df.index.strftime(ISO_TIMEFMT)

            # Stratospheric does not have begin and end time in the same manner
            if data_type != 'stratospheric':
                df['begin_time'] = pd.to_datetime(df['begin_time'], format='%Y %b %d %H%M %Z').dt.strftime(
                    ISO_TIMEFMT).replace(
                    {np.nan: '-999'})
                df['end_time'] = pd.to_datetime(df['end_time'],
                                                format='%Y %b %d %H%M %Z').dt.strftime(ISO_TIMEFMT).replace(
                    {np.nan: '-999'})

            # Peak flux variable for radio events
            if data_type == 'radio':
                df['peak_flux'] = df['peak_flux'].replace({np.nan: -999})

            data = df[parameters].replace(replace_nan_fill)

            # Potentially print data per row for final check
            # for index, row in data.iterrows():
            #     print(row)

            # Upload to the hapi server
            add_data(hapi_server,
                     hapi_server_key,
                     db_id,
                     data)


# Temporary method to test directly from swxtools, commented for now
if __name__ == "__main__":

    # Argument parser
    parser_output = argument_parser()
    print("Parser output", parser_output)

    # Process NOAA data using swxtools
    process_output = process_noaa(parser_output)
    print("Process output", process_output)

    # Upload processed NOAA data to the server
    if parser_output['upload']:
        upload_noaa(process_output, parser_output)
        print("Upload succesful")
