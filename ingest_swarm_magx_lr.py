#!/usr/bin/env python3
import pandas as pd
import os
import sys
import json
import logging
import glob
import numpy as np
import chaosmagpy
import schedule
import time
from swxtools.access import swarm_diss
from swxtools.orbit.transforms import (
    interpolate_orbit_to_datetimeindex,
    itrf_to_geodetic,
    geodetic_to_qd
)
from swxtools import hapi_client
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

chaosmodel = chaosmagpy.load_CHAOS_matfile(
    config['local_source_data_path'] + '/chaosmodel/CHAOS-7.17.mat'
)

metadata_json = '''{
    "id": "sw_oper_maga_lr",
    "description": "Magnetic field data from the Swarm A satellite",
    "timeStampLocation": "begin",
    "resourceURL": "https://swarm-diss.eo.esa.int/",
    "resourceID": "ESA",
    "contact": "ESA EO Help",
    "contactID": "ESA EO Help",
    "resampleMethod": "mean",
    "cadence": "PT1S",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-99999.0",
            "description": "Timestamp of the measurement",
            "label": "",
            "key": true
        },
        {
            "name": "Latitude",
            "type": "double",
            "units": "degrees",
            "fill": "-99999.0",
            "description": "Geocentric latitude",
            "label": "",
            "key": false
        },
        {
            "name": "Longitude",
            "type": "double",
            "units": "degrees",
            "fill": "-99999.0",
            "description": "Geocentric longitude",
            "label": "",
            "key": false
        },
        {
            "name": "Radius",
            "type": "double",
            "units": "m",
            "fill": "-99999.0",
            "description": "Radius",
            "label": "",
            "key": false
        },
        {
            "name": "B_NEC_1",
            "type": "double",
            "units": "nT",
            "fill": "-99999.0",
            "description": "Northward component of the magnetic field",
            "label": "",
            "key": false
        },
        {
            "name": "B_NEC_2",
            "type": "double",
            "units": "nT",
            "fill": "-99999.0",
            "description": "Eastward component of the magnetic field",
            "label": "",
            "key": false
        },
        {
            "name": "B_NEC_3",
            "type": "double",
            "units": "nT",
            "fill": "-99999.0",
            "description": "Nadir component of the magnetic field",
            "label": "",
            "key": false
        },
        {
            "name": "F",
            "type": "double",
            "units": "nT",
            "fill": "-99999.0",
            "description": "Magnitude of the magnetic field",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_1",
            "type": "double",
            "units": "nT",
            "fill": "-99999.0",
            "description": "Northward component of the magnetic field, with core and crustal fields removed using the CHAOS model",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_2",
            "type": "double",
            "units": "nT",
            "fill": "-99999.0",
            "description": "Eastward component of the magnetic field, with core and crustal fields removed using the CHAOS model",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_3",
            "type": "double",
            "units": "nT",
            "fill": "-99999.0",
            "description": "Nadir component of the magnetic field, with core and crustal fields removed using the CHAOS model",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_F",
            "type": "double",
            "units": "nT",
            "fill": "-99999.0",
            "description": "Magnitude of the magnetic field, with core and crustal fields removed using the CHAOS model",
            "label": "",
            "key": false
        },
        {
            "name": "F_error",
            "type": "double",
            "units": "nT",
            "fill": "-99999.0",
            "description": "Error estimate of the magnetic field intensity",
            "label": "",
            "key": false
        },
        {
            "name": "Flags_F",
            "type": "integer",
            "units": "",
            "fill": "-99",
            "description": "Flags related to the magnetic field intensity measurements, see Table 6-2 of SW-RS-DSC_SY-0007",
            "label": "",
            "key": false
        },
        {
            "name": "Flags_B",
            "type": "integer",
            "units": "",
            "fill": "-99",
            "description": "Flags characterizing the magnetic field vector measurements, see Table 6-2 of SW-RS-DSC-SY-0007",
            "label": "",
            "key": false
        },
        {
            "name": "lon",
            "type": "double",
            "units": "deg",
            "fill": "-99999.0",
            "description": "Geographic longitude",
            "label": "",
            "key": false
        },
        {
            "name": "lat",
            "type": "double",
            "units": "deg",
            "fill": "-99999.0",
            "description": "Geodetic latitude",
            "label": "",
            "key": false
        },
        {
            "name": "height",
            "type": "double",
            "units": "m",
            "fill": "-99999.0",
            "description": "Geodetic altitude",
            "label": "",
            "key": false
        },
        {
            "name": "lon_qd",
            "type": "double",
            "units": "deg",
            "fill": "-99999.0",
            "description": "Quasi-dipole longitude",
            "label": "",
            "key": false
        },
        {
            "name": "lat_qd",
            "type": "double",
            "units": "deg",
            "fill": "-99999.0",
            "description": "Quasi-dipole latitude",
            "label": "",
            "key": false
        },
        {
            "name": "mlt",
            "type": "double",
            "units": "hours",
            "fill": "-99999.0",
            "description": "Quasi-dipole magnetic local time",
            "label": "",
            "key": false
        }
    ],
    "x_relations": [
        {
            "id": "sw_oper_maga_lr_PT3S",
            "description": "Magnetic field data from the Swarm A satellite, downsampled to 2 second cadence",
            "cadence": "PT3S",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_maga_lr_PT8S",
            "description": "Magnetic field data from the Swarm A satellite, downsampled to 8 second cadence",
            "cadence": "PT8S",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_maga_lr_PT30S",
            "description": "Magnetic field data from the Swarm A satellite, downsampled to 30 second cadence",
            "cadence": "PT30S",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_maga_lr_PT2M",
            "description": "Magnetic field data from the Swarm A satellite, downsampled to 2 minute cadence",
            "cadence": "PT2M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_maga_lr_PT6M",
            "description": "Magnetic field data from the Swarm A satellite, downsampled to 6 minute cadence",
            "cadence": "PT6M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_maga_lr_PT15M",
            "description": "Magnetic field data from the Swarm A satellite, downsampled to 15 minute cadence",
            "cadence": "PT15M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        }
    ]
}'''


metadata_json_aggregate = '''{
    "id": "sw_oper_maga_lr_aggregate",
    "description": "Lower cadence aggregate statistics of the magnetic field data of the Swarm A satellite",
    "timeStampLocation": "begin",
    "resourceURL": "https://swarm-diss.eo.esa.int/",
    "resourceID": "ESA",
    "contact": "ESA EO Help",
    "contactID": "ESA EO Help",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-9999.0",
            "description": "Timestamp",
            "label": "",
            "key": true
        },
        {
            "name": "Delta_B_NEC_1_min",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Minimum of the northward component of the magnetic field perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_1_mean",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Mean of the northward component of the magnetic field perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_1_max",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Maximum of the northward component of the magnetic field perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_2_min",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Minimum of the eastward component of the magnetic field perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_2_mean",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Mean of the eastward component of the magnetic field perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_2_max",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Maximum of the eastward component of the magnetic field perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_3_min",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Minimum of the nadir component of the magnetic field perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_3_mean",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Mean of the nadir component of the magnetic field perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_B_NEC_3_max",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Maximum of the nadir component of the magnetic field perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_F_min",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Minimum of the magnetic field magnitude perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_F_mean",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Mean of the magnetic field magnitude perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "Delta_F_max",
            "type": "double",
            "units": "nT",
            "fill": "-9999.0",
            "description": "Maximum of the magnetic field magnitude perturbation",
            "label": "",
            "key": false
        },
        {
            "name": "coverage",
            "type": "double",
            "units": "",
            "fill": "-9999.0",
            "description": "Coverage",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1H30M",
    "x_relations": [
        {
            "id": "sw_oper_maga_lr_aggregate_PT6H",
            "description": "Lower cadence aggregate statistics of the magnetic field data of the Swarm A satellite",
            "cadence": "PT6H",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_maga_lr_aggregate_P1D",
            "description": "Lower cadence aggregate statistics of the magnetic field data of the Swarm A satellite",
            "cadence": "P1D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_maga_lr_aggregate_P4D",
            "description": "Lower cadence aggregate statistics of the magnetic field data of the Swarm A satellite",
            "cadence": "P4D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_maga_lr_aggregate_P16D",
            "description": "Lower cadence aggregate statistics of the magnetic field data of the Swarm A satellite",
            "cadence": "P16D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_maga_lr_aggregate_P32D",
            "description": "Lower cadence aggregate statistics of the magnetic field data of the Swarm A satellite",
            "cadence": "P32D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        }
    ]
}'''


def get_metadata(metadata_json, satletter, fast=False, prel=False):
    # Update the metadata
    # 1. Set the satellite
    metadata_json_sat = metadata_json.replace(
        'maga_lr',
        f'mag{satletter.lower()}_lr'
    )
    metadata_json_sat = metadata_json_sat.replace(
        'Swarm A',
        f'Swarm {satletter.upper()}'
    )

    # 2. Change oper to fast data, if requested
    if fast:
        metadata_json_sat = metadata_json_sat.replace('oper', 'fast')
    elif prel:
        metadata_json_sat = metadata_json_sat.replace('oper', 'prel')

    metadata = json.loads(metadata_json_sat)
    return metadata


def ingest_swarm_magx_lr(
        satletter,
        metadata,
        t_start=pd.to_datetime("1900-01-01T00:00:00Z", utc=True),
        t_stop=pd.to_datetime("2100-01-01T00:00:00Z", utc=True)):

    # Get the necessary info from metadata
    metadata_values = hapi_client.get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']
    if 'fast' in db_id:
        fast = True
        prel = False
    elif 'prel' in db_id:
        fast = False
        prel = True
    else:
        fast = False
        prel = False

    # Info
    info_request = hapi_client.get_info(hapi_server, db_id)

    # Create if the dataset does not exist
    if info_request['status']['code'] == 1406:
        logging.info(
            f"Creating new dataset {db_id} on server {hapi_server}."
        )
        hapi_client.create_dataset(hapi_server, hapi_server_key, metadata)

    # Download latest data files
    swarm_diss.download(sat=f'{satletter.upper()}',
                        data_type='MODx_SC',
                        fast=fast,
                        prel=False)

    swarm_diss.download(sat=f'{satletter.upper()}',
                        data_type='MAGx_LR',
                        fast=fast,
                        prel=prel)


    # Read the list of already processed/ingested data files
    processed_filelist_file = f'processed_files/{db_id}_processed_files.txt'
    if os.path.isfile(processed_filelist_file):
        with open(processed_filelist_file, 'r') as fh:
            processed_filenames = fh.read().splitlines()
    else:
        processed_filenames = []

    # Initial set up of the boundaries for processing lower cadence data
    t_first_data = pd.to_datetime("2100-01-01T00:00:00", utc=True)
    t_last_data = pd.to_datetime("1900-01-01T00:00:00", utc=True)

    # Get the magnetometer data filenames
    swarm_obj_mag = swarm_diss.SwarmFiles(
        data_type='MAGx_LR',
        sat=f'{satletter.upper()}',
        fast=fast,
        prel=prel
    )

    swarm_obj_mag.set_time_interval(t_start, t_stop)
    filelist = swarm_obj_mag.filelist.to_dict(orient='records')

    for i_file, file_info in enumerate(filelist):

        # Skip if file has already been processed
        if file_info['filename'] in processed_filenames:
            continue

        pickle_file = f"{file_info['filename']}.pickle"

        if os.path.isfile(pickle_file):
            swarm_data_complete = pd.read_pickle(pickle_file)
            logging.info(f"Read data from {pickle_file}")
        else:
            # Load the MAG data
            logging.info(f"Processing: {i_file} {file_info['filename']}")
            swarm_data = swarm_obj_mag.to_dataframe_for_file_index(i_file)

            # Remove the flagged data
            swarm_data['B_NEC_1'].where(
                swarm_data['Flags_B'] == 0, np.nan, inplace=True
            )
            swarm_data['B_NEC_2'].where(
                swarm_data['Flags_B'] == 0, np.nan, inplace=True
            )
            swarm_data['B_NEC_3'].where(
                swarm_data['Flags_B'] == 0, np.nan, inplace=True
            )
            swarm_data['F'].where(
                swarm_data['Flags_F'] == 0, np.nan, inplace=True
            )

            # Compute the model values
            modeltime = (pd.to_datetime(swarm_data.index.values) -
                         pd.to_datetime("2000-01-01T"))/pd.to_timedelta(1, 'D')

            B_radius, B_theta, B_phi = chaosmodel(
                modeltime,
                swarm_data['Radius'].values / 1000,
                90.0 - swarm_data['Latitude'].values,
                swarm_data['Longitude'].values,
                source_list='internal'
            )

            # print(b_chaos_1[0:10], b_chaos_2[0:10], b_chaos_3[0:10])

            swarm_data['Delta_B_NEC_1'] = (
                swarm_data['B_NEC_1'].values + B_theta)
            swarm_data['Delta_B_NEC_2'] = (
                swarm_data['B_NEC_2'].values - B_phi)
            swarm_data['Delta_B_NEC_3'] = (
                swarm_data['B_NEC_3'].values + B_radius)
            swarm_data['Delta_F'] = (
                swarm_data['F'].values -
                np.sqrt(B_radius**2 + B_theta**2 + B_phi**2)
            )

            # Get and clean up the orbit data
            swarm_obj_orbit = swarm_diss.SwarmFiles(
                data_type='MODx_SC',
                sat=f'{satletter.upper()}',
                fast=fast
            )
            swarm_obj_orbit.set_time_interval(
                swarm_data.iloc[0].name - pd.to_timedelta(2, 'min'),
                swarm_data.iloc[-1].name + pd.to_timedelta(2, 'min')
            )
            swarm_orbit = swarm_obj_orbit.to_dataframe()
            swarm_orbit.drop('time_gps', axis=1, inplace=True)
            swarm_orbit = swarm_orbit[~swarm_orbit.index.duplicated()]

            # Interpolate the orbit
            swarm_orbit_interpolated = interpolate_orbit_to_datetimeindex(
                swarm_orbit,
                swarm_data.index
            )

            # Convert to geodetic and quasi-dipole coordinates
            swarm_orbit_geo = itrf_to_geodetic(
                swarm_orbit_interpolated
            )
            swarm_orbit_qd = geodetic_to_qd(
                swarm_orbit_geo
            )

            # Append the orbit data to the Langmuir Probe data
            swarm_data_complete = pd.concat(
                [swarm_data,
                 swarm_orbit_qd[[
                     'lon',
                     'lat',
                     'height',
                     'lon_qd',
                     'lat_qd',
                     'mlt'
                 ]]],
                axis=1
            )

            # Add time-tag as string
            swarm_data_complete['time'] = (
                swarm_data_complete.index.strftime(ISO_TIMEFMT)
            )

            # Store in pickle
            swarm_data_complete.to_pickle(pickle_file)

        swarm_data = swarm_data_complete[parameters].replace(replace_nan_fill)

        # Upload to the data store
        hapi_client.add_data(hapi_server,
                             hapi_server_key,
                             db_id,
                             swarm_data)

        # Append the filename to the list of processed files
        processed_filenames.append(file_info['filename'])
        with open(processed_filelist_file, 'a+') as fh:
            fh.write(file_info['filename'] + '\n')

        # Update the boundaries for processing lower cadence data
        if swarm_data_complete.index[0] < t_first_data:
            t_first_data = swarm_data_complete.index[0]
        if swarm_data_complete.index[-1] > t_last_data:
            t_last_data = swarm_data_complete.index[-1]

        hapi_client.resample_lower_cadences(
            hapi_server,
            hapi_server_key,
            db_id,
            dataframe=dataframe_by_time_closure(satletter, dir='./', fast=fast, prel=prel),
            t_first_data=swarm_data_complete.index[0],
            t_last_data=swarm_data_complete.index[-1]
        )

    return (t_first_data, t_last_data)


def dataframe_by_time_closure(satletter='A', dir='./', fast=False, prel=False):
    def dataframe_by_time(t0, t1):
        if fast:
            fastoper = 'FAST'
        elif prel:
            fastoper = 'PREL'
        else:
            fastoper = 'OPER'

        datatype = f'SW_{fastoper}_MAG{satletter.upper()}_LR'
        filelist_full = sorted(glob.glob(f'{dir}/{datatype}*.pickle'))
        if len(filelist_full) == 0:
            return pd.DataFrame()
        dfs = []
        for file in filelist_full:
            basename = os.path.basename(file)
            file_t0 = pd.to_datetime(basename.split('_')[5], utc=True)
            file_t1 = pd.to_datetime(basename.split('_')[6], utc=True)
            if file_t0 <= t1 and file_t1 >= t0:
                dfs.append(pd.read_pickle(file))
                logging.info(f"Reading dataframe from file: {file}")
        return pd.concat(dfs, axis=0).sort_index().drop_duplicates()
    return dataframe_by_time


def ingest_swarm_all(fast=True, prel=False):
    if fast or prel:
        t_start = pd.to_datetime("2023-04-21T00:00:00", utc=True)
    else:
        t_start = pd.to_datetime("2024-01-01T00:00:00", utc=True)
    t_stop = pd.Timestamp.utcnow()
    for satletter in ['a', 'b', 'c']:
        loggin.info(f"Processing for satellite {satletter}")
        metadata = get_metadata(metadata_json, satletter, fast=fast, prel=prel)
        _ = ingest_swarm_magx_lr(satletter, metadata, t_start, t_stop)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    fast = "oper" not in arguments and "prel" not in arguments
    prel = "oper" not in arguments and "fast" not in arguments
    if arguments[0] not in ['resample', 'aggregate']:
        if fast:
            ingest_swarm_all(fast=True, prel=False)
            schedule.every(15).minutes.do(ingest_swarm_all, fast=True, prel=False)
        elif prel:
            ingest_swarm_all(fast=False, prel=True)
            schedule.every(12).hours.do(ingest_swarm_all, fast=False, prel=True)
        else:
            ingest_swarm_all(fast=False, prel=False)
            schedule.every(12).hours.do(ingest_swarm_all, fast=False, prel=False)

        while True:
            try:
                schedule.run_pending()
            except (BaseException) as err:
                logging.error(f"Exception: {err}")
            time.sleep(1)

    elif arguments[0] == 'resample':
        satletter = arguments[1]
        fastoper = arguments[2]
        if fastoper == 'fast':
            fast = True
            prel = False
        elif fastoper == 'prel':
            fast = False
            prel = True
        else:
            fast = False
            prel = False
        t_start = pd.to_datetime(arguments[3], utc=True)
        t_stop = pd.to_datetime(arguments[4], utc=True)
        logging.info(f"Processing lower cadences for satellite {satletter}")
        metadata = get_metadata(metadata_json, satletter, fast=fast, prel=prel)
        metadata_values = hapi_client.get_info_values(metadata)
        db_id = metadata_values['id']
        hapi_client.resample_lower_cadences(
            hapi_server,
            hapi_server_key,
            db_id,
            dataframe=dataframe_by_time_closure(satletter, dir='./', fast=fast),
            t_first_data=t_start,
            t_last_data=t_stop
        )

    elif arguments[0] == 'aggregate':
        satletter = arguments[1]
        fastoper = arguments[2]
        if fastoper == 'fast':
            fast = True
            prel = False
        elif fastoper == 'prel':
            fast = False
            prel = True
        else:
            fast = False
            prel = False
        t_start = pd.to_datetime(arguments[3], utc=True)
        t_stop = pd.to_datetime(arguments[4], utc=True)
        logging.info(f"Processing lower cadence aggregates for satellite {satletter}")
        metadata = get_metadata(metadata_json_aggregate, satletter, fast=fast, prel=prel)

        info_request = hapi_client.get_info(hapi_server, metadata['id'])
        if info_request['status']['code'] == 1406:
            logging.info(
                f"Creating new dataset {metadata['id']} on server {hapi_server}."
            )
            hapi_client.create_dataset(hapi_server, hapi_server_key, metadata)

        hapi_client.ingest_lower_cadence_aggregates(
            hapi_server,
            hapi_server_key,
            metadata,
            dataframe_by_time_closure(satletter=satletter, dir='./', fast=fast, prel=prel),
            t0=t_start,
            t1=t_stop,
            high_cadence='PT1S',
            min_completeness=0.9
        )
