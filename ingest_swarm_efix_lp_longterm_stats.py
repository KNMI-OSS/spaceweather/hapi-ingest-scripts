#!/usr/bin/env python3
import pandas as pd
import os
import sys
import json
import glob
import logging
import schedule
import time
from swxtools.access import swarm_diss
from swxtools.orbit.transforms import interpolate_orbit_to_datetimeindex, itrf_to_geodetic, geodetic_to_qd
from swxtools import hapi_client
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json_longterm = '''{
    "id": "sw_oper_efia_lp_longterm_stats",
    "description": "Ionospheric plasma data from the Langmuir Probe of the Swarm A satellite",
    "timeStampLocation": "begin",
    "resourceURL": "https://swarm-diss.eo.esa.int/",
    "resourceID": "ESA",
    "contact": "ESA EO Help",
    "contactID": "ESA EO Help",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-9999.0",
            "description": "Timestamp",
            "label": "",
            "key": true
        },
        {
            "name": "Ne_min",
            "type": "double",
            "units": "1/cm3",
            "fill": "-9999.0",
            "description": "Minimum plasma density (electron)",
            "label": "",
            "key": false
        },
        {
            "name": "Ne_mean",
            "type": "double",
            "units": "1/cm3",
            "fill": "-9999.0",
            "description": "Mean plasma density (electron)",
            "label": "",
            "key": false
        },
        {
            "name": "Ne_max",
            "type": "double",
            "units": "1/cm3",
            "fill": "-9999.0",
            "description": "Maximum plasma density (electron)",
            "label": "",
            "key": false
        },
        {
            "name": "Te_min",
            "type": "double",
            "units": "K",
            "fill": "-9999.0",
            "description": "Minimum plasma electron temperature",
            "label": "",
            "key": false
        },
        {
            "name": "Te_mean",
            "type": "double",
            "units": "K",
            "fill": "-9999.0",
            "description": "Mean plasma electron temperature",
            "label": "",
            "key": false
        },
        {
            "name": "Te_max",
            "type": "double",
            "units": "K",
            "fill": "-9999.0",
            "description": "Maximum plasma electron temperature",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1H30M",
    "x_relations": [
        {
            "id": "sw_oper_efia_lp_longterm_stats",
            "description": "Ionospheric plasma data from the Langmuir Probe of the Swarm A satellite, downsampled to 2 second cadence",
            "cadence": "PT6H",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_efia_lp_longterm_stats",
            "description": "Ionospheric plasma data from the Langmuir Probe of the Swarm A satellite, downsampled to 8 second cadence",
            "cadence": "P1D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_efia_lp_longterm_stats",
            "description": "Ionospheric plasma data from the Langmuir Probe of the Swarm A satellite, downsampled to 30 second cadence",
            "cadence": "P4D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "sw_oper_efia_lp_longterm_stats",
            "description": "Ionospheric plasma data from the Langmuir Probe of the Swarm A satellite, downsampled to 2 minute cadence",
            "cadence": "P16D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        }
    ]
}'''


def get_metadata(satletter, fast=False):
    # Update the metadata
    # 1. Set the satellite
    metadata_json_sat = metadata_json.replace(
        'efia_lp',
        f'efi{satletter.lower()}_lp'
    )
    metadata_json_sat = metadata_json_sat.replace(
        'Swarm A',
        f'Swarm {satletter.upper()}'
    )

    # 2. Change oper to fast data, if requested
    if fast:
        metadata_json_sat = metadata_json_sat.replace('oper', 'fast')

    metadata = json.loads(metadata_json_sat)
    return metadata


def dataframe_by_time_closure(satletter='A', dir='./', fast=False):
    def dataframe_by_time(t0, t1):
        if fast:
            fastoper = 'FAST'
        else:
            fastoper = 'OPER'

        datatype = f'SW_{fastoper}_EFI{satletter.upper()}_LP'
        filelist_full = sorted(glob.glob(f'{dir}/{datatype}*.pickle'))
        if len(filelist_full) == 0:
            return pd.DataFrame()
        dfs = []
        for file in filelist_full:
            file_t0 = pd.to_datetime(os.path.basename(file).split('_')[5], utc=True)
            file_t1 = pd.to_datetime(os.path.basename(file).split('_')[6], utc=True)
            if file_t0 <= t1 and file_t1 >= t0:
                dfs.append(pd.read_pickle(file))
                logging.info(f"Reading dataframe from file: {file}")
        return pd.concat(dfs, axis=0).sort_index().drop_duplicates()
    return dataframe_by_time


def ingest_swarm_all(fast=True):
    if fast:
        t_start = pd.to_datetime("2023-04-21T00:00:00", utc=True)
    else:
        t_start = pd.to_datetime("2023-01-01T00:00:00", utc=True)
    t_stop = pd.Timestamp.utcnow()
    for satletter in ['a', 'b', 'c']:
        print(f"Processing for satellite {satletter}")
        metadata = get_metadata(satletter, fast=fast)
        _ = ingest_swarm_efix_lp(satletter, metadata, t_start, t_stop)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    arguments = sys.argv[1:]
    fast = "oper" not in arguments
    if arguments[0] != 'resample':
        if fast:
            ingest_swarm_all(fast=True)
            schedule.every(30).minutes.do(ingest_swarm_all, fast=True)
        else:
            ingest_swarm_all(fast=False)
            schedule.every(12).hours.do(ingest_swarm_all, fast=False)

        while True:
            try:
                schedule.run_pending()
            except (BaseException) as err:
                logging.error(f"Exception: {err}")
            time.sleep(1)

    elif arguments[0] == 'resample':
        satletter = arguments[1]
        fastoper = arguments[2]
        if fastoper == 'fast':
            fast = True
        else:
            fast = False
        t_start = pd.to_datetime(arguments[3], utc=True)
        t_stop = pd.to_datetime(arguments[4], utc=True)
        print(f"Processing lower cadences for satellite {satletter}")
        metadata = get_metadata(satletter, fast=fast)
        metadata_values = hapi_client.get_info_values(metadata)
        db_id = metadata_values['id']
        hapi_client.resample_lower_cadences(
            hapi_server,
            hapi_server_key,
            db_id,
            dataframe=dataframe_by_time_closure(satletter, dir='./', fast=fast),
            t_first_data=t_start,
            t_last_data=t_stop
        )
