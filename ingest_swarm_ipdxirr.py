#!/usr/bin/env python3
import pandas as pd
import os
import sys
import json
import glob
import logging
import schedule
import time
from swxtools.access import swarm_diss
from swxtools.orbit.transforms import interpolate_orbit_to_datetimeindex, itrf_to_geodetic, geodetic_to_qd
from swxtools import hapi_client
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json = '''{
    "id": "sw_oper_ipdairr",
    "description": "Ionospheric plasma irregularities data from the Swarm A satellite",
    "timeStampLocation": "begin",
    "resourceURL": "https://swarm-diss.eo.esa.int/",
    "resourceID": "ESA",
    "contact": "w.j.miloch@fys.uio.no",
    "contactID": "w.j.miloch@fys.uio.no",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "Latitude",
            "type": "double",
            "units": "degrees",
            "fill": "-999.9",
            "description": "Latitude",
            "label": "",
            "key": false
        },
        {
            "name": "Longitude",
            "type": "double",
            "units": "degrees",
            "fill": "-999.9",
            "description": "Longitude",
            "label": "",
            "key": false
        },
        {
            "name": "Radius",
            "type": "double",
            "units": "m",
            "fill": "-999.9",
            "description": "Radius",
            "label": "",
            "key": false
        },
        {
            "name": "Ne",
            "type": "double",
            "units": "1/cm3",
            "fill": "-999.0",
            "description": "Electron density",
            "label": "",
            "key": false
        },
        {
            "name": "Background_Ne",
            "type": "double",
            "units": "1/cm3",
            "fill": "-999.0",
            "description": "Background electron density",
            "label": "",
            "key": false
        },
        {
            "name": "Foreground_Ne",
            "type": "double",
            "units": "1/cm3",
            "fill": "-999.0",
            "description": "Foreground electron density",
            "label": "",
            "key": false
        },
        {
            "name": "Te",
            "type": "double",
            "units": "K",
            "fill": "-999.0",
            "description": "Electron temperature",
            "label": "",
            "key": false
        },
        {
            "name": "PCP_flag",
            "type": "integer",
            "units": "",
            "fill": "-999",
            "description": "Polar cap patch flag",
            "label": "",
            "key": false
        },
        {
            "name": "Grad_Ne_at_100km",
            "type": "double",
            "units": "1/cm3",
            "fill": "-999.0",
            "description": "Electron density gradient over 100 km based on 2 Hz data",
            "label": "",
            "key": false
        },
        {
            "name": "Grad_Ne_at_50km",
            "type": "double",
            "units": "1/cm3",
            "fill": "-999.0",
            "description": "Electron density gradient over 50 km based on 2 Hz data",
            "label": "",
            "key": false
        },
        {
            "name": "Grad_Ne_at_20km",
            "type": "double",
            "units": "1/cm3",
            "fill": "-999.0",
            "description": "Electron density gradient over 20 km based on 2 Hz data",
            "label": "",
            "key": false
        },
        {
            "name": "Grad_Ne_at_PCP_edge",
            "type": "double",
            "units": "1/cm3",
            "fill": "-999.0",
            "description": "The linear electron density gradient calculated over the edges of a patch; non-zero only at the edges of polar cap patches",
            "label": "",
            "key": false
        },
        {
            "name": "ROD",
            "type": "double",
            "units": "1/cm3/s",
            "fill": "-999.0",
            "description": "Rate Of change of Density",
            "label": "",
            "key": false
        },
        {
            "name": "RODI10s",
            "type": "double",
            "units": "1/cm3/s",
            "fill": "-999.0",
            "description": "Rate Of change of Density Index (RODI) over 10 s",
            "label": "",
            "key": false
        },
        {
            "name": "RODI20s",
            "type": "double",
            "units": "1/cm3/s",
            "fill": "-999.0",
            "description": "Rate Of change of Density Index (RODI) over 20 s",
            "label": "",
            "key": false
        },
        {
            "name": "delta_Ne10s",
            "type": "double",
            "units": "1/cm3",
            "fill": "-999.0",
            "description": "Fluctuation amplitudes over the baseline of 10 s",
            "label": "",
            "key": false
        },
        {
            "name": "delta_Ne20s",
            "type": "double",
            "units": "1/cm3",
            "fill": "-999.0",
            "description": "Fluctuation amplitudes over the baseline of 20 s",
            "label": "",
            "key": false
        },
        {
            "name": "delta_Ne40s",
            "type": "double",
            "units": "1/cm3",
            "fill": "-999.0",
            "description": "Fluctuation amplitudes over the baseline of 40 s",
            "label": "",
            "key": false
        },
        {
            "name": "Num_GPS_satellites",
            "type": "integer",
            "units": "",
            "fill": "-999",
            "description": "Total number of tracked GPS satellites above 20 deg",
            "label": "",
            "key": false
        },
        {
            "name": "mVTEC",
            "type": "double",
            "units": "TECU",
            "fill": "-999.0",
            "description": "Median of VTEC from all available GPS satellites with elevation angle above 30deg",
            "label": "",
            "key": false
        },
        {
            "name": "mROT",
            "type": "double",
            "units": "TECU/s",
            "fill": "-999.0",
            "description": "Median of Rate Of TEC (ROT) from all available GPS satellites with elevation above 30\u00b0",
            "label": "",
            "key": false
        },
        {
            "name": "mROTI10s",
            "type": "double",
            "units": "TECU/s",
            "fill": "-999.0",
            "description": "Median of Rate Of TEC Index (ROTI) over 10 s from all available GPS satellites with elevation angle above 30deg",
            "label": "",
            "key": false
        },
        {
            "name": "mROTI20s",
            "type": "double",
            "units": "TECU/s",
            "fill": "-999.0",
            "description": "Median of Rate Of TEC Index (ROTI) over 20 s from all available GPS satellites with elevation angle above 30deg",
            "label": "",
            "key": false
        },
        {
            "name": "IBI_flag",
            "type": "integer",
            "units": "",
            "fill": "-999",
            "description": "Plasma bubble index",
            "label": "",
            "key": false
        },
        {
            "name": "Ionosphere_region_flag",
            "type": "integer",
            "units": "",
            "fill": "-999",
            "description": "Ionosphere region flag (0: equator, 1: mid-latitudes, 2: auroral oval; 3: polar cap)",
            "label": "",
            "key": false
        },
        {
            "name": "IPIR_index",
            "type": "integer",
            "units": "",
            "fill": "-999",
            "description": "0-3 low, 4-5 medium, and > 6 high level of fluctuations in the ionospheric plasma density",
            "label": "",
            "key": false
        },
        {
            "name": "Ne_quality_flag",
            "type": "double",
            "units": "TECU",
            "fill": "-999.0",
            "description": "Quality flag for the Ne data",
            "label": "",
            "key": false
        },
        {
            "name": "TEC_STD",
            "type": "double",
            "units": "TECU",
            "fill": "-999.0",
            "description": "Standard deviation of VTEC from GPS satellites with elevation angle above 30deg",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1S"
}'''


def get_metadata(metadata_json, satletter):
    # Update the metadata
    # 1. Set the satellite
    metadata_json_sat = metadata_json.replace(
        'ipdairr',
        f'ipd{satletter.lower()}irr'
    )
    metadata_json_sat = metadata_json_sat.replace(
        'Swarm A',
        f'Swarm {satletter.upper()}'
    )

    metadata = json.loads(metadata_json_sat)
    return metadata


def ingest_swarm_ipdxirr(
        satletter,
        metadata,
        t_start=pd.to_datetime("1900-01-01T00:00:00Z", utc=True),
        t_stop=pd.to_datetime("2100-01-01T00:00:00Z", utc=True)):

    # Get the necessary info from metadata
    metadata_values = hapi_client.get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    # Info
    info_request = hapi_client.get_info(hapi_server, db_id)

    # Create if the dataset does not exist
    if info_request['status']['code'] == 1406:
        logging.info(
            f"Creating new dataset {db_id} on server {hapi_server}."
        )
        hapi_client.create_dataset(hapi_server, hapi_server_key, metadata)

    # Download latest data files
    for data_type in ['IPDxIRR']:
        swarm_diss.download(sat=f'Swarm {satletter.upper()}',
                            data_type=data_type)

    # Read the list of already processed/ingested data files
    processed_filelist_file = f'processed_files/{db_id}_processed_files.txt'
    if os.path.isfile(processed_filelist_file):
        with open(processed_filelist_file, 'r') as fh:
            processed_filenames = fh.read().splitlines()
    else:
        processed_filenames = []

    # Initial set up of the boundaries for processing lower cadence data
    t_first_data = pd.to_datetime("2100-01-01T00:00:00", utc=True)
    t_last_data = pd.to_datetime("1900-01-01T00:00:00", utc=True)

    # Get the Langmuir Probe data filenames
    swarm_obj_lp = swarm_diss.SwarmFiles(
        data_type='IPDxIRR',
        sat=f'Swarm {satletter.upper()}'
    )
    swarm_obj_lp.set_time_interval(t_start, t_stop)
    filelist = swarm_obj_lp.filelist.to_dict(orient='records')

    for i_file, file_info in enumerate(filelist):

        # Skip if file has already been processed
        if file_info['filename'] in processed_filenames:
            continue

        pickle_file = f"{file_info['filename']}.pickle"

        if os.path.isfile(pickle_file):
            swarm_data_complete = pd.read_pickle(pickle_file)
            logging.info(f"Read data from {pickle_file}")
        else:
            # Load the LP data
            logging.info(f"Processing: {i_file} {file_info['filename']}")
            swarm_data = swarm_obj_lp.to_dataframe_for_file_index(i_file)

            if len(swarm_data) == 0:
                logging.info("No data found, continuing...")
                continue

            swarm_data_complete = swarm_data
            # Add time-tag as string
            swarm_data_complete['time'] = (
                swarm_data_complete.index.strftime(ISO_TIMEFMT)
            )

            # Store in pickle
            swarm_data_complete.to_pickle(pickle_file)

        swarm_data = swarm_data_complete[parameters].replace(replace_nan_fill)
        # Upload to the data store
        hapi_client.add_data(hapi_server,
                             hapi_server_key,
                             db_id,
                             swarm_data)

        # Append the filename to the list of processed files
        processed_filenames.append(file_info['filename'])
        with open(processed_filelist_file, 'a+') as fh:
            fh.write(file_info['filename'] + '\n')

    return (t_first_data, t_last_data)


def dataframe_by_time_closure(satletter='A', dir='./'):
    def dataframe_by_time(t0, t1):
        datatype = f'SW_{fastoper}_IPD{satletter.upper()}IRR'
        filelist_full = sorted(glob.glob(f'{dir}/{datatype}*.pickle'))
        if len(filelist_full) == 0:
            return pd.DataFrame()
        dfs = []
        for file in filelist_full:
            file_t0 = pd.to_datetime(os.path.basename(file).split('_')[5], utc=True)
            file_t1 = pd.to_datetime(os.path.basename(file).split('_')[6], utc=True)
            if file_t0 <= t1 and file_t1 >= t0:
                dfs.append(pd.read_pickle(file))
                logging.info(f"Reading dataframe from file: {file}")
        return pd.concat(dfs, axis=0).sort_index().drop_duplicates()
    return dataframe_by_time


def ingest_swarm_all(fast=True):
    if fast:
        t_start = pd.to_datetime("2023-04-21T00:00:00", utc=True)
    else:
        t_start = pd.to_datetime("2021-01-01T00:00:00", utc=True)
    t_stop = pd.Timestamp.utcnow()
    for satletter in ['a', 'b', 'c']:
        print(f"Processing for satellite {satletter}")
        metadata = get_metadata(metadata_json, satletter)
        _ = ingest_swarm_ipdxirr(satletter, metadata, t_start, t_stop)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    ingest_swarm_all(fast=False)
    schedule.every(12).hours.do(ingest_swarm_all)

    while True:
        try:
            schedule.run_pending()
        except (BaseException) as err:
            logging.error(f"Exception: {err}")
        time.sleep(1)
