#!/usr/bin/env python3
import numpy as np
import pandas as pd
import json
import logging
import schedule
import time

from swxtools.access import silso_sunspot_number, penticton_f10
from swxtools.config import config
from swxtools.hapi_client import ensure_dataset_info, get_info, get_info_values, add_data

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                    level=logging.INFO,
                    datefmt="%Y-%m-%d %H:%M:%S")

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

metadata_json = {}
metadata_json['ssn_daily'] = '''{
    "id": "sunspot_number_silso_daily",
    "description": "Sunspot data from the World Data Center SILSO, Royal Observatory of Belgium, Brussels",
    "timeStampLocation": "begin",
    "resourceURL": "https://www.sidc.be/SILSO/datafiles",
    "resourceID": "SILSO sunspot number",
    "contact": "silso.info@oma.be",
    "contactID": "SILSO info, Royal Observatory of Belgium",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999.9",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "sunspot_number",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Daily sunspot number",
            "label": "",
            "key": false
        },
        {
            "name": "sunspot_number_north",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Daily North sunspot number",
            "label": "",
            "key": false
        },
        {
            "name": "sunspot_number_south",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Daily South sunspot number",
            "label": "",
            "key": false
        },
        {
            "name": "standard_deviation",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Daily sunspot number standard deviation",
            "label": "",
            "key": false
        }
    ],
    "cadence": "P1D"
}'''

metadata_json['ssn_monthly'] = '''{
    "id": "sunspot_number_silso_monthly",
    "description": "Sunspot data from the World Data Center SILSO, Royal Observatory of Belgium, Brussels",
    "timeStampLocation": "begin",
    "resourceURL": "https://www.sidc.be/SILSO/datafiles",
    "resourceID": "SILSO sunspot number",
    "contact": "silso.info@oma.be",
    "contactID": "SILSO info, Royal Observatory of Belgium",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999.9",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "sunspot_number_monthly_mean",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Monthly mean sunspot number",
            "label": "",
            "key": false
        },
        {
            "name": "sunspot_number_monthly_mean_north",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Monthly mean north sunspot number",
            "label": "",
            "key": false
        },
        {
            "name": "sunspot_number_monthly_mean_south",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Monthly mean south sunspot number",
            "label": "",
            "key": false
        },
        {
            "name": "standard_deviation",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Monthly sunspot number standard deviation",
            "label": "",
            "key": false
        }
    ],
    "cadence": "P31D"
}'''

metadata_json['13month_smoothed'] = '''{
    "id": "sunspot_number_silso_monthly_smoothed",
    "description": "Sunspot data from the World Data Center SILSO, Royal Observatory of Belgium, Brussels",
    "timeStampLocation": "begin",
    "resourceURL": "https://www.sidc.be/SILSO/datafiles",
    "resourceID": "SILSO sunspot number",
    "contact": "silso.info@oma.be",
    "contactID": "SILSO info, Royal Observatory of Belgium",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999.9",
            "description": "Time",
            "label": "",
            "key": true
        },
                {
            "name": "sunspot_number_13month_smoothed",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Monthly 13-month smoothed sunspot number",
            "label": "",
            "key": false
        },
        {
            "name": "sunspot_number_13month_smoothed_north",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Monthly 13-month smoothed north sunspot number",
            "label": "",
            "key": false
        },
        {
            "name": "sunspot_number_13month_smoothed_south",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Monthly 13-month smoothed south sunspot number",
            "label": "",
            "key": false
        },
        {
            "name": "standard_deviation",
            "type": "double",
            "units": "",
            "fill": "-999.9",
            "description": "Monthly sunspot number standard deviation",
            "label": "",
            "key": false
        }
    ],
    "cadence": "P31D"
}'''

metadata_json['f10_daily'] = '''{
    "id": "f10_7",
    "description": "DRAO Penticton F10.7",
    "timeStampLocation": "begin",
    "resourceURL": "ftp://ftp.seismo.nrcan.gc.ca/spaceweather/solar_flux/daily_flux_values",
    "resourceID": "DRAO",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999.9",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "f10_7",
            "type": "double",
            "units": "sfu",
            "fill": "-999.9",
            "description": "Penticton noontime 10.7 cm solar radio flux",
            "label": "",
            "key": false
        },
        {
            "name": "f10_7a",
            "type": "double",
            "units": "sfu",
            "fill": "-999.9",
            "description": "Penticton noontime 10.7 cm solar radio flux, 81-day running average",
            "label": "",
            "key": false
        }

    ],
    "cadence": "P1D"
}'''

metadata_json['f10_monthly'] = '''{
    "id": "f10_7_monthly",
    "description": "DRAO Penticton F10.7",
    "timeStampLocation": "begin",
    "resourceURL": "ftp://ftp.seismo.nrcan.gc.ca/spaceweather/solar_flux/daily_flux_values",
    "resourceID": "DRAO",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999.9",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "f10_7a",
            "type": "double",
            "units": "sfu",
            "fill": "-999.9",
            "description": "Penticton noontime 10.7 cm solar radio flux, 81-day running average",
            "label": "",
            "key": false
        },
        {
            "name": "f10_7_monthly_mean",
            "type": "double",
            "units": "sfu",
            "fill": "-999.9",
            "description": "Penticton noontime 10.7 cm solar radio flux, monthly mean",
            "label": "",
            "key": false
        },
        {
            "name": "f10_7_13month_smoothed",
            "type": "double",
            "units": "sfu",
            "fill": "-999.9",
            "description": "Penticton noontime 10.7 cm solar radio flux, 13-month smoothed monthly mean",
            "label": "",
            "key": false
        }
    ],
    "cadence": "P31D"
}'''


def add_silso_sunspots_daily(since="1700-01-01"):
    # Set the timespan
    t0 = pd.to_datetime(since)

    # Set the metadata and create the dataset if necessary
    metadata = json.loads(metadata_json['ssn_daily'])
    _ = ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    # Set the info from the metadata
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    # Get the data
    _ = silso_sunspot_number.download()

    # Final (total) data since 1818
    df = silso_sunspot_number.to_dataframe('daily', 'total')
    df['sunspot_number_north'] = np.nan
    df['sunspot_number_south'] = np.nan
    df.index = df.index + pd.to_timedelta("PT12H")
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    df_in = df[parameters][:"1992-01-01T00:00:00"].replace(replace_nan_fill)[t0:]
    if len(df_in) > 0:
        logging.info(f"Adding final daily sunspot data to {db_id}")
        add_data(hapi_server, hapi_server_key, db_id, df_in)

    # Final (total) hemispheric data since 1992
    df = silso_sunspot_number.to_dataframe('daily', 'hemispheric')
    df.index = df.index + pd.to_timedelta("PT12H")
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    df_in = df[parameters].replace(replace_nan_fill)[t0:]
    if len(df_in) > 0:
        logging.info(f"Adding final daily sunspot data to {db_id}")
        add_data(hapi_server, hapi_server_key, db_id, df_in)

    # Preliminary (current) data (no hemispheric info available, so set these to NaNs)
    df = silso_sunspot_number.to_dataframe('daily', 'current')
    df['sunspot_number_north'] = np.nan
    df['sunspot_number_south'] = np.nan
    df.index = df.index + pd.to_timedelta("PT12H")
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    df_in = df[parameters].replace(replace_nan_fill)[t0:]
    if len(df_in) > 0:
        logging.info(f"Adding preliminary daily sunspot data to {db_id}")
        add_data(hapi_server, hapi_server_key, db_id, df_in)


def add_silso_sunspots_monthly(since="1700-01-01"):
    # Set the timespan
    t0 = pd.to_datetime(since)

    metadata = json.loads(metadata_json['ssn_monthly'])
    _ = ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    # Set the info from the metadata
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    # Get the data
    _ = silso_sunspot_number.download()

    # Final (total) data since 1818
    df = silso_sunspot_number.to_dataframe('monthly', 'total')
    df.rename({'sunspot_number': 'sunspot_number_monthly_mean'}, axis=1, inplace=True)
    df['sunspot_number_monthly_mean_north'] = np.nan
    df['sunspot_number_monthly_mean_south'] = np.nan
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    df_in = df[parameters][:"1992-01-01T00:00:00"].replace(replace_nan_fill)[t0:]
    if len(df_in) > 0:
        logging.info(f"Adding final monthly mean sunspot data to {db_id}")
        add_data(hapi_server, hapi_server_key, db_id, df_in)

    # # Final (total) hemispheric data since 1992
    df = silso_sunspot_number.to_dataframe('monthly', 'hemispheric')
    df.rename({'sunspot_number': 'sunspot_number_monthly_mean',
                'sunspot_number_north': 'sunspot_number_monthly_mean_north',
                'sunspot_number_south': 'sunspot_number_monthly_mean_south'}, axis=1, inplace=True)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    df_in = df[parameters].replace(replace_nan_fill)[t0:]
    if len(df_in) > 0:
        logging.info(f"Adding final monthly mean sunspot data to {db_id}")
        add_data(hapi_server, hapi_server_key, db_id, df_in)


def add_silso_sunspots_monthly_smoothed(since="1700-01-01"):
    # Set the timespan
    t0 = pd.to_datetime(since)

    metadata = json.loads(metadata_json['13month_smoothed'])
    _ = ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    # Set the info from the metadata
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    # Get the data
    _ = silso_sunspot_number.download()

    # Final (total) data since 1818
    df = silso_sunspot_number.to_dataframe('13m_smoothed', 'total')
    df.rename({'sunspot_number': 'sunspot_number_13month_smoothed'}, axis=1, inplace=True)
    df['sunspot_number_13month_smoothed_north'] = np.nan
    df['sunspot_number_13month_smoothed_south'] = np.nan
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    df_in = df[parameters][:"1992-07-01T00:00:00"].replace(replace_nan_fill)[t0:]
    if len(df_in) > 0:
        logging.info(f"Adding final 13-month smoothed sunspot data to {db_id}")
        add_data(hapi_server, hapi_server_key, db_id, df_in)

    # Final (total) hemispheric data since 1992
    df = silso_sunspot_number.to_dataframe('13m_smoothed', 'hemispheric')
    df.rename({'sunspot_number': 'sunspot_number_13month_smoothed',
                'sunspot_number_north': 'sunspot_number_13month_smoothed_north',
                'sunspot_number_south': 'sunspot_number_13month_smoothed_south'}, axis=1, inplace=True)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    df_in = df[parameters]["1992-07-01T00:00:00":].replace(replace_nan_fill)[t0:]
    if len(df_in) > 0:
        logging.info(f"Adding final 13-month smoothed sunspot data to {db_id}")
        add_data(hapi_server, hapi_server_key, db_id, df_in)


def add_f10_7_daily(since="1700_01-01"):
    # Set the timespan
    t0 = pd.to_datetime(since, utc=True)

    metadata = json.loads(metadata_json['f10_daily'])
    _ = ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    # Set the info from the metadata
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    # Get the data
    _ = penticton_f10.download()
    df = penticton_f10.to_dataframe(
                noontime=True,
                drop_outliers=True,
                merge=True,
                timestamp='mid'
         )
    rolling = df['f10_7'].rolling(window=pd.to_timedelta('81D'),
                                  closed='neither', center=True)
    df_f107a = pd.DataFrame({'f10_7a': rolling.mean()})
    df = df[['f10_7']].join(df_f107a)

    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    df_in = df[parameters].replace(replace_nan_fill)[t0:]

    # Upload the data
    add_data(hapi_server, hapi_server_key, db_id, df_in)


def add_f10_7_monthly(since="1700-01-01"):
    # Set the timespan
    t0 = pd.to_datetime(since, utc=True)

    metadata = json.loads(metadata_json['f10_monthly'])
    _ = ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    # Set the info from the metadata
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    # Get the data
    _ = penticton_f10.download()
    df = penticton_f10.to_dataframe(
                noontime=True,
                drop_outliers=True,
                merge=True,
                timestamp='mid'
         )
    rolling = df['f10_7'].rolling(window=pd.to_timedelta('81D'),
                                  closed='neither', center=True)
    df_f107a = pd.DataFrame({'f10_7a': rolling.mean()})
    df_f107a.index = df_f107a.index - pd.to_timedelta("PT12H")
    f10_7_monthly = pd.DataFrame({'f10_7_monthly_mean': df['f10_7'].groupby(pd.PeriodIndex(df.index, freq="M")).mean()})
    f10_7_13month_smoothed = f10_7_monthly.rolling(13, center=True).mean().rename({'f10_7_monthly_mean': 'f10_7_13month_smoothed'}, axis=1)
    df = pd.concat([f10_7_monthly, f10_7_13month_smoothed], axis=1)
    df.index = df.index.to_timestamp().tz_localize('utc')
    df = df.join(df_f107a)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    df_in = df[parameters].replace(replace_nan_fill)

    # Upload the data
    add_data(hapi_server, hapi_server_key, db_id, df_in)

if __name__ == "__main__":
    add_silso_sunspots_daily(since=pd.Timestamp.now() - pd.to_timedelta('P31D'))
    add_silso_sunspots_monthly(since=pd.Timestamp.now() - pd.to_timedelta('P100D'))
    add_silso_sunspots_monthly_smoothed(since=pd.Timestamp.now() - pd.to_timedelta('P100D'))
    add_f10_7_daily(since=pd.Timestamp.now() - pd.to_timedelta('P31D'))
    add_f10_7_monthly(since=pd.Timestamp.now() - pd.to_timedelta('P100D'))
    schedule.every(24).hours.do(add_silso_sunspots_daily, since=pd.Timestamp.now()-pd.to_timedelta('P31D'))
    schedule.every(48).hours.do(add_silso_sunspots_monthly, since=pd.Timestamp.now()-pd.to_timedelta('P12M'))
    schedule.every(48).hours.do(add_silso_sunspots_monthly_smoothed, since="1700-01-01")
    schedule.every(24).hours.do(add_f10_7_daily, since=pd.Timestamp.now()-pd.to_timedelta('P31D'))
    schedule.every(48).hours.do(add_f10_7_monthly, since=pd.Timestamp.now()-pd.to_timedelta('P12M'))

    while True:
        try:
            schedule.run_pending()
        except (BaseException) as err:
            logging.error(f"Exception: {err}")
            time.sleep(3600)
        time.sleep(1)

