#!/usr/bin/env python3
import pandas as pd
import json
import logging
import requests.exceptions
import schedule
import time
import sys
from swxtools.access import gfz_kp_hp_indices
from swxtools.hapi_client import get_info, create_dataset, add_data, resample_lower_cadences
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"
metadata_json = {}
metadata_json['Hp30'] = '''{
    "id": "hp30_index",
    "description": "Geomagnetic Hp30 index",
    "timeStampLocation": "begin",
    "resourceURL": "https://www.gfz-potsdam.de/en/hpo-index/",
    "resourceID": "GFZ Potsdam",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "Time",
            "key": true
        },
        {
            "name": "Hp30",
            "type": "double",
            "units": "",
            "fill": "-9999.0",
            "description": "Geomagnetic Hp30 index",
            "label": "Hp30",
            "key": false
        }
    ],
    "cadence": "PT30M",
    "x_relations": [
        {
            "id": "hp30_index_PT3H",
            "description": "Geomagnetic Hp30 index, downsampled maximum over 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "hp30_index_P1D",
            "description": "Geomagnetic Hp30 index, downsampled maximum over 1 day",
            "cadence": "P1D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "hp30_index_P7D",
            "description": "Geomagnetic Hp30 index, downsampled maximum over 7 days",
            "cadence": "P7D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "hp30_index_P31D",
            "description": "Geomagnetic Hp30 index, downsampled maximum over 31 days",
            "cadence": "P31D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        }
    ]
}'''

metadata_json['Kp'] = '''{
    "id": "kp_index",
    "description": "Geomagnetic Kp index",
    "timeStampLocation": "begin",
    "resourceURL": "https://www.gfz-potsdam.de/en/hpo-index/",
    "resourceID": "GFZ Potsdam",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "Time",
            "key": true
        },
        {
            "name": "Kp",
            "type": "double",
            "units": "",
            "fill": "-9999.0",
            "description": "Geomagnetic Kp index",
            "label": "Hp30",
            "key": false
        }
    ],
    "cadence": "PT3H",
    "x_relations": [
        {
            "id": "kp_index_P1D",
            "description": "Geomagnetic Kp index, downsampled maximum over 1 day",
            "cadence": "P1D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "kp_index_P7D",
            "description": "Geomagnetic Kp index, downsampled maximum over 7 days",
            "cadence": "P7D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "kp_index_P31D",
            "description": "Geomagnetic Kp index, downsampled maximum over 31 days",
            "cadence": "P31D",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        }
    ]
}'''


# Get the id, parameters and cadences from the definition
metadata = json.loads(metadata_json['Hp30'])
db_id = metadata['id']
cadences = [relation['cadence'] for relation in metadata['x_relations']]
parameters = [param['name'] for param in metadata['parameters']]


def check_metadata_info(hapi_server, hapi_server_key, metadata):
    # Get info
    info_request = get_info(hapi_server, metadata['id'])
    status_code = info_request['status']['code']

    if status_code == 1200:  # Ok
        # TODO: check if the metadata matches the info, or else raise an error
        return info_request
    elif status_code == 1406:
        # Dataset does not exist
        logging.info(f"Dataset {metadata['id']} does not exist, adding")
        create_dataset(hapi_server, hapi_server_key, metadata)
        # Get info again
        info_request = get_info(hapi_server, metadata['id'])
        status_code = info_request['status']['code']
        if status_code == 1200:  # OK
            return info_request
        else:
            raise ValueError(f"Error creating dataset {metadata['id']} "
                             f"on {hapi_server}")
    else:
        raise ValueError("HAPI status code not yet implemented: "
                         f"{status_code}")


def store_gfz_index_file(index_type, filename):
    logging.info(f"Storing {filename} in data store.")
    df = gfz_kp_hp_indices.txt_to_dataframe(filename)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    parameters = ['time', index_type]
    data = df[parameters].dropna()
    add_data(hapi_server,
             hapi_server_key,
             db_id,
             data)


def store_gfz_index(index_type='Hp30'):
    filenames = gfz_kp_hp_indices.download_data(index_type=index_type)
    for filename in filenames:
        logging.info(f"Storing {filename}")
        store_gfz_index_file(index_type, filename)


def store_gfz_index_latest(index_type='Hp30', past_days=3):
    new_data_stop = pd.Timestamp.utcnow()
    info = check_metadata_info(hapi_server, hapi_server_key, metadata)
    if info['startDate'] == '':
        if index_type == 'Hp30':
            new_data_start = pd.to_datetime("1985-01-01T00:00:00", utc=True)
        elif index_type == 'Kp':
            new_data_start = pd.to_datetime("1932-01-01T00:00:00", utc=True)
    else:
        new_data_start = (pd.to_datetime(info['stopDate'], utc=True) -
                          pd.to_timedelta(past_days, 'D'))
    logging.info(f"Downloading {index_type} data via GFZ API "
                 f"for {new_data_start} - {new_data_stop}")
    df = gfz_kp_hp_indices.dataframe_from_gfz_api(new_data_start,
                                                  new_data_stop,
                                                  index_type=index_type)
    if len(df) == 0:
       logging.error("Empty dataframe received from GFZ-Potsdam API")
       return

    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    parameters = ['time', index_type]
    data = df[parameters].dropna()

    add_data(
        hapi_server,
        hapi_server_key,
        db_id,
        data
    )

    resample_lower_cadences(
        hapi_server,
        hapi_server_key,
        db_id,
        t_first_data=df.index[0],
        t_last_data=df.index[-1],
        resample_method='max'
    )


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    if 'Kp' in sys.argv[1:]:
        index_type = 'Kp'
    else:
        index_type = 'Hp30'

    # Get the id, parameters and cadences from the definition
    metadata = json.loads(metadata_json[index_type])
    db_id = metadata['id']
    cadences = [relation['cadence'] for relation in metadata['x_relations']]
    parameters = [param['name'] for param in metadata['parameters']]

    if 'resample_all' in sys.argv[1:]:
        resample_lower_cadences(
            hapi_server,
            hapi_server_key,
            db_id,
            t_first_data=pd.to_datetime("1932-01-01T", utc=True),
            t_last_data=pd.Timestamp.utcnow(),
            resample_method='max'
        )

    store_gfz_index_latest(index_type=index_type)
    schedule.every(15).minutes.do(store_gfz_index_latest, index_type=index_type)

    while True:
        try:
            schedule.run_pending()
        except requests.exceptions.ConnectionError as err:
            logging.error(f"Connection error: {err}")
            time.sleep(600)
        time.sleep(1)
