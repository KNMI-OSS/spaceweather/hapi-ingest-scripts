#
# This file is part of hapi-ingest-scripts
#
# https://gitlab.com/KNMI-OSS/spaceweather/hapi-ingest-scripts
#
# Copyright (c) 2024 KNMI
# All Rights Reserved
"""Process MetOffice space weather events using swxtools and upload them to the hapi server."""

import json
import numpy as np
import pandas as pd
import argparse
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
    resample_lower_cadences,
)
from swxtools.config import config
import swxtools.access.metoffice_events as swxtools_metoffice
from os import walk

# - global parameters ------------------------------
hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

# Time format for timeline viewer
ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

# Json structure for all event types
metadata_json_metoffice_electron = '''{
    "id": "electron_alerts_metoffice",
    "description": "Electron alerts from MetOffice",
    "timeStampLocation": "begin",
    "resourceURL": "https://surfdrive.surf.nl/files/index.php/s/3ruClgXLDRo04oy",
    "resourceID": "MetOffice Space Weather Operations Centre",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "predicted_fluence",
            "type": "double",
            "units": "particles cm-2 day-1 sr-1",
            "fill": "-999",
            "description": "Predicted fluence (only for warnings)",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

metadata_json_metoffice_geomagnetic = '''{
    "id": "geomagnetic_alerts_metoffice",
    "description": "Geomagnetic alerts from MetOffice",
    "timeStampLocation": "begin",
    "resourceURL": "https://surfdrive.surf.nl/files/index.php/s/3ruClgXLDRo04oy",
    "resourceID": "MetOffice Space Weather Operations Centre",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "noaa_scale",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "NOAA scale",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

metadata_json_metoffice_proton = '''{
    "id": "proton_alerts_metoffice",
    "description": "Electron flux alerts from MetOffice",
    "timeStampLocation": "begin",
    "resourceURL": "https://surfdrive.surf.nl/files/index.php/s/3ruClgXLDRo04oy",
    "resourceID": "MetOffice Space Weather Operations Centre",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "noaa_scale",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "NOAA scale",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

metadata_json_metoffice_xray = '''{
    "id": "xray_alerts_metoffice",
    "description": "Electron flux alerts from MetOffice",
    "timeStampLocation": "begin",
    "resourceURL": "https://surfdrive.surf.nl/files/index.php/s/3ruClgXLDRo04oy",
    "resourceID": "MetOffice Space Weather Operations Centre",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Issue date/time",
            "label": "",
            "key": true
        },
        {
            "name": "product_id",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Product ID",
            "label": "",
            "key": false
        },
        {
            "name": "begin_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "Begin time",
            "label": "",
            "key": false
        },
        {
            "name": "end_time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-999",
            "description": "End time",
            "label": "",
            "key": false
        },
        {
            "name": "peak_flux",
            "type": "double",
            "units": "W m-2",
            "fill": "-999",
            "description": "Peak flux",
            "label": "",
            "key": false
        },
        {
            "name": "message",
            "type": "string",
            "units": "",
            "fill": "-999",
            "description": "Message",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

def argument_parser():
    """Process the command line arguments to settings for the program

    Parameters
    ----------
    None

    Returns
    -------
    settings_dict : dict
        A dictionary with values for the different program settings.
    """

    # Set up parser
    parser = argparse.ArgumentParser()

    ISO_TIMEFMT_PARSER = "%Y-%m-%d"

    # Choices for parameters
    notif_sets = ['ALL', 'RE', 'AR', 'TEST']
    event_types = ['ALL', 'EL', 'GEO', 'PRO', 'XRAY']

    # Add potential parser arguments
    parser.add_argument('--t0', '--start',
                        help='Start date/time, Y-m-d format')
    parser.add_argument('--t1', '--stop',
                        help='Stop date/time, Y-m-d format')

    parser.add_argument('--sets', choices=notif_sets, nargs='+', help='Choose which data sets to use')
    parser.add_argument('--types', choices=event_types, nargs='+', help='Choose which event types to process')

    parser.add_argument("--upload", help="upload the processed NOAA notifications")

    # Retrieve arguments from command line
    args = parser.parse_args()
    settings_dict = {}

    # Save arguments to a usable dict
    if 'RE' in args.sets or 'ALL' in args.sets:
        settings_dict['recent'] = True
    else:
        settings_dict['recent'] = False
    if 'AR' in args.sets or 'ALL' in args.sets:
        settings_dict['archive'] = True
    else:
        settings_dict['archive'] = False
    if 'TEST' in args.sets or 'ALL' in args.sets:
        settings_dict['test'] = True
    else:
        settings_dict['test'] = False

    if 'EL' in args.types or 'ALL' in args.types:
        settings_dict['electron'] = True
    else:
        settings_dict['electron'] = False
    if 'GEO' in args.types or 'ALL' in args.types:
        settings_dict['geomagnetic'] = True
    else:
        settings_dict['geomagnetic'] = False
    if 'PRO' in args.types or 'ALL' in args.types:
        settings_dict['proton'] = True
    else:
        settings_dict['proton'] = False
    if 'XRAY' in args.types or 'ALL' in args.types:
        settings_dict['xray'] = True
    else:
        settings_dict['xray'] = False

    if args.upload == 'y':
        settings_dict['upload'] = True
    else:
        settings_dict['upload'] = False

    if args.t0 and args.t1:
        settings_dict['start_date'] = args.t0
        settings_dict['end_date'] = args.t1

    return settings_dict


def process_metoffice(parser_data: dict) -> dict:
    """Process the MetOffice events to upload-ready notifications

    Parameters
    ----------
    parser_data : dict
        A dictionary from the argument parser
        with values for the different program settings.

    Returns
    -------
    notifications : dict
        A dictionary with notification dataframes for each of the event types.
    """

    electron_dataframes = []
    geomagnetic_dataframes = []
    proton_dataframes = []
    xray_dataframes = []

    # Archive pipeline
    if parser_data['archive']:
        # TODO, no archive available as of yet, uncomment code when this is the case
        print("Start downloading (archive)")
        # swxtools_metoffice.download_metoffice_html()
        print("Finished downloading (archive)")

        # Make list of html files
        # read_list = []
        # path = config['local_source_data_path'] + '/metoffice_events'
        #
        # for root, dirc, files in walk(path):
        #     for FileName in files:
        #         if '.zip' not in FileName:
        #             read_list.append(path + '/' + FileName)

        print("Start parsing (archive)")
        # archive_dataframes = swxtools_metoffice.parse_metoffice_html_to_dataframe(read_list, parser_data)
        print("Finished parsing (archive)")

        # Add parser output to input dataframes for uploading
        # if parser_data['electron']:
        #     electron_dataframes.append(archive_dataframes['electron'])
        # if parser_data['geomagnetic']:
        #     geomagnetic_dataframes.append(archive_dataframes['geomagnetic'])
        # if parser_data['proton']:
        #     proton_dataframes.append(archive_dataframes['proton'])
        # if parser_data['xray']:
        #     xray_dataframes.append(archive_dataframes['xray'])

    # Recent data pipeline
    if parser_data['recent']:
        # TODO, no recent available as of yet, uncomment code when this is the case
        print("Start downloading (recent)")
        # recent_notifications = swxtools_metoffice.download_metoffice_recent_json()
        print("Finished downloading (recent)")

        print("Start parsing (recent)")
        # recent_dataframes = swxtools_metoffice.parse_metoffice_recent_json_to_dataframe(recent_notifications, parser_data)
        print("Finished parsing (recent)")

        # Add parser output to input dataframes for uploading
        # if parser_data['electron']:
        #     electron_dataframes.append(recent_dataframes['electron'])
        # if parser_data['geomagnetic']:
        #     geomagnetic_dataframes.append(recent_dataframes['geomagnetic'])
        # if parser_data['proton']:
        #     proton_dataframes.append(recent_dataframes['proton'])
        # if parser_data['xray']:
        #     xray_dataframes.append(recent_dataframes['xray'])

    # For now only this test dataset is available
    csv_dict = {}
    if parser_data['test']:
        csv_dict = swxtools_metoffice.parse_metoffice_csv_to_dict(['metoffice_advisories.csv'])

    # Convert
    print("Start converting")
    electron_notifications_final = swxtools_metoffice.convert_metoffice_electron([csv_dict['electron']])
    geomagnetic_notifications_final = swxtools_metoffice.convert_metoffice_geomagnetic([csv_dict['geomagnetic']])
    proton_notifications_final = swxtools_metoffice.convert_metoffice_proton([csv_dict['proton']])
    xray_notifications_final = swxtools_metoffice.convert_metoffice_xray([csv_dict['xray']])
    print("Finished converting")

    # Print results
    print("Print results")
    print("Electron events", electron_notifications_final)
    print("Geomagnetic events", geomagnetic_notifications_final)
    print("Proton events", proton_notifications_final)
    print("X-ray events", xray_notifications_final)
    print("Finished printing results")

    notifications_dict = {'electron': electron_notifications_final,
                          'geomagnetic': geomagnetic_notifications_final,
                          'proton': proton_notifications_final,
                          'xray': xray_notifications_final}

    return notifications_dict


def upload_metoffice(upload_data: dict, parser_data: dict):
    """Upload the MetOffice events to the hapi server

    Parameters
    ----------
    upload_data : dict
        A dictionary from the process MetOffice function
        with the dataframes of MetOffice events to upload to the hapi server.
    parser_data : dict
        A dictionary from the argument parser
        with values for the different program settings.

    Returns
    -------
    None

    Uploads data to the hapi server
    """

    # Go over processed lists and upload one by one
    for data_type, data_to_upload in upload_data.items():

        # Set metadata for json depending on data type
        if data_type == 'electron' and parser_data['electron']:
            metadata = json.loads(metadata_json_metoffice_electron)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        elif data_type == 'geomagnetic' and parser_data['geomagnetic']:
            metadata = json.loads(metadata_json_metoffice_geomagnetic)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        elif data_type == 'proton' and parser_data['proton']:
            metadata = json.loads(metadata_json_metoffice_proton)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        elif data_type == 'xray' and parser_data['proton']:
            metadata = json.loads(metadata_json_metoffice_xray)
            info_metadata = ensure_dataset_info(hapi_server, hapi_server_key, metadata)
        else:
            print('Data type ', data_type, ' skipped')
            continue

        # Set other settings
        metadata_values = get_info_values(metadata)
        db_id = metadata_values['id']
        parameters = metadata_values['parameters']
        replace_nan_fill = metadata_values['replace_nan_fill']

        # Retrieve alerts and put in dataframe
        notifications = data_to_upload

        # Check whether any notifications are present
        if len(notifications) > 0:
            df = pd.DataFrame(notifications)
            df.index = pd.to_datetime(df['issue_datetime'], format='%Y-%m-%dT%H:%M:%SZ')

            # Convert time to correct format
            df['time'] = df.index.strftime(ISO_TIMEFMT)

            df['begin_time'] = pd.to_datetime(df['begin_time'], format='%Y-%m-%dT%H:%M:%SZ').dt.strftime(
                ISO_TIMEFMT).replace(
                {np.nan: '-999'})
            df['end_time'] = pd.to_datetime(df['end_time'],
                                            format='%Y-%m-%dT%H:%M:%SZ').dt.strftime(ISO_TIMEFMT).replace(
                {np.nan: '-999'})

            if data_type == 'electron':
                df['predicted_fluence'] = df['predicted_fluence'].replace({np.nan: -999, '': -999})
            if data_type == 'xray':
                df['peak_flux'] = df['peak_flux'].replace({np.nan: -999})

            data = df[parameters].replace(replace_nan_fill)

            # Potentially print data per row for final check
            # passed_ids = []
            # for index, row in data.iterrows():
            #     if row['product_id'] not in passed_ids:
            #         print(row['product_id'])
            #         passed_ids.append(row['product_id'])
            # print('Passed ids', passed_ids)

            # Upload to the hapi server
            add_data(hapi_server,
                     hapi_server_key,
                     db_id,
                     data)


# Run test files when file is directly called
if __name__ == "__main__":

    # Argument parser
    parser_output = argument_parser()
    print("Parser output", parser_output)

    # Process MetOffice data using swxtools
    process_output = process_metoffice(parser_output)
    print("Process output", process_output)

    # Upload processed MetOffice data to the server
    if parser_output['upload']:
        upload_metoffice(process_output, parser_output)
        print("Upload succesful")
