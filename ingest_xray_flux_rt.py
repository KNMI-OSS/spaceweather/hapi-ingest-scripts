#!/usr/bin/env python3
import json
import sys
import logging
import schedule
import time
import numpy as np
import pandas as pd
from swxtools.access import swpc_rt
from swxtools.dataframe_tools import mark_gaps_in_dataframe
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
    resample_lower_cadences,
)
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json = '''{
    "id": "xray_flux_rt",
    "description": "Real-time GOES X-ray flux",
    "timeStampLocation": "begin",
    "resourceURL": "https://services.swpc.noaa.gov/json/goes/primary/",
    "resourceID": "NOAA Space Weather Prediction Center",
    "contact": "",
    "contactID": "",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "",
            "description": "Time",
            "label": "",
            "key": true
        },
        {
            "name": "xray_flux_short",
            "type": "double",
            "units": "W/m²",
            "fill": "-1e30",
            "description": "Solar X-ray flux in the 0.05-0.4 nm passband",
            "label": "",
            "key": false
        },
        {
            "name": "xray_flux_long",
            "type": "double",
            "units": "W/m²",
            "fill": "-1e30",
            "description": "Solar X-ray flux in the 0.1-0.8 nm passband",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1M",
    "x_relations": [
        {
            "id": "xray_flux_rt_PT5M",
            "description": "Real-time GOES X-ray flux downsampled to 5 minutes",
            "cadence": "PT5M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "xray_flux_rt_PT30M",
            "description": "Real-time GOES X-ray flux downsampled to 30 minutes",
            "cadence": "PT30M",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "xray_flux_rt_PT3H",
            "description": "Real-time GOES X-ray flux downsampled to 3 hours",
            "cadence": "PT3H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "xray_flux_rt_PT12H",
            "description": "Real-time GOES X-ray flux downsampled to 12 hours",
            "cadence": "PT12H",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "xray_flux_rt_P3D",
            "description": "Real-time GOES X-ray flux downsampled to 3 days",
            "cadence": "P3D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        },
        {
            "id": "xray_flux_rt_P10D",
            "description": "Real-time GOES X-ray flux downsampled to 10 days",
            "cadence": "P10D",
            "type": "resample",
            "method": "mean",
            "add": "automatic"
        }
    ]
}'''


def store_xrays_df(metadata, df, resample=True):
    # Get the id, parameters and cadences from the definition
    metadata_values = get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']
    cadence = metadata_values['cadence']
    df = mark_gaps_in_dataframe(
        df,
        nominal_timedelta=pd.to_timedelta(cadence),
        nominal_start_time=None,
        nominal_end_time=df.index[-1]+pd.to_timedelta(cadence)
    )
    df = df.rename({'0.05-0.4nm': 'xray_flux_short',
                    '0.1-0.8nm': 'xray_flux_long'}, axis=1)
    df = df.where(df['xray_flux_long'] > 1e-10, other=np.nan)
    df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    data = df[parameters].replace(replace_nan_fill)

    add_data(hapi_server,
             hapi_server_key,
             db_id,
             data)

    if resample:
        resample_lower_cadences(hapi_server,
                                hapi_server_key,
                                db_id,
                                t_first_data=df.index[0],
                                t_last_data=df.index[-1])


def store_xrays_file(metadata, filename, resample=True):
    logging.info(f"Storing {filename} in data store.")
    df = swpc_rt.json_to_dataframe(filename)
    store_xrays_df(metadata, df, resample)


def store_xrays(metadata, duration='1-day', primary_secondary='primary'):
    if primary_secondary == 'both':
        versions = ['secondary', 'primary']
    else:
        versions = [primary_secondary]

    filenames = {}
    dfs = {}
    # Download the files
    for version in versions:
        filenames[version] = swpc_rt.download_data(
            datatype='xrays',
            duration=duration,
            primary_secondary=version)

    if primary_secondary != 'both':
        for version in versions:
            for filename in filenames[version]:
                store_xrays_file(metadata, filename)
    else:
        dfs = {}
        df = {}
        for version in versions:
            dfs[version] = []
            for file in filenames[version]:
                dfs[version].append(
                    swpc_rt.json_to_dataframe(file)
                )
            df[version] = pd.concat(dfs[version])
        df = df['primary'].where(df['primary'].notna(), df['secondary'])
        store_xrays_df(metadata, df)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    metadata = json.loads(metadata_json)
    info = ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    arguments = sys.argv[1:]
    if len(arguments) == 0:
        store_xrays(metadata=metadata, duration='7-day',
                    primary_secondary='both')

        schedule.every(5).minutes.do(store_xrays,
                                     duration='1-day',
                                     metadata=metadata,
                                     primary_secondary='both')

        while True:
            try:
                schedule.run_pending()
            except (BaseException) as err:
                logging.error(f"Exception: {err}")
            time.sleep(1)

    else:
        for filename in arguments:
            store_xrays_file(
                metadata=metadata,
                filename=filename,
                resample=False
            )
