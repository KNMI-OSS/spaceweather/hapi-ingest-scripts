#!/usr/bin/env python3
import pandas as pd
import os
import sys
import json
import glob
import logging
import schedule
import time
from swxtools.access import swarm_diss
from swxtools.orbit import transforms
from swxtools import hapi_client
from swxtools.config import config

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json = '''{
    "id": "gdc_sat01_orbit",
    "description": "Orbit data from the GDC 1 satellite",
    "timeStampLocation": "begin",
    "resourceURL": "http://gdc.smce.nasa.gov/",
    "resourceID": "NASA",
    "contact": "",
    "contactID": "",
    "cadence": "PT30S",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-9999.0",
            "description": "Timestamp",
            "label": "",
            "key": true
        },
        {
            "name": "x_itrf",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "ECEF Cartesian position, X-component",
            "label": "",
            "key": false
        },
        {
            "name": "y_itrf",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "ECEF Cartesian position, Y-component",
            "label": "",
            "key": false
        },
        {
            "name": "z_itrf",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "ECEF Cartesian position, Z-component",
            "label": "",
            "key": false
        },
        {
            "name": "x_gcrs",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "GCRS Cartesian position, X-component",
            "label": "",
            "key": false
        },
        {
            "name": "y_gcrs",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "GCRS Cartesian position, Y-component",
            "label": "",
            "key": false
        },
        {
            "name": "z_gcrs",
            "type": "double",
            "units": "km",
            "fill": "-9999.0",
            "description": "GCRS Cartesian position, Z-component",
            "label": "",
            "key": false
        },
        {
            "name": "lon",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Geographic longitude",
            "label": "",
            "key": false
        },
        {
            "name": "lat",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Geodetic latitude",
            "label": "",
            "key": false
        },
        {
            "name": "height",
            "type": "double",
            "units": "m",
            "fill": "-9999.0",
            "description": "Geodetic altitude",
            "label": "",
            "key": false
        },
        {
            "name": "lon_qd",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Quasi-dipole longitude",
            "label": "",
            "key": false
        },
        {
            "name": "lat_qd",
            "type": "double",
            "units": "deg",
            "fill": "-9999.0",
            "description": "Quasi-dipole latitude",
            "label": "",
            "key": false
        },
        {
            "name": "mlt",
            "type": "double",
            "units": "hours",
            "fill": "-9999.0",
            "description": "Quasi-dipole magnetic local time",
            "label": "",
            "key": false
        }
    ],
    "x_relations": [
        {
            "id": "gdc_sat01_orbit_PT2M",
            "description": "Orbit data from the GDC 1 satellite, downsampled to 2 minute cadence",
            "cadence": "PT2M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        },
        {
            "id": "gdc_sat01_orbit_PT6M",
            "description": "Orbit data from the GDC 1 satellite, downsampled to 6 minute cadence",
            "cadence": "PT6M",
            "type": "resample",
            "method": "max",
            "add": "automatic"
        }
    ]
}'''

metadata_json_aggregate = '''{
    "id": "gdc_sat01_orbit_aggregate",
    "description": "Lower cadence aggregate statistics of the orbit data from the Swarm A satellite",
    "timeStampLocation": "begin",
    "resourceURL": "https://swarm-diss.eo.esa.int/",
    "resourceID": "ESA",
    "contact": "ESA EO Help",
    "contactID": "ESA EO Help",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-9999.0",
            "description": "Timestamp",
            "label": "",
            "key": true
        },
        {
            "name": "height_min",
            "type": "double",
            "units": "m",
            "fill": "-9999.0",
            "description": "Minimum geodetic altitude",
            "label": "",
            "key": false
        },
        {
            "name": "height_mean",
            "type": "double",
            "units": "m",
            "fill": "-9999.0",
            "description": "Mean geodetic altitude",
            "label": "",
            "key": false
        },
        {
            "name": "height_max",
            "type": "double",
            "units": "m",
            "fill": "-9999.0",
            "description": "Maximum geodetic altitude",
            "label": "",
            "key": false
        },
        {
            "name": "coverage",
            "type": "double",
            "units": "",
            "fill": "-9999.0",
            "description": "Coverage",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT1H30M",
    "x_relations": [
        {
            "id": "gdc_sat01_orbit_aggregate_PT6H",
            "description": "Lower cadence aggregate statistics of the orbit altitude from the GDC 1 satellite",
            "cadence": "PT6H",
            "type": "resample",
            "method": "mean",
            "add": "manual"
        },
        {
            "id": "gdc_sat01_orbitc_aggregate_P1D",
            "description": "Lower cadence aggregate statistics of the orbit altitude from the GDC 1 satellite",
            "cadence": "P1D",
            "type": "resample",
            "method": "mean",
            "add": "manual"
        },
        {
            "id": "gdc_sat01_orbitc_aggregate_P4D",
            "description": "Lower cadence aggregate statistics of the orbit altitude from the GDC 1 satellite",
            "cadence": "P4D",
            "type": "resample",
            "method": "mean",
            "add": "manual"
        },
        {
            "id": "gdc_sat01_orbit_aggregate_P16D",
            "description": "Lower cadence aggregate statistics of the orbit altitude from the GDC 1 satellite",
            "cadence": "P16D",
            "type": "resample",
            "method": "mean",
            "add": "manual"
        },
        {
            "id": "gdc_sat01_orbit_aggregate_P32D",
            "description": "Lower cadence aggregate statistics of the orbit altitude from the GDC 1 satellite",
            "cadence": "P32D",
            "type": "resample",
            "method": "mean",
            "add": "manual"
        }
    ]
}'''

def get_metadata(metadata_json, satnumber):
    # Update the metadata
    # 1. Set the satellite
    metadata_json_sat = metadata_json.replace(
        'gdc_sat01_orbit',
        f'gdc_sat{satnumber:02d}_orbit'
    )
    metadata_json_sat = metadata_json_sat.replace(
        'GDC 1',
        f'GDC {satnumber}'
    )
    metadata = json.loads(metadata_json_sat)
    return metadata


class GDCorbits():
    def __init__(self,
                 dir='/Volumes/datasets/geospace_dynamics_constellation/GDC_EphemerisRevC-2/',
                 sat=1,
                 phase=1,
                 t0=pd.to_datetime("2028-07-31T00:00:00", utc=True),
                 t1=pd.to_datetime("2031-05-01T00:00:00", utc=True)):
        self.dir = dir
        self.sat = sat
        self.phase = phase
        self.filename = f'{dir}/Phase_{phase:1d}/GDC_Ephemeris_UTC_G{sat:1d}_ND_Phase{phase:1d}.txt'

    def set_time_interval(self, t0, t1):
        self.t0 = pd.to_datetime(t0, utc=True)
        self.t1 = pd.to_datetime(t1, utc=True)

    def to_dataframe(self):
        columns = ["Day",
                   "Month",
                   "Year",
                   "Time",
                   "X-J2K",
                   "Y-J2K",
                   "Z-J2K",
                   "Vx-J2K",
                   "Vy-J2K",
                   "Vz-J2K",
                   "LAT",
                   "LON",
                   "ALT",
                   "Xsun-J2K",
                   "Ysun-J2K",
                   "Zsun-J2K",
                   "Xpm-Y2K",
                   "Ypm-J2K",
                   "Zpm-J2K"]
        df = pd.read_table(self.filename,
                           skiprows=6,
                           delim_whitespace=True,
                           header=None,
                           names=columns,
                           parse_dates={
                               'datetime': ["Year", "Month", "Day", "Time"]
                                        }
                           )
        df.index = pd.to_datetime(df['datetime'], format='mixed', utc=True)
        return df


def ingest_gdc_orbit(satnumber, metadata):
    # Get the necessary info from metadata
    metadata_values = hapi_client.get_info_values(metadata)
    db_id = metadata_values['id']
    parameters = metadata_values['parameters']
    replace_nan_fill = metadata_values['replace_nan_fill']

    # Info
    info_request = hapi_client.get_info(hapi_server, db_id)

    # Create if the dataset does not exist
    if info_request['status']['code'] == 1406:
        logging.info(
            f"Creating new dataset {db_id} on server {hapi_server}."
        )
        hapi_client.create_dataset(hapi_server, hapi_server_key, metadata)

    for phase in [1, 2, 3, 4]:
        # Skip if file has already been processed
        o = GDCorbits(sat=satnumber, phase=phase)
        gdc_orbit = o.to_dataframe().rename(
            {'X-J2K': 'x_gcrs',
             'Y-J2K': 'y_gcrs',
             'Z-J2K': 'z_gcrs',
             'Vx-J2K': 'vx_gcrs',
             'Vy-J2K': 'vy_gcrs',
             'Vz-J2K': 'vz_gcrs'}, axis=1
        )

        # Process the orbit transforms
        gdc_orbit_itrs = transforms.igrs_to_itrs(gdc_orbit)
        gdc_orbit_geo = transforms.itrf_to_geodetic(gdc_orbit_itrs)
        gdc_orbit_complete = transforms.geodetic_to_qd(gdc_orbit_geo)

        # Add time-tag as string
        gdc_orbit_complete['time'] = (
            gdc_orbit_complete.index.strftime(ISO_TIMEFMT)
        )

        gdc_data = gdc_orbit_complete[parameters].replace(replace_nan_fill)

        # Upload to the data store
        hapi_client.add_data(hapi_server,
                             hapi_server_key,
                             db_id,
                             gdc_data)


        hapi_client.resample_lower_cadences(
            hapi_server,
            hapi_server_key,
            db_id,
            dataframe=gdc_orbit_complete,
            t_first_data=gdc_orbit_complete.index[0],
            t_last_data=gdc_orbit_complete.index[-1]
        )


def ingest_gdc_all():
    for satnumber in [1, 2, 3, 4, 5, 6]:
        print(f"Processing for satellite {satnumber}")
        metadata = get_metadata(metadata_json, satnumber)
        _ = ingest_gdc_orbit(satnumber, metadata)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    ingest_gdc_all()