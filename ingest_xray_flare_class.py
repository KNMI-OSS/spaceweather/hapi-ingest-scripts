#!/usr/bin/env python3
from swxtools.access import flare_class
import logging
import schedule
import time
import json
from swxtools.config import config
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
)

hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

metadata_json = '''{
    "id": "xray_flux_flare_class",
    "description": "GOES XRS flare class",
    "timeStampLocation": "begin",
    "resourceURL": "",
    "resourceID": "",
    "contact": "eelco.doornbos@knmi.nl",
    "contactID": "Eelco Doornbos",
    "parameters": [
        {
            "name": "time",
            "type": "isotime",
            "units": "UTC",
            "fill": "-9999.0",
            "description": "Timestamp of the image",
            "label": "",
            "key": true
        },
        {
            "name": "xrsb_flux",
            "type": "double",
            "units": "W/m2",
            "fill": "-999.0",
            "description": "Averaged flux for XRS-B",
            "label": "",
            "key": false
        },
        {
            "name": "flare_class",
            "type": "string",
            "units": "",
            "fill": "None",
            "description": "Flare class",
            "label": "",
            "key": false
        }
    ],
    "cadence": "PT6H"
}'''

def ingest_flare_class():
    # Ensure the dataset is on the server
    metadata = json.loads(metadata_json)
    ensure_dataset_info(hapi_server, hapi_server_key, metadata)

    # Get the data
    df = flare_class.get_combined_flare_class()

    # Select the M-class and X-class flares only
    mxflares = df[df['flare_class'].str.startswith('M') |
                  df['flare_class'].str.startswith('X')]
    mxflares['time'] = mxflares.index.strftime(ISO_TIMEFMT)

    # Upload to the HAPI server
    logging.info("Updating flare class data")
    metadata_values = get_info_values(metadata)
    parameters = metadata_values["parameters"]
    db_id = metadata_values['id']

    add_data(hapi_server,
             hapi_server_key,
             db_id,
             mxflares[parameters])


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s",
                        level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    ingest_flare_class()
    schedule.every(30).minutes.do(ingest_flare_class)

    while True:
        try:
            schedule.run_pending()
        except (BaseException) as err:
            logging.error(f"Exception: {err}")
        time.sleep(1)
